/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author albert
 */
public class TaskInformation {
    public static void main(String[] args)
    {     
         TaskInformation x = new TaskInformation();

    }
    
    /**
     * NAME: getTaskAss
     * PURPOSE: connects to the servers php to get the records of nurse Task and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getTaskAss(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/nurseTask.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/task/nurseTask.json", "nurseTask");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: setAccomplishUnaccomplish
     * PURPOSE: This method will delete the specific nurse task record 
     * PARAMETERS: int ID, int status
     * RETURN VALUE: void
     */
    
    public void setAccomplishUnaccomplish(int ID, int status ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/setAccomplishUnaccomplishTask.php?ID="+ID+"&status="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: deleteAccomplishUnaccomplishTask
     * PURPOSE: This method will delete the specific nurse task record 
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void deleteAccomplishedTask(int ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/deleteAccomplishedTask.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
    /**
     * NAME: deleteUnaccomplishUnaccomplishTask
     * PURPOSE: This method will delete the specific nurse task record 
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void deleteUnaccomplishedTask(int ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/deleteUnaccomplishedTask.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
      /**
     * NAME: addTask
     * PURPOSE: This method will add new Task record.
     * PARAMETERS: String description, String typee,String patient,String nurse,String d,String ID
     * RETURN VALUE: void   updateTask.php
     */
    
    public void addTask(String description, String typee,String patient,String nurse,String d,String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/addTask.php?description="+description+"&typee="+typee+"&patient="+patient+"&nurse="+nurse+"&d="+d+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
     /**
     * NAME: updateTask
     * PURPOSE: This method will update new Task record.
     * PARAMETERS:  String description,String typee,String patient,String nurse,String IDS,String d
     * RETURN VALUE: void
     */
    
    public void  updateTask(String description,String typee,String patient,String nurse,String IDS,String d){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/updateTask.php?description="+description+"&typee="+typee+"&patient="+patient+"&nurse="+nurse+"&IDS="+IDS+"&d="+d+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteTaskHeadnurse
     * PURPOSE: This method will delete nurse task record.
     * PARAMETERS: String IDS 
     * RETURN VALUE: void
     */
    
      public void deleteTaskHeadnurse(String IDS ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/deleteTaskHeadnurse.php?IDS="+IDS+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
      
      /**
     * NAME: deleteStatus
     * PURPOSE: This method will update new nurse task record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
      
      public void deleteStatus(String ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/task/deleteAccomplishedTask.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
}
