/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class PaymentInformation {
    
    /**
     * NAME: addRequestPayment
     * PURPOSE: This method will add new request payment record.
     * PARAMETERS: String patID, String qty, String price, String desc
     * RETURN VALUE: void
     */
    public void addRequestPayment(String patID, String qty, String price, String desc){
        double total;
        double q;
        double p;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        q = Float.parseFloat(qty);
        p = Integer.parseInt(price);
        
        total = q * p;
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/payment/addPayment.php?patID="+patID+"&qty="+qty+"&price="+price+"&desc="+desc+"&total="+total+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
}
