/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_DB_IPAddress.ServerIPAddress;
import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author cathy
 */
public class PatientRoomInformation {
    
    
    /**
     * NAME: getPatRoom
     * PURPOSE: This method will get Patient room record.
     * PARAMETERS: no parameters.
     * RETURN VALUE: 
     */
    
    public void getPatRoom() throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patRoom/patRoom.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
       
        download.downloadData("http://"+IP+"/HIMSv2/patRoom/patRoom.json", "patRoom");
       
    }
    /**
     * NAME: addProom
     * PURPOSE: This method will add new Room record.
     * PARAMETERS: int idrm, String pat
     * RETURN VALUE: void
     */
    
    public void addProom(int idrm, String pat){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patRoom/addpatRoom.php?idrm="+idrm+"&pat="+pat+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
     /**
     * NAME: updatepatRoom
     * PURPOSE: This method will update new Room record.
     * PARAMETERS: int idrm, String roomId
     * RETURN VALUE: void
     */
    
    public void updatepatRoom(int idrm, String roomId){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patRoom/updatepatRoom.php?idrm="+idrm+"&roomId="+roomId+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
     /**
     * NAME: OutpatRoom
     * PURPOSE: This method will update new Room record.
     * PARAMETERS: String roomId
     * RETURN VALUE: void
     */
    public void OutpatRoom(String roomId){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patRoom/outpatRoom.php?roomId="+roomId+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    /**
     * NAME: getOutPatRoom
     * PURPOSE: This method will set Out Patient room record.
     * PARAMETERS: int ID
     * RETURN VALUE: 
     */
    public void getOutPatRoom(int ID) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patRoom/patRoomOut.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
       
        download.downloadData("http://"+IP+"/HIMSv2/patRoom/patRoomOut.json", "patRoomOut");
       
    }
}
