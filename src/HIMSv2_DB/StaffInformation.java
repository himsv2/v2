/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_DB_IPAddress.ServerIPAddress;
import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class StaffInformation {
    
    
    /**
     * NAME: getStaff
     * PURPOSE: get staff information from the db.
     * PARAMETERS: no parameters.
     * RETURN VALUE: 
     */
    public void getStaff() throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/staff.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
       
        download.downloadData("http://"+IP+"/HIMSv2/staff/staff.json", "staff");
       
    }
    /**
     * NAME: addStaff
     * PURPOSE: This method will add new Staff record.
     * PARAMETERS: String fname, String lname, String mname, String username, String password, String age, String gender,
     * String contact, String email, String address, String citizen, String department, String bday, String position, int ns
     * RETURN VALUE: void
     */
    
    public void addStaff(String fname, String lname, String mname, String username, String password, String age, String gender,
            String contact, String email, String address, String citizen, String department, String bday, String position, int ns){
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/addStaff.php?fname="+fname+"&lname="+lname+"&mname="+mname+"&username="+username+"&password="+password+
                "&age="+age+"&gender="+gender+"&contact="+contact+"&email="+email+"&address="+address+"&citizen="+citizen+"&department="+department+"&bday="+bday+
                "&position="+position+"&ns="+ns+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateStaff
     * PURPOSE: This method will add new Staff record.
     * PARAMETERS: String ID,String fname, String lname, String mname, String username, String password, String age, String gender,
     * String contact, String email, String address, String citizen, String department, String bday, String position, int ns
     * RETURN VALUE: void
     */
    public void updateStaff(String ID,String fname, String lname, String mname, String username, String age, String gender,
            String contact, String email, String address, String citizen, String department, String bday, String position, int ns){
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/updateStaff.php?fname="+fname+"&lname="+lname+"&mname="+mname+"&username="+username+"&age="+age+"&gender="+gender+"&contact="+contact+"&email="+email+"&address="+address+"&citizen="+citizen+"&department="+department+"&bday="+bday+
                "&position="+position+"&ns="+ns+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
     /**
     * NAME: quitStaff
     * PURPOSE: This method will change the staff position.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void quitStaff(String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/quitStaff.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Changed!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateUsername
     * PURPOSE: This method will add new Staff record.
     * PARAMETERS: int ID,String username
     * RETURN VALUE: void
     */
    
    public void updateUsername(int ID,String username){       
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/updateUsername.php?ID="+ID+"&username="+username+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: userInfo
     * PURPOSE: This method will get user info record.
     * PARAMETERS: int ID,String username
     * RETURN VALUE: void
     */
    
    public void userInfo(int ID){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/login/userInfo.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
         try {
            download.downloadData("http://"+IP+"/HIMSv2/login/login.json", "phplog_login");
        } catch (IOException ex) {
            System.out.println("error");
        }   
    }
    
    /**
     * NAME: updatePassword
     * PURPOSE: This method will update user pass record.
     * PARAMETERS: int ID,String password
     * RETURN VALUE: void
     */
    
    public void updatePassword(int ID,String password){
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/staff/updatePassword.php?ID="+ID+"&password="+password+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
}
