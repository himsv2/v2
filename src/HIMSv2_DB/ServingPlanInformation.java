/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author CAMINERO
 */
public class ServingPlanInformation {

    public static void main(String[] args) {
        ServingPlanInformation x = new ServingPlanInformation();

    }

    /**
     * NAME: getServingPlan 
     * PURPOSE: connects to the servers php to get the records of Serving Plan and download the record from the server to this PC.
     * PARAMETERS: no parameters 
     * RETURN VALUE: void
     */
    
    public void getServingPlan() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/servingplan/servingplan.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/servingplan/servingplan.json", "servingplan");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }

     /**
     * NAME: addServingPlan 
     * PURPOSE: This method will add new Serving Plan record
     * PARAMETERS: String date, String day, String pat, int meal,String time,String amPm
     * RETURN VALUE: void
     */
    
    public void addServingPlan(String date, String day, String pat, int meal,String time,String amPm) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/servingplan/addservingplan.php?date="+date+"&day="+day+"&pat="+pat+"&meal="+meal+"&time="+time+"&amPm="+amPm+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
     /**
     * NAME: UpdateServingPlan
     * PURPOSE: This method will update new Serving Plan record
     * PARAMETERS: int m, String Id, String day, String date,String time,String amPm
     * RETURN VALUE: void
     */
    
    public void UpdateServingPlan(int m, String Id, String day, String date,String time,String amPm) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/servingplan/updateservingplan.php?m=" + m + "&Id=" + Id + "&day=" + day + "&date=" + date + "&time="+time+"&amPm="+amPm+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }

    /**
     * NAME: deleteServingPlan
     * PURPOSE: This method will delete Serving Plan record
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void deleteServingPlan(String ID) throws IOException {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/servingplan/deleteServingplan.php?ID=" + ID + "";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: ServedServingPlan
     * PURPOSE: This method will get Serving Plan record
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
     public void ServedServingPlan(String ID) throws IOException {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/servingplan/Servedservingplan.php?ID=" + ID + "";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
}
