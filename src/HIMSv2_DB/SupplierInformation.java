/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author albert
 */
public class SupplierInformation {
    public static void main(String[] args)
    {     
         SupplierInformation x = new SupplierInformation();

    }
    
    /**
     * NAME: getSupplier
     * PURPOSE: connects to the servers php to get the records of Supplier and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getSupplierList(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/supplier/supplierList.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/supplier/supplierList.json", "supplierList");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
     
   
    
    /**
     * NAME: addSupplierList
     * PURPOSE: This method will add new Supplier record.
     * PARAMETERS: String supplier name and String supplier address
     * RETURN VALUE: void
     */
    
    public void addSupplierList(String suppName, String suppAdd){
        String date;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/supplier/supplierListAdd.php?suppName="+suppName+"&suppAdd="+suppAdd+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);

    }
    
    /**
     * NAME: delSupplierList
     * PURPOSE: This method will delete Supplier record.
     * PARAMETERS: integer ID
     * RETURN VALUE: void
     */
    
    public void delSupplierList(int ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/supplier/delsuppList.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
    /**
     * NAME: updateSupplierList
     * PURPOSE: This method will update Supplier record.
     * PARAMETERS: integer suppId , String supplier name, and String supplier address
     * RETURN VALUE: void
     */
    
    public void updateSupplierList(int suppId, String suppName, String suppAdd){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/supplier/updatesupplierList.php?suppName="+suppName+"&suppAdd="+suppAdd+"&suppId="+suppId+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
}
