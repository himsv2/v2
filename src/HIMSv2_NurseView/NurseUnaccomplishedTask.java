/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_NurseView;

import HIMSv2_EquipmentInventoryView.*;
import HIMSv2_EmergencyView.*;
import HIMSv2_DB.BedInformation;
import HIMSv2_DB.BuildingInformation;
import HIMSv2_DB.RequestInformation;
import HIMSv2_DB.RoomInformation;
import HIMSv2_DB.RoomTypeInformation;
import HIMSv2_DB.TaskInformation;
import HIMSv2_FunctionChecker.FunctionNameGetter;
import HIMSv2_FunctionChecker.FunctionSearch;
import HIMSv2_NurseView.NurseAccomplishedTask;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JRootPane;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class NurseUnaccomplishedTask extends javax.swing.JPanel {

    /**
     * Creates new form adminManageBuildingView
     */
    public NurseUnaccomplishedTask() throws IOException {
        initComponents();
        this.data();
    }
    
    /**
     * NAME: data
     * PURPOSE: this will get the information of the bed from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values.
     */
    
    public void data() throws IOException{
        TaskInformation a = new TaskInformation();
        a.getTaskAss();
        try {
            this.loadTask();
        } catch (FileNotFoundException ex) {
            System.out.println("error");
        } catch (JSONException ex) {
            System.out.println("error");
        }
        
    }
    
    /**
     * NAME: loadTask
     * PURPOSE: it will displays the data that we get from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values
     */
    public void loadTask() throws FileNotFoundException, IOException, JSONException{
        
        int num = 1;
        String desc;
        String type;
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel)tblrequest.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        try {
  
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseTask.json"));
            JSONObject json = (JSONObject) o;
            
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                     if(json.get("Status").equals("3")){    
                        desc = (String) json.get("tskDesc");
                        desc = desc.replace("_", " ");
                        type = (String) json.get("tskType");
                        type = type.replace("_", " ");
                         model.addRow(new Object[]{num,desc,type,json.get("tskTime"),json.get("tskPat_id")});
                        num++;
                     }
            }
        } catch (ParseException ex) {
          //  Logger.getLogger(Patient.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblrequest = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        tblrequest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "TASK", "TYPE", "TIME", "PATIENT NO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblrequest.setRowHeight(25);
        tblrequest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblrequestMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblrequest);

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });

        jLabel1.setText("SEARCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(393, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tblrequestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblrequestMouseClicked
        // TODO add your handling code here:
        try {
            // TODO add your handling code here:
            int number = 0;
            String tskDesc;
            String tskType;
            String tskTime;
            String tskPat_id;
                    
            int rowNum = tblrequest.getSelectedRow();
            number = (int) tblrequest.getValueAt(rowNum, 0);
            tskDesc= (String) tblrequest.getValueAt(rowNum, 1);
            tskType= (String) tblrequest.getValueAt(rowNum, 2);
            tskTime= (String) tblrequest.getValueAt(rowNum, 3);
            tskPat_id= (String) tblrequest.getValueAt(rowNum, 4);
            
            NurseUnaccomplishedTaskForm view = new NurseUnaccomplishedTaskForm();
            view.setVb(this);
            view.setVisible(true);
            view.setdata(number, tskDesc, tskType, tskTime, tskPat_id);
        } catch (IOException ex) {
            Logger.getLogger(NurseAccomplishedTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(NurseAccomplishedTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblrequestMouseClicked

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        // TODO add your handling code here:
        FunctionSearch a = new FunctionSearch();
        a.search(tblrequest, search.getText());
        
    }//GEN-LAST:event_searchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblrequest;
    // End of variables declaration//GEN-END:variables
}
