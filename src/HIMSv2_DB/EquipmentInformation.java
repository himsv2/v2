/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class EquipmentInformation {
    public static void main(String[] args)
    {     
         EquipmentInformation x = new EquipmentInformation();

    }
    
    /**
     * NAME: getEquipment
     * PURPOSE: connects to the servers php to get the records of Equipment and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getEquipment(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/equipment/equipment.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/equipment/equipment.json", "equipment");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addEquipment
     * PURPOSE: This method will add new Equipment record.
     * PARAMETERS: String item code name of the equipment String quantity of the equipment, String equipment type,
     * String equipment description and string price of the equipment.
     * RETURN VALUE: void
     */
    public void addEquipment(String itemCode, String name, String qty, String type, String desc, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/equipment/addEquipment.php?itemCode="+itemCode+"&name="+name+"&qty="+qty+"&type="+type+"&desc="+desc+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    /**
     * NAME: updateEquipment
     * PURPOSE: This method will update new Equipment record.
     * PARAMETERS: String item Code, String name, String quantity, String type, String description,int Equipment ID and the price of the equipment.
     * RETURN VALUE: void
     */
    public void updateEquipment(String itemCode, String name, String qty, String type, String desc,int EquipID, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/equipment/updateEquipment.php?itemCode="+itemCode+"&name="+name+"&qty="+qty+"&type="+type+"&desc="+desc+"&EquipID="+EquipID+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteEquipment
     * PURPOSE: This method will delete new Equipment record.
     * PARAMETERS: int Equipment ID to be deleted
     * RETURN VALUE: void
     */
    public void deleteEquipment(int EquipID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/equipment/deleteEquipment.php?EquipID="+EquipID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    /**
     * NAME: dupdateQuantity
     * PURPOSE: This method will update the quantity of the Equipment record.
     * PARAMETERS: int Equipment ID and quantity.
     * RETURN VALUE: void
     */
    public void updateQuantity(int ID, int qty){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/equipment/updateEquipmentQty.php?ID="+ID+"&qty="+qty+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
}
