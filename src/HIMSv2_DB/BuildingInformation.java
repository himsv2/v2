/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class BuildingInformation {
    public static void main(String[] args)
    {     
         BuildingInformation x = new BuildingInformation();

    }
    
    /**
     * NAME: getBuilding
     * PURPOSE: connects to the servers php to get the records of Building and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBuilding(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/building/building.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/building/building.json", "building");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addBuilding
     * PURPOSE: This method will add new building record.
     * PARAMETERS: String building name and String description
     * RETURN VALUE: void
     */
    
    public void addBuilding(String buildingName, String desc){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/building/addBuilding.php?buildingName="+buildingName+"&desc="+desc+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    /**
     * NAME: deleteBuilding
     * PURPOSE: This method will delete the building record.
     * PARAMETERS: int ID of the building to delete
     * RETURN VALUE: void
     */
    
    public void deleteBuilding(int buildID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/building/deleteBuilding.php?buildID="+buildID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    /**
     * NAME: updateBuilding
     * PURPOSE: This method will update building record.
     * PARAMETERS: String building name and String description
     * RETURN VALUE: void
     */
    
    public void updateBuilding(String buildingName, String desc,int buildId){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/building/updateBuilding.php?buildingName="+buildingName+"&desc="+desc+"&buildId="+buildId+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
}
