/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_DB_IPAddress.ServerIPAddress;
import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class DoctorInformation {
    
    
    /**
     * NAME: getDoc
     * PURPOSE: get doctor's information from the db.
     * PARAMETERS: no parameters.
     * RETURN VALUE: void
     */
    public void getDoc() throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/doctor/doctor.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
       
        download.downloadData("http://"+IP+"/HIMSv2/doctor/doctor.json", "doctor");
       
    }
}
