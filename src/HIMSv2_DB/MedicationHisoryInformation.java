/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class MedicationHisoryInformation {
    /**
     * NAME: getMedicationHistory
     * PURPOSE: connects to the servers php to get the records of medication history of the given patient and download the record
     * from the server to this PC.
     * PARAMETERS: int ID of the patient
     * RETURN VALUE: void
     */
    
    public void getMedicationHistory(int ID){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicationHistory/medicationHistory.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/medicationHistory/medicationHistory.json", "medicationHistory");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
     /**
     * NAME: addMedication
     * PURPOSE: This method will add new medication record for the specific patient.
     * PARAMETERS: int patient ID, String diagnose , String description and int docID
     * RETURN VALUE: void
     */
    public void addMedication(int ID, String diagnose, String desc, int docID ){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicationHistory/addMedicationHistory.php?ID="+ID+"&diagnose="+diagnose+"&desc="+desc+"&date="+date+"&docID="+docID+"";
    
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
}
