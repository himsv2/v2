/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.DoctorInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.RoomInformation;
import HIMSv2_DB.StaffInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionNameGetter {
    
    /**
     * NAME: getbuildingName
     * PURPOSE: This method will get the name of the given building id.
     * PARAMETERS: int buildingID
     * RETURN VALUE: string  building name
     */
    
    public String getbuildingName(int buildingID) throws FileNotFoundException, IOException, ParseException{
        String building = " ";
        String buildID;
        int b = 0;
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            buildID = (String) json.get("buildId");
            b = Integer.parseInt(buildID);
            if(( b == buildingID)) {
                building =  (String) json.get("buildName");
            }
            
        } 
        return building;
    }
    
    /**
     * NAME: getfloorName
     * PURPOSE: This method will get the name of the given floor id.
     * PARAMETERS: int floor id
     * RETURN VALUE: string  floor name
     */
    
    public String getfloorName(int floorID) throws FileNotFoundException, IOException, ParseException{
        String floor = " ";
        String floorId;
        int b = 0;
       
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\floor.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            floorId = (String) json.get("floor_id");
            b = Integer.parseInt(floorId);
            if(( b == floorID)) {
                floor =  (String) json.get("floorName");
            }
            
        } 
        return floor;
    }
    
    /**
     * NAME: getRoomTypeName
     * PURPOSE: This method will get the name of the given room type id.
     * PARAMETERS: integer room type id
     * RETURN VALUE: string  room type name
     */
    
    public String getRoomTypeName(int rmID) throws FileNotFoundException, IOException, ParseException{
        String rm = " ";
        String rmId;
        int b = 0;
       
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\roomType.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rmId = (String) json.get("rtId");
            b = Integer.parseInt(rmId);
            if(( b == rmID)) {
                rm =  (String) json.get("rtName");
            }
            
        } 
        return rm;
    }
    
    /**
     * NAME: getPatient
     * PURPOSE: This method will get the name of the given patient id.
     * PARAMETERS: integer patient id
     * RETURN VALUE: string  patient name
     */
    
    public String getPatient(int pID) throws FileNotFoundException, IOException, ParseException{
        String p = " ";
        String pId;
        int b = 0;
       
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            pId = (String) json.get("rtId");
            b = Integer.parseInt(pId);
            if(( b == pID)) {
                p =  (String) json.get("rtName");
            }
            
        } 
        return p;
    }
    
    /**
     * NAME: getRoom
     * PURPOSE: This method will get the name of the given patient id.
     * PARAMETERS: int room id
     * RETURN VALUE: string room name
     */
    
    public String getRoom(int rID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        
        RoomInformation a = new RoomInformation();
        a.getRoom();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("rmId");
            b = Integer.parseInt(rId);
            if(( b == rID)) {
                r =  (String) json.get("rmName");
            }
            
        } 
        return r;
    }
    /**
     * NAME: getRoomFilter
     * PURPOSE: This method will get the name of the given room id.
     * PARAMETERS: int room id
     * RETURN VALUE: string room name
     */
    
    public String getRoomFilter(int rID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("rmId");
            b = Integer.parseInt(rId);
            if(( b == rID)) {
                r =  (String) json.get("rmName");
            }
            
        } 
        return r;
    }
    
    /**
     * NAME: getNurseStation
     * PURPOSE: This method will get the name of the given nurseStation id.
     * PARAMETERS: String nurse Station id
     * RETURN VALUE: string  nurse station name
     */
    
    public String getNurseStation(int nsID) throws FileNotFoundException, IOException, ParseException{
        String ns = " ";
        String nurseStationID;
        int b = 0;
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            nurseStationID = (String) json.get("nstId");
            b = Integer.parseInt(nurseStationID);
            if(( b == nsID)) {
               ns =  (String) json.get("nstName");
            }    
        } 
        return ns;
    }
    
    /**
     * NAME: getstaff
     * PURPOSE: This method will get the name of the given staff id.
     * PARAMETERS: integer staff id
     * RETURN VALUE: string staff name
     */
    
    public String getstaff(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        StaffInformation c = new StaffInformation();
        c.getStaff();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("stId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("stLname") + ","+(String) json.get("stFname");
            }
            
        } 
        return r;
    }
    /**
     * NAME: getPat
     * PURPOSE: This method will get the name of the given staff id.
     * PARAMETERS: String patient id
     * RETURN VALUE: string patient name
     */
    
    public String getPat(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        StaffInformation c = new StaffInformation();
        c.getStaff();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("ptId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("ptLname") + ", "+(String) json.get("ptFname");
            }
            
        } 
        return r;
    }
    /**
     * NAME: getMed
     * PURPOSE: This method will get the name of the given medicine id.
     * PARAMETERS: int  sId
     * RETURN VALUE: string  patient name
     */
    
    public String getMed(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        MedicineInformation c = new MedicineInformation();
        c.getMedicine();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("medInId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("medInName"); 
            } 
        } 
        return r;
    }
     /**
     * NAME: getDoctor
     * PURPOSE: This method will get the name of the given doctor id.
     * PARAMETERS: int doctor id
     * RETURN VALUE: string doctor name
     */
    
    public String getDoctor(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        DoctorInformation c = new DoctorInformation();
        c.getDoc();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\doctor.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("docId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("docLname") + ", "+(String) json.get("docFname"); 
            }
            
        } 
        return r;
    }
    /**
     * NAME: getRestriction
     * PURPOSE: This method will get the name of the given restriction id.
     * PARAMETERS: int restriction id
     * RETURN VALUE: string restriction name
     */
    
    public String getRestriction(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        MedicineInformation c = new MedicineInformation();
        c.getMedicine();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\restriction.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("restId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("restName"); 
            } 
        } 
        return r;
    }
    
    /**
     * NAME: getMeal
     * PURPOSE: This method will get the name of the given meal id.
     * PARAMETERS: int meal id
     * RETURN VALUE: string meal name
     */
    
    public String getMeal(int sID) throws FileNotFoundException, IOException, ParseException{
        String r = " ";
        String rId;
        int b = 0;
        MealPlanInformation c = new MealPlanInformation();
        c.getMealPlan();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            rId = (String) json.get("mealplanId");
            b = Integer.parseInt(rId);
            if(( b == sID)) {
                r =  (String) json.get("mealplanName"); 
            } 
        } 
        return r;
    }
}
