/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB_IPAddress;

/**
 *
 * @author cathy
 */
public class ServerIPAddress {
    
    /**
     * NAME: IPAddress
     * PURPOSE: This is where we can find and get the IP address of the server
     * PARAMETERS: no parameters
     * RETURN VALUE: String (the IP Address of the server
     */
    public String IPAddress(){
        return "192.168.1.19";
    }
}
