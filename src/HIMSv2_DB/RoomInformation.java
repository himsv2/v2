/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class RoomInformation {
    public static void main(String[] args)
    {     
         RoomInformation x = new RoomInformation();

    }
    
    /**
     * NAME: getRoom
     * PURPOSE: connects to the servers php to get the records of room and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getRoom(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/room.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/room/room.json", "room");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addRoom
     * PURPOSE: This method will add new Room record.
     * PARAMETERS: String rmName, String rmDesc, int rmFloor, int rmRoomType, int building,int nsID,String price
     * RETURN VALUE: void
     */
    
    public void addRoom(String rmName, String rmDesc, int rmFloor, int rmRoomType, int building,int nsID,String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/addRoom.php?rmName="+rmName+"&rmDesc="+rmDesc+"&rmFloor="+rmFloor+"&rmRoomType="+rmRoomType+"&building="+building+"&nsID="+nsID+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
     /**
     * NAME: deleteRoom
     * PURPOSE: This method will delete Room record.
     * PARAMETERS: int rID
     * RETURN VALUE: void
     */
    
    public void deleteRoom(int rID ){
        System.out.println(rID);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/deleteRoom.php?rID="+rID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateRoom
     * PURPOSE: This method will update Room record.
     * PARAMETERS: String room name , room description and String room ID, building id and room type
     * RETURN VALUE: void
     */
    
    public void updateRoom(String rmName, String rmDesc, int rmFloor, int rmRoomType, int building, int rID, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
       
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/updateRoom.php?rmName="+rmName+"&rmDesc="+rmDesc+"&rmFloor="+rmFloor+"&rmRoomType="+rmRoomType+"&building="+building+"&rID="+rID+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
     
    }
    
    /**
     * NAME: filterRoom
     * PURPOSE: This method will get the Occupied or not occupied Room record.
     * PARAMETERS: int 0 if not occupied and 1 for occupied.
     * RETURN VALUE: void
     */
    
    public void filterRoom(int rmOccupied ){
        Download_JSON download = new Download_JSON();
        System.out.println(rmOccupied);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/filterRoom.php?rmOccupied="+rmOccupied+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/room/room.json", "room");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
     /**
     * NAME: getRoom
     * PURPOSE: This method will get the Room record.
     * PARAMETERS: String name
     * RETURN VALUE: void
     */
    
    public void getRoom(String name){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/getRoom.php?name="+name+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/room/room.json", "room");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}