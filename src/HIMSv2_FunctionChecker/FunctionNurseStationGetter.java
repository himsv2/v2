/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionNurseStationGetter {
    
    /**
     * NAME: getNS
     * PURPOSE: This method will get the price of the room item.
     * PARAMETERS: String ns
     * RETURN VALUE: string nurse station
     */
    
    public String[] getNS(String ns) throws FileNotFoundException, IOException{
         String[] nurseStation = new String[10];
         int ndx = 0;
         
        JSONParser parser = new JSONParser();
        
        try {
            
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
            
            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                if(ns.equals(json.get("rmNurseStation"))){
                    nurseStation[ndx] = (String) json.get("rmName");
                    ndx++;
                }
                
            }
            
            
        } catch (ParseException ex) {
            System.out.println("error");
        }
         return nurseStation;
    }
}
