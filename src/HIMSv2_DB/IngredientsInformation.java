/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class IngredientsInformation {
    /**
     * NAME: getIngredients
     * PURPOSE: connects to the servers php to get the records of ingredients and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getIngredients(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/ingredients/ingredients.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/ingredients/ingredients.json", "ingredients");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addIngredients
     * PURPOSE: This method will add new ingredients record.
     * PARAMETERS: String ingredients name and String description 
     * RETURN VALUE: void
     */
    
    public void addIngredients(String Name, String desc){
        float p = 0;
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/ingredients/addIngredients.php?Name="+Name+"&desc="+desc+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateIngredients
     * PURPOSE: This method will update the ingredients record.
     * PARAMETERS: String ingredients name and String description and ingredients ID to update
     * RETURN VALUE: void
     */
    
    public void updateIngredients(String Name, String desc,int ID){
        float p = 0;
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/ingredients/updateIngredients.php?Name="+Name+"&desc="+desc+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: delIngredients
     * PURPOSE: This method will delete ingredient's record.
     * PARAMETERS: int ID of the ingredients to be deleted.
     * RETURN VALUE: void
     */
    
    public void delIngredients(int ID){
        float p = 0;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/ingredients/deleteIngredients.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
}
