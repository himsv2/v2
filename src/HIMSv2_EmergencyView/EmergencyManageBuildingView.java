/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_EmergencyView;

import HIMSv2_DB.BuildingInformation;
import HIMSv2_FunctionChecker.FunctionSearch;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class EmergencyManageBuildingView extends javax.swing.JPanel {

    /**
     * Creates new form adminManageBuildingView
     */
    public EmergencyManageBuildingView() throws IOException {
        initComponents();
        this.data();
    }
    
    /**
     * NAME: data
     * PURPOSE: this will get the information of the building from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values.
     */
    
    public void data() throws IOException{
     
        BuildingInformation patient = new BuildingInformation();
        patient.getBuilding();
        
        try {
            this.loadbuilding();
        } catch (FileNotFoundException ex) {
            System.out.println("error");
        } catch (JSONException ex) {
            System.out.println("error");
        }
        
    }
    
    /**
     * NAME: loadbuilding
     * PURPOSE: it will displays the data that we get from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values
     */
    public void loadbuilding() throws FileNotFoundException, IOException, JSONException{
        String desc ;
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel)tblBuilding.getModel();
        model.setRowCount(0);
        Object[] row = new Object[2];
        try {
            
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
            
            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                desc = (String) json.get("buildDesc");
                desc = desc.replace("_", " ");
                model.addRow(new Object[]{json.get("buildName"),desc});
            }
            
            
        } catch (ParseException ex) {
          //  Logger.getLogger(Patient.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblBuilding = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        addBuilding = new javax.swing.JButton();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseClicked(evt);
            }
        });

        tblBuilding.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NAME", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBuilding.setRowHeight(25);
        tblBuilding.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBuildingMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblBuilding);

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        addBuilding.setText("ADD BUILDING");
        addBuilding.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBuildingActionPerformed(evt);
            }
        });

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });

        jLabel1.setText("SEARCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
                .addComponent(addBuilding, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(98, 98, 98))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(addBuilding, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(21, 21, 21))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addBuildingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBuildingActionPerformed
        // TODO add your handling code here:
        EmergencyAddBuilding add = new EmergencyAddBuilding();
        add.setVb(this);
        add.setVisible(true);
    }//GEN-LAST:event_addBuildingActionPerformed

    private void jScrollPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseClicked
        // TODO add your handling code here:
       
        
    }//GEN-LAST:event_jScrollPane1MouseClicked

    private void tblBuildingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBuildingMouseClicked
        // TODO add your handling code here:
        String name; 
        String desc;
        int rowNum = tblBuilding.getSelectedRow();
        name = (String) tblBuilding.getValueAt(rowNum, 0);
        desc = (String) tblBuilding.getValueAt(rowNum, 1);
        EmergencyUpdateDeleteBuildingView view = new EmergencyUpdateDeleteBuildingView();
        view.setVb(this);
        view.setVisible(true);
        
        try {
            view.setData(name, desc);
        } catch (IOException ex) {
            Logger.getLogger(EmergencyManageBuildingView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(EmergencyManageBuildingView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_tblBuildingMouseClicked

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        // TODO add your handling code here:
        FunctionSearch a = new FunctionSearch();
        a.search(tblBuilding, search.getText());
        
    }//GEN-LAST:event_searchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addBuilding;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblBuilding;
    // End of variables declaration//GEN-END:variables
}
