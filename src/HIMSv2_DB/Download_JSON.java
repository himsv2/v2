/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 *
 * @author cathy
 */
public class Download_JSON {
    
    /**
     * NAME: downLoadData
     * PURPOSE: this is where you will create the filepath to where you want to save the JSON file you want to download 
     *          from the server and call the function downloadUsingNIO to download from the server to this PC.
     * PARAMETERS: String url and String responsible(the filename of the JSON file)
     * RETURN VALUE: void
     */
    
    public static void downloadData(String url, String responsible) throws IOException{
        String filePath = "C:\\HIMSv2\\"+responsible+".json";
        File file = new File(filePath);
           
        File dir = new File("C:\\HIMSv2");
            
        System.out.println(filePath);
        
        if (!dir.exists()) {
            if (dir.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        if (file.createNewFile()){
            System.out.println("File is created!");
        }else{
            System.out.println("File already exists.");
        }
            
        downloadUsingNIO(url,filePath);
        System.out.println("Downloading "+ url+" |||| "+filePath);
        System.out.println("Download Complete!");
    } 
    
    /**
     * NAME: downloadingUsingNIO
     * PURPOSE: This is where the downloading of the JSON file from the server.
     * PARAMETERS: String urlStr and String file ( the file path)
     * RETURN VALUE: void
     */
       
    public static void downloadUsingNIO(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
    }  
}
