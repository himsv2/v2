/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.DocFeeInformation;
import HIMSv2_DB.EquipmentInformation;
import HIMSv2_DB.LinensInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.RoomInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionPriceGetter {

    /**
     * NAME: getEquipmentPrice PURPOSE: This method will get the price of the
     * equipment item. PARAMETERS: String equipment name RETURN VALUE: string
     * price
     */
    public String getEquipmentPrice(String item) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        EquipmentInformation a = new EquipmentInformation();
        a.getEquipment();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("equipName").equals(item))) {
                price = (String) json.get("equipPrice");
            }
        }
        return price;
    }

    /**
     * NAME: getLinensPrice PURPOSE: This method will get the price of the
     * equipment item. PARAMETERS: String linens name RETURN VALUE: string price
     */
    public String getLinensPrice(String item) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        LinensInformation a = new LinensInformation();
        a.getLinens();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("lineName").equals(item))) {
                price = (String) json.get("lPrice");
            }
        }
        return price;
    }

    /**
     * NAME: getMedicinePrice PURPOSE: This method will get the price of the
     * medicine item. PARAMETERS: String medicine name RETURN VALUE: string
     * price
     */
    public String getMedicinePrice(String item) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        MedicineInformation a = new MedicineInformation();
        a.getMedicine();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("medInName").equals(item))) {
                price = (String) json.get("medInPrice");
            }
        }
        return price;
    }

    /**
     * NAME: getRoomPrice PURPOSE: This method will get the price of the room
     * PARAMETERS: String item RETURN VALUE: string price
     */
    public String getRoomPrice(String item) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        RoomInformation a = new RoomInformation();
        a.getRoom();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("rmName").equals(item))) {
                price = (String) json.get("rmPrice");
            }
        }
        return price;
    }

    /**
     * NAME: getMealPrice PURPOSE: This method will get the price of the meal
     * item. PARAMETERS: String item RETURN VALUE: string price
     */
    public String getMealPrice(String item) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        MealPlanInformation a = new MealPlanInformation();
        a.getMealPlan();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("mealplanName").equals(item))) {
                price = (String) json.get("mealplanPrice");
            }
        }
        return price;
    }

    /**
     * NAME: getMealPrice PURPOSE: This method will get the price of the meal
     * item. PARAMETERS: String item RETURN VALUE: string price
     */
    public String getDocFee(String ID) throws FileNotFoundException, IOException, ParseException {

        String price = "0";
        DocFeeInformation a = new DocFeeInformation();
        a.getDocFee();
        int ctr = 0;

        a.getDocFeeID(ID);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\docFee.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("doc_Id").equals(ID))) {
                ctr++;
            }
        }
        if (ctr != 0) {
            Object p = parser.parse(new FileReader("C:\\HIMSv2\\docFeeID.json"));
            JSONObject json2 = (JSONObject) p;
            org.json.simple.JSONArray l = (org.json.simple.JSONArray) json2.get("data");
            Iterator<String> j = l.iterator();
            json2 = (JSONObject) l.get(0);

            price = (String) json.get("docFee");

        } else {
            JOptionPane.showMessageDialog(null, "Sorry, this doctor is not yet available for patient!");
        }

        return price;
    }
}
