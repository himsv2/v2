/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author CAMINERO
 */
public class AccountingInformation {
    public static void main(String[] args) {
        AccountingInformation x = new AccountingInformation();
    }
    
    /**
     * NAME: getRequistion
     * PURPOSE: This method will get the Requisition record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getRequistion(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisition.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/accounting/purchaseRequisition.json", "purchaseRequisition");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getOrder
     * PURPOSE: This method will get the Order record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getOrder(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseOrder.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/accounting/purchaseOrder.json", "purchaseOrder");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getTransaction
     * PURPOSE: This method will get the Transaction record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getTransaction(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseTransaction.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/accounting/purchaseTransaction.json", "purchaseTransaction");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addRequistion 
     * PURPOSE: This method will add new Requisition record.
     * PARAMETERS: String name, String desc, String qty , String L and String n
     * RETURN VALUE: void
     */
    public void addRequistion(String name, String desc, String qty,String L,String n) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseRequisitionAddReq.php?name=" + name + "&qty=" + qty + "&desc=" +desc+ "&date="+date+"&L="+L+"&n="+n+" ";
        JavaHTTPConnectionReader javaHTTPConnectionReader = new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
    
    /**
     * NAME: addOrder
     * PURPOSE: This method will add new Order record.
     * PARAMETERS: String itemN, String Remark, int qty, String Un, String Department, and String Datee
     * RETURN VALUE: void
     */
    public void addOrder(String itemN, String Remark, int qty, String Un, String Department, String Datee) {      
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
       
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseAddOrder.php?itemN=" + itemN + "&qty=" + qty + "&Remark=" +Remark+ "&date="+date+"&Un="+Un+"&Department="+Department+"&Datee="+Datee+"";
        JavaHTTPConnectionReader javaHTTPConnectionReader = new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
    
      /**
     * NAME: updateReq
     * PURPOSE: This method will update new Requisition record.
     * PARAMETERS: String Name,String qty,String desc,String ID
     * RETURN VALUE: void
     */
    public void  updateReq(String Name,String qty,String desc,String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionUpdate.php?Name="+Name+"&qty="+qty+"&desc="+desc+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
      /**
     * NAME: updateReqEq
     * PURPOSE: This method will update new Requisition Equipment record.
     * PARAMETERS: String Name,String qty,String desc,String ID
     * RETURN VALUE: void
     */
    public void  updateReqEq(String Name,String qty,String desc,String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionUpdateeQ.php?Name="+Name+"&qty="+qty+"&desc="+desc+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
     /**
     * NAME: updateReqMed
     * PURPOSE: This method will update new Requisition Medical record.
     * PARAMETERS: String Name,String qty,String desc,String unit,String ID
     * RETURN VALUE: void
     */
    public void  updateReqMed(String Name,String qty,String desc,String unit,String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionUpdateMed.php?Name="+Name+"&qty="+qty+"&desc="+desc+"&unit="+unit+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
     /**
     * NAME: deleteReq
     * PURPOSE: This method will delete Requisition record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
      public void deleteReq(String ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionDeleteReq.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
       /**
     * NAME: CancelReq
     * PURPOSE: This method will cancel Requisition record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
      public void CancelReq(String ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionCancelReq.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
      
      /**
     * NAME: getRequistionEquip
     * PURPOSE: This method will get Requisition Equipment record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
      
      public void getRequistionEquip(){      
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionEquip.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/accounting/purchaseRequisitionEquip.json", "purchaseRequisitionEquip");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
      
      /**
     * NAME: getRequistionMeds
     * PURPOSE: This method will get Requisition Medical record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
      
      public void getRequistionMeds(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/purchaseRequisitionMed.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/accounting/purchaseRequisitionMed.json", "purchaseRequisitionMed");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
      
    /**
     * NAME: getRequest 
     * PURPOSE: This method will get Request record.
     * PARAMETERS: String reqType
     * RETURN VALUE: void
     */
      
    public void getRequest(String reqType) throws IOException {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseOrder.php?reqType=" + reqType + "";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        

        if (reqType.equals("Linens")) {
            try {
                download.downloadData("http://" + IP + "/HIMSv2/accounting/getRequestLinens.json", "getRequestLinens");
            } catch (IOException ex) {
                System.out.println("linens error");
            }
        } else if (reqType.equals("Medicine")) {
            try {
                download.downloadData("http://" + IP + "/HIMSv2/accounting/getRequestMedicine.json", "getRequestMedicine");
            } catch (IOException ex) {
                System.out.println("medicine error");
            }
        } else {
            try {
                download.downloadData("http://" + IP + "/HIMSv2/accounting/purchaseOrder.json", "purchaseOrder");
            } catch (IOException ex) {
                System.out.println("equipment error");
            }
        }

    }
    
    /**
     * NAME: getOrderApproved 
     * PURPOSE: This method will get Approved Orders record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getOrderApproved() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseOrderApproved.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/accounting/purchaseOrderApproved.json", "purchaseOrderApproved");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }

    /**
     * NAME: getOrderDisApproved 
     * PURPOSE: This method will get Disapproved Orders record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getOrderDisApproved() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseOrderDisapproved.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/accounting/purchaseOrderDisapproved.json", "purchaseOrderDisapproved");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }
    
    /**
     * NAME: getApproved 
     * PURPOSE: This method will get Approved record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getApproved() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseRequisitionApproved.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/accounting/purchaseRequisitionApproved.json", "purchaseRequisitionApproved");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }

    /**
     * NAME: getOrderDisApproved 
     * PURPOSE: This method will get Disapproved record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getDisApproved() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/accounting/purchaseRequisitionDisapproved.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/accounting/purchaseRequisitionDisapproved.json", "purchaseRequisitionDisapproved");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }
      
}
