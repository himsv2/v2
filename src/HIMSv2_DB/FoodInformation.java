/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class FoodInformation {
    /**
     * NAME: getFood
     * PURPOSE: connects to the servers php to get the records of Food and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getFood(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/food/food.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/food/food.json", "food");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    /**
     * NAME: addFood
     * PURPOSE: This method will add new food record.
     * PARAMETERS: String food name and String description 
     * RETURN VALUE: void
     */
    
    public void addFood(String Name, String desc){
        float p = 0;
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/food/addFood.php?Name="+Name+"&desc="+desc+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateFood
     * PURPOSE: This method will update food record.
     * PARAMETERS: String food name and String description and food ID to update
     * RETURN VALUE: void
     */
    
    public void updateFood(String Name, String desc, int ID){
        float p = 0;
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/food/updateFood.php?Name="+Name+"&desc="+desc+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: DeleteFood
     * PURPOSE: This method will delete food record.
     * PARAMETERS: int ID of the food to be deleted
     * RETURN VALUE: void
     */
    
    public void deleteFood( int ID){       
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/food/deleteFood.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
}
