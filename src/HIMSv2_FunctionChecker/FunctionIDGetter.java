/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.AccountingInformation;
import HIMSv2_DB.EquipmentInformation;
import HIMSv2_DB.FoodInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.MedicineSchedInformation;
import HIMSv2_DB.MedicineScheduleUnavailableInformation;
import HIMSv2_DB.PatientInformation;
import HIMSv2_DB.ReportInformation;
import HIMSv2_DB.RequestInformation;
import HIMSv2_DB.RestrictionInformation;
import HIMSv2_DB.SupplierInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionIDGetter {

    /**
     * NAME: getbuildingId 
     * PURPOSE: This method will get the ID of the given building name. 
     * PARAMETERS: String building name RETURN 
     * VALUE: int id of the building name
     */
    public int getbuildingId(String buildingName) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("buildName").equals(buildingName))) {
                String cute = (String) json.get("buildId");
                roomID = Integer.parseInt(cute);
            }

        }
        return roomID;
    }

    /**
     * NAME: getID 
     * PURPOSE: This method will get the ID of the given room name.
     * PARAMETERS: String room name 
     * RETURN VALUE: int id of the room name
     */
    public int getId(String roomName) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("rmName").equals(roomName))) {

                String cute = (String) json.get("rmId");
                roomID = Integer.parseInt(cute);

            }

        }
        return roomID;
    }

    /**
     * NAME: getFloor 
     * PURPOSE: This method will get the ID of the given floor name. 
     * PARAMETERS: String floor name 
     * RETURN VALUE: int id of the floor name
     */
    public int getFloor(String floorName) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\floor.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("floorName").equals(floorName))) {

                String ID = (String) json.get("floor_id");
                roomID = Integer.parseInt(ID);

            }

        }
        return roomID;
    }

    /**
     * NAME: getRoomType 
     * PURPOSE: This method will get the ID of the given room type name. 
     * PARAMETERS: String room type name 
     * RETURN VALUE: int id of the roomtype name
     */
    public int getRoomType(String rtName) throws FileNotFoundException, IOException, ParseException {
        int roomTypeID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\roomType.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomTypeID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("rtName").equals(rtName))) {

                String ID = (String) json.get("rtId");
                roomTypeID = Integer.parseInt(ID);

            }

        }
        return roomTypeID;
    }

    /**
     * NAME: getBed 
     * PURPOSE: This method will get the ID of the given bed name.
     * PARAMETERS: String bed name 
     * RETURN VALUE: int id of the bed name
     */
    public int getBed(String bName) throws FileNotFoundException, IOException, ParseException {
        int bedID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\bed.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("bedName").equals(bName))) {

                String ID = (String) json.get("bedId");
                bedID = Integer.parseInt(ID);

            }

        }
        return bedID;
    }

    /**
     * NAME: getNurseStationId 
     * PURPOSE: This method will get the ID of the given building name. 
     * PARAMETERS: String building name 
     * RETURN VALUE: int id ofthe building name
     */

    public int getNurseStationId(String nsName) throws FileNotFoundException, IOException, ParseException {
        int nsID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("nstName").equals(nsName)) {
                String cute = (String) json.get("nstId");
                nsID = Integer.parseInt(cute);
            }

        }
        return nsID;
    }

    /**
     * NAME: getNurseStation
     * PURPOSE: This method will get the ID of the given nurse station name. 
     * PARAMETERS: String building name 
     * RETURN VALUE: int id of the building name
     */

    public int getNurseStation(String nsId) throws FileNotFoundException, IOException, ParseException {
        int nsID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("stId").equals(nsId)) {
                String cute = (String) json.get("stStationId");
                nsID = Integer.parseInt(cute);
            }

        }
        return nsID;
    }

    /**
     * NAME: getUser 
     * PURPOSE: This method will get the ID of the user.
     * PARAMETERS: no parameter 
     * RETURN VALUE: int id of the user
     */
    public int getUser() throws FileNotFoundException, IOException, ParseException {
        int userID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;

        o = parser.parse(new FileReader("C:\\HIMSv2\\phplog_login.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(0);
        ID = (String) json.get("stId");
        userID = Integer.parseInt(ID);
        return userID;
    }

    /**
     * NAME: getRequestLinens 
     * PURPOSE: This method will get the ID of the
     * selected request. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getRequestLinens(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\getLinensRequest.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(no - 1);
        ID = (String) json.get("reqId");
        reqID = Integer.parseInt(ID);
        return reqID;
    }

    /**
     * NAME: getRequestEquipment 
     * PURPOSE: This method will get the ID of the selected request. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    public int getRequestEquipment(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\getEquipmentRequest.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(no - 1);
        ID = (String) json.get("reqId");
        reqID = Integer.parseInt(ID);
        return reqID;
    }

    /**
     * NAME: getEquipment 
     * PURPOSE: This method will get the ID of the selected equipment. 
     * PARAMETERS: String code 
     * RETURN VALUE: int id of the user
     */
    public int getEquipment(String code) throws FileNotFoundException, IOException, ParseException {
        int equipID = 0;
        JSONParser parser = new JSONParser();
        Object o;

        o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("equipCode").equals(code)) {
                String cute = (String) json.get("equipId");
                equipID = Integer.parseInt(cute);
            }

        }
        return equipID;
    }

    /**
     * NAME: getApproved
     * PURPOSE: This method will get the ID of the selected request. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getApproved(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("equipment");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getEquipmentRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("0")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getDisapproved 
     * PURPOSE: This method will get the ID of the selected request. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getDisapproved(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("equipment");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getEquipmentRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("2")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getPending 
     * PURPOSE: This method will get the ID of the selected request. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */

    public int getPending(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("equipment");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getEquipmentRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("1")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getPending 
     * PURPOSE: This method will get the ID of the selected equipment. 
     * PARAMETERS: String name
     * RETURN VALUE: int id of the user
     */
    public int getEquipt(String name) throws FileNotFoundException, IOException, ParseException {
        int ID = 0;
        int num = 0;
        String q;
        JSONParser parser = new JSONParser();
        Object o;

        EquipmentInformation c = new EquipmentInformation();
        c.getEquipment();

        o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);

            if (json.get("equipName").equals(name)) {
                q = (String) json.get("equipId");
                ID = Integer.parseInt(q);
            }

        }
        return ID;
    }

    /**
     * NAME: getLinen 
     * PURPOSE: This method will get the ID of the selected linen. 
     * PARAMETERS: String code
     * RETURN VALUE: int id of the user
     */
    public int getLinen(String code) throws FileNotFoundException, IOException, ParseException {
        int linenID = 0;
        JSONParser parser = new JSONParser();
        Object o;

        o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("lineItemCode").equals(code)) {
                String cute = (String) json.get("lineId");
                linenID = Integer.parseInt(cute);
            }

        }
        return linenID;
    }

    /**
     * NAME: getPendingLinen 
     * PURPOSE: This method will get the ID of the selected linen. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    public int getPendingLinen(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("linens");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getLinensRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("1")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getApprovedLinen
     * PURPOSE: This method will get the ID of the selected linen. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getApprovedLinen(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("linens");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getLinensRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("0")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getDisapprovedLinen 
     * PURPOSE: This method will get the ID of the selected linen. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    public int getDisapprovedLinen(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getRequestInvetory("linens");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getLinensRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("2")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getReport
     * PURPOSE: This method will get the ID of the selected report. 
     * PARAMETERS:  int no
     * RETURN VALUE: int id of the user
     */

    public int getReport(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\report.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(no - 1);
        ID = (String) json.get("srId");
        reqID = Integer.parseInt(ID);
        return reqID;
    }

    /**
     * NAME: getPatient 
     * PURPOSE: This method will get the ID of the selected patient. 
     * PARAMETERS: String idss
     * RETURN VALUE: int id of the user
     */

    public int getPatient(String idss) throws FileNotFoundException, IOException, ParseException {
        int linenID = 0;
        JSONParser parser = new JSONParser();
        Object o;

        o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("ptId").equals(idss)) {
                String cute = (String) json.get("ptId");
                linenID = Integer.parseInt(cute);
            }

        }
        return linenID;
    }

    /**
     * NAME: getTask 
     * PURPOSE: This method will get the ID of the selected task. 
     * PARAMETERS:  String code
     * RETURN VALUE: int id of the user
     */

    public int getTask(String code) throws FileNotFoundException, IOException, ParseException {
        int taskID = 0;
        JSONParser parser = new JSONParser();
        Object o;

        o = parser.parse(new FileReader("C:\\HIMSv2\\nurseTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("lineItemCode").equals(code)) {
                String cute = (String) json.get("lineId");
                taskID = Integer.parseInt(cute);
            }

        }
        return taskID;
    }

    /**
     * NAME: getRequestMedicine 
     * PURPOSE: This method will get the ID of the selected medicine. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getRequestMedicine(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\getMedicineRequest.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(no - 1);
        ID = (String) json.get("reqId");
        reqID = Integer.parseInt(ID);
        return reqID;
    }

    /**
     * NAME: getMedicine
     * PURPOSE: This method will get the ID of the selected medicine. 
     * PARAMETERS:  String itmName
     * RETURN VALUE: int id of the user
     */
    public int getMedicine(String itmName) throws FileNotFoundException, IOException, ParseException {
        int medID = 0;
        JSONParser parser = new JSONParser();
        Object o;

        o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("medInName").equals(itmName)) {
                String cute = (String) json.get("medInId");
                medID = Integer.parseInt(cute);
            }

        }
        return medID;
    }

    /**
     * NAME: getMedicineApproved 
     * PURPOSE: This method will get the ID of the selected medicine. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    public int getMedicineApproved(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getMedicineRequestInvetory("medicine");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getMedicineRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("0")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMedicineDisapproved 
     * PURPOSE: This method will get the ID of the selected medicine. 
     * PARAMETERS: int no 
     * RETURN VALUE: int id of the user
     */
    public int getMedicineDisapproved(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getMedicineRequestInvetory("medicine");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getMedicineRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("2")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMedicinePending 
     * PURPOSE: This method will get the ID of the selected medicine. 
     * PARAMETERS:int no
     * RETURN VALUE: int id of the user
     */
    public int getMedicinePending(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        RequestInformation a = new RequestInformation();
        a.getMedicineRequestInvetory("medicine");
        o = parser.parse(new FileReader("C:\\HIMSv2\\getMedicineRequestInventory.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if (json.get("reqDone").equals("1")) {
                num++;
                if (num == no) {
                    String cute = (String) json.get("reqId");
                    reqID = Integer.parseInt(cute);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMed 
     * PURPOSE: This method will get the ID of the selected medicine 
     * PARAMETERS: String name
     * RETURN VALUE: int id of the user
     */
    public int getMed(String name) throws FileNotFoundException, IOException, ParseException {
        int ID = 0;
        int num = 0;
        String q;
        JSONParser parser = new JSONParser();
        Object o;

        MedicineInformation c = new MedicineInformation();
        c.getMedicine();

        o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);

            if (json.get("medInName").equals(name)) {
                q = (String) json.get("medInId");
                ID = Integer.parseInt(q);
            }

        }
        return ID;
    }

    /**
     * NAME: getPendingTask 
     * PURPOSE: This method will get the ID of the selected task 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    
    public int getPendingTask(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\nurseTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        json = (JSONObject) k.get(no - 1);
        ID = (String) json.get("tskId");
        reqID = Integer.parseInt(ID);
        return reqID;
    }
    
    /**
     * NAME: getReportList 
     * PURPOSE: This method will get the ID of the selected report 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */

    public int getReportList(int no) throws FileNotFoundException, IOException, ParseException {
        ReportInformation a = new ReportInformation();
        a.getNurseReport();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\report.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("srndel").equals("1")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("srId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMedSchedList
     * PURPOSE: This method will get the ID of the given medicine name. 
     * PARAMETERS: int no and String nID
     * RETURN VALUE: int id of the user
     */
    public int getMedSchedList(int no, String nID) throws FileNotFoundException, IOException, ParseException, NullPointerException {
        MedicineSchedInformation a = new MedicineSchedInformation();
        a.getMedSched();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\medicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("mdDel").equals("1") && json.get("mdNurseId").equals(nID) && json.get("msStatus").equals("1")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("msId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMedSchedListAccomplish
     * PURPOSE: This method will get the ID of the given medicine name. 
     * PARAMETERS: int no and String nID 
     * RETURN VALUE: int id of the user
     */

    public int getMedSchedListAccomplish(int no, String nID) throws FileNotFoundException, IOException, ParseException, NullPointerException {      
        MedicineSchedInformation a = new MedicineSchedInformation();
        a.getMedSched();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\medicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("mdDel").equals("1") && json.get("mdNurseId").equals(nID) && json.get("msStatus").equals("2")) {
                num++;
                if (num == no) {
                    System.out.println("heafafhehehe-------" + no);
                    ID = (String) json.get("msId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getMedSchedListUnAccomplish
     * PURPOSE: This method will get the ID of the given medicine name. 
     * PARAMETERS: int no and String nID 
     * RETURN VALUE: int id of the user
     */

    public int getMedSchedListUnAccomplish(int no, String nID) throws FileNotFoundException, IOException, ParseException, NullPointerException {
        MedicineSchedInformation a = new MedicineSchedInformation();
        a.getMedSched();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\medicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("mdDel").equals("1") && json.get("mdNurseId").equals(nID) && json.get("msStatus").equals("3")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("msId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getIngredients
     * PURPOSE: This method will get the ID of the given ingredient name. 
     * PARAMETERS: String ingredientName
     * RETURN VALUE: int id of the user
     */
    
    public int getIngredients(String ingredientName) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\ingredients.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("IngredientsName").equals(ingredientName))) {

                String ID = (String) json.get("IngredientsId");
                roomID = Integer.parseInt(ID);

            }

        }
        return roomID;
    }

    /**
     * NAME: getFoodId
     * PURPOSE: This method will get the ID of the given Food name. 
     * PARAMETERS: String foodName
     * RETURN VALUE: int id of the user
     */
    
    public int getFoodId(String foodName) throws FileNotFoundException, IOException, ParseException {
        int foodID = 0;
        FoodInformation food = new FoodInformation();
        food.getFood();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\food.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("foodName").equals(foodName))) {
                String cute = (String) json.get("foodId");
                foodID = Integer.parseInt(cute);
            }

        }
        return foodID;
    }

    /**
     * NAME: getRestriction
     * PURPOSE: This method will get the ID of the given restriction name. 
     * PARAMETERS: String fss
     * RETURN VALUE: int id of the user
     */

    public int getRestriction(String fss) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\restriction.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("restName").equals(fss))) {
                String cute = (String) json.get("restId");
                roomID = Integer.parseInt(cute);
            }

        }
        return roomID;
    }

    /**
     * NAME: getRestrictionList
     * PURPOSE: This method will get the ID of the given restriction name. 
     * PARAMETERS: int no
     * RETURN VALUE: int id of the user
     */
    
    public int getRestrictionList(int no) throws FileNotFoundException, IOException, ParseException, NullPointerException {
        RestrictionInformation a = new RestrictionInformation();
        a.getRestriction();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\restriction.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("restDelete").equals("1")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("restId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }

    /**
     * NAME: getmeal
     * PURPOSE: This method will get the ID of the given meal name. 
     * PARAMETERS: String fss
     * RETURN VALUE: int id of the user
     */

    public int getmeal(String fss) throws FileNotFoundException, IOException, ParseException {
        int roomID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; roomID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("mealplanName").equals(fss))) {
                String cute = (String) json.get("mealplanId");
                roomID = Integer.parseInt(cute);
            }

        }
        return roomID;
    }

    /**
     * NAME: getroom
     * PURPOSE: This method will get the ID of the given room name. 
     * PARAMETERS: String fss
     * RETURN VALUE: int id of the user
     */

    public int getroom(String fss) throws FileNotFoundException, IOException, ParseException {
        int mealID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; mealID == 0 || x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("rmName").equals(fss))) {
                String cute = (String) json.get("rmId");
                mealID = Integer.parseInt(cute);
            }

        }
        return mealID;
    }

    /**
     * NAME: getPatRoom
     * PURPOSE: This method will get the ID of the given patient room name. 
     * PARAMETERS: String ID
     * RETURN VALUE: int id of the user
     */
    
    public int getPatRoom(String ID) throws FileNotFoundException, IOException, ParseException {
        int roomPatID = 0;
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patRoom.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0;  x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            if ((json.get("prPat_Id").equals(ID))) {
                String cute = (String) json.get("prId");
                roomPatID = Integer.parseInt(cute);
            }

        }
        return roomPatID;
    }
    
    /**
     * NAME: getMealPlanId
     * PURPOSE: This method will get the ID of the given mealPlan name.
     * PARAMETERS: String mealName
     * RETURN VALUE: integer id of the mealPlan name
     */
    
    public int getMealPlanId(String mealName) throws FileNotFoundException, IOException, ParseException{
        int mealID = 0;
        MealPlanInformation food = new MealPlanInformation();
        food.getMealPlan();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size(); x++){ 
            json = (JSONObject)k.get(x);
            if((json.get("mealplanName").equals(mealName))) {
                String cute = (String) json.get("mealplanId");
                mealID = Integer.parseInt(cute);      
            }
            
        } 
        return mealID;
    }
    
    /**
     * NAME: getSupplierList
     * PURPOSE: This method will get the ID of the given supplier Id.
     * PARAMETERS: integer no
     * RETURN VALUE: integer id of the supplier
     */
    
     public int getSupplierList(int no) throws FileNotFoundException, IOException, ParseException, NullPointerException{
        SupplierInformation a = new SupplierInformation();
        a.getSupplierList();
        int num = 0;
        int suppID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\supplierList.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){      
                json = (JSONObject)k.get(x);
                if(json.get("suppDel").equals("1")){                  
                    num++;
                    if(num == no){
                        ID = (String) json.get("supplier_id");
                       suppID = Integer.parseInt(ID);        
                   }
                }
        }
        return suppID;
        }
      
     /**
     * NAME: getMedSchedPat
     * PURPOSE: This method will get the ID of the given Medicine schedule name.
     * PARAMETERS: int no and String nID
     * RETURN VALUE: integer id of the medicine schedule name
     */
     
    public int getMedSchedPAT(int no, String nID) throws FileNotFoundException, IOException, ParseException, NullPointerException {
        MedicineSchedInformation a = new MedicineSchedInformation();
        a.getMedSched();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\medicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("mdhDel").equals("1") && json.get("msPat_id").equals(nID) && json.get("msStatus").equals("1")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("msId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    }
     
    /**
     * NAME: getuNMedSchedPat
     * PURPOSE: This method will get the ID of the given Medicine schedule name.
     * PARAMETERS: int no and String nID
     * RETURN VALUE: integer id of the medicine schedule name
     */
    
    public int getuNMedSchedPAT(int no, String nID) throws FileNotFoundException, IOException, ParseException, NullPointerException {
        MedicineSchedInformation a = new MedicineSchedInformation();
        a.getMedSched();
        int num = 0;
        int reqID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\unavailmedicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (json.get("umdhDel").equals("1") && json.get("umsPat_id").equals(nID) && json.get("umsStatus").equals("1")) {
                num++;
                if (num == no) {
                    ID = (String) json.get("umsId");
                    reqID = Integer.parseInt(ID);
                }
            }
        }
        return reqID;
    } 
    
    /**
     * NAME: getRequisitionPending
     * PURPOSE: This method will get the ID of the given Requisition name.
     * PARAMETERS: int no 
     * RETURN VALUE: integer id of the requisition name
     */
    
    public int getRequisitionPending(int no) throws FileNotFoundException, IOException, ParseException {
        int reqID = 0;
        int num = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        AccountingInformation a = new AccountingInformation();
        a.getRequistion();
        o = parser.parse(new FileReader("C:\\HIMSv2\\purchaseRequisition.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {

            json = (JSONObject) k.get(x);
            String cute = (String) json.get("pr_id");
            reqID = Integer.parseInt(cute);
        }
        return reqID;
    }
    
     /**
     * NAME: getMedUnavailList 
     * PURPOSE: This method will get the ID of the given unavailable medicine 
     * PARAMETERS: String floor name 
     * RETURN VALUE: integer id of the unavailable medicine 
     */
    
     public int getMedUnavailList(int no) throws FileNotFoundException, IOException, ParseException, NullPointerException{
        MedicineScheduleUnavailableInformation a = new MedicineScheduleUnavailableInformation();
        a.getMedSchedUnavail();
        int num = 0;
        int suppID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\unavailmedicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){      
                json = (JSONObject)k.get(x);
                if(json.get("umdDel").equals("1") && json.get("umsStatus").equals("1") ){                  
                    num++;
                    if(num == no){
                        ID = (String) json.get("umsId");
                       suppID = Integer.parseInt(ID);        
                   }
                }
        }
        return suppID;
        }
     /**
     * NAME: getMedUnavailableUncc 
     * PURPOSE: This method will get the ID of the given unavailable medicine 
     * PARAMETERS: int no
     * RETURN VALUE: integer id of the unavailable medicine 
     */
     
     public int getMedUnavailableUncc(int no) throws FileNotFoundException, IOException, ParseException, NullPointerException{
        MedicineScheduleUnavailableInformation a = new MedicineScheduleUnavailableInformation();
        a.getMedSchedUnavail();
        int num = 0;
        int suppID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\unavailmedicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){      
                json = (JSONObject)k.get(x);
                if(json.get("umdDel").equals("1") && json.get("umsStatus").equals("3") ){                  
                    num++;
                    if(num == no){
                        ID = (String) json.get("umsId");
                       suppID = Integer.parseInt(ID);        
                   }
                }
        }
        return suppID;
        }
    
     
     /**
     * NAME: getMedUnavailableAcc 
     * PURPOSE: This method will get the ID of the given unavailable account
     * PARAMETERS: int no 
     * RETURN VALUE: integer id of the unavailable account
     */
     
     public int getMedUnavailableAcc(int no) throws FileNotFoundException, IOException, ParseException, NullPointerException{
        MedicineScheduleUnavailableInformation a = new MedicineScheduleUnavailableInformation();
        a.getMedSchedUnavail();
        int num = 0;
        int suppID = 0;
        JSONParser parser = new JSONParser();
        Object o;
        String ID;
        o = parser.parse(new FileReader("C:\\HIMSv2\\unavailmedicineTask.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){      
                json = (JSONObject)k.get(x);
                if(json.get("umdDel").equals("1") && json.get("umsStatus").equals("2") ){                  
                    num++;
                    if(num == no){
                        ID = (String) json.get("umsId");
                       suppID = Integer.parseInt(ID);        
                   }
                }
        }
        return suppID;
        }
     
     /**
     * NAME: getRestId 
     * PURPOSE: This method will get the ID of the given restriction name 
     * PARAMETERS: String restName 
     * RETURN VALUE: int id of the restriction name
     */
     
    public int getRestId(String restName) throws FileNotFoundException, IOException, ParseException {
        int foodID = 0;
        RestrictionInformation food = new RestrictionInformation();
        food.getRestriction();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\restriction.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("restName").equals(restName))) {
                String cute = (String) json.get("restId");
                foodID = Integer.parseInt(cute);
            }

        }
        return foodID;
    }
     /**
     * NAME: getRestId 
     * PURPOSE: This method will get the ID of the given restriction name 
     * PARAMETERS: String restName 
     * RETURN VALUE: int id of the restriction name
     */
     
    public int getPatId(String fName,String lName,String mName, String dob) throws FileNotFoundException, IOException, ParseException {
        int ptID = 0;
        PatientInformation pat = new PatientInformation();
        pat.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();

        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if ((json.get("ptFname").equals(fName)) && (json.get("ptLname").equals(lName)) && (json.get("ptMname").equals(mName)) && (json.get("ptDateOfBirth").equals(dob)) ) {
                String patID = (String) json.get("ptId");
                ptID = Integer.parseInt(patID);
            }

        }
        return ptID;
    }
}
