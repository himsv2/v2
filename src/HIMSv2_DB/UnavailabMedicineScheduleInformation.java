/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author albert
 */
public class UnavailabMedicineScheduleInformation {
    public static void main(String[] args)
    {     
         UnavailabMedicineScheduleInformation x = new UnavailabMedicineScheduleInformation();

    }
    
    /**
     * NAME: getunMedSched
     * PURPOSE: connects to the servers php to get the records of Medicine schedule and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getunMedSched(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/unavailmedicineTask.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/unavailMedTask/unavailmedicineTask.json", "unavailmedicineTask");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    /**
     * NAME: addUnAvailMedSched
     * PURPOSE: This method will add new medicine schedule record.
     * PARAMETERS: String d,String medname,String dosage,String un,String qty,String notes, String IDS,String doctorId,String nurseId,String time,String amPm
     * RETURN VALUE: void
     */
    public void addUnAvailMedSched(String d,String medname,String dosage,String un,String qty,String notes, String IDS,String doctorId,String nurseId,String time,String amPm) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/addunavailMedTask.php?d="+d+"&medname="+medname+"&dosage="+dosage+"&un="+un+"&qty="+qty+"&notes="+notes+"&IDS="+IDS+"&doctorId="+doctorId+"&nurseId="+nurseId+"&time="+time+"&amPm="+amPm+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
      /**
     * NAME: updateUnAvailMedSched
     * PURPOSE: This method will update new medicine schedule record.
     * PARAMETERS: String notes,String med,String dos,String unit,String qty,String d,String MedSchedID,String time,String amPm
     * RETURN VALUE: void
     */
    
    public void updateUnAvailMedSched(String notes,String med,String dos,String unit,String qty,String d,String MedSchedID,String time,String amPm){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/updateUnAvailMedicineTask.php?notes="+notes+"&med="+med+"&dos="+dos+"&unit="+unit+"&qty="+qty+"&d="+d+"&MedSchedID="+MedSchedID+"&time="+time+"&amPm="+amPm+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteUnAvailMed
     * PURPOSE: This method will delete medicine schedule record.
     * PARAMETERS: String IDS
     * RETURN VALUE: void
     */
    
      public void deleteUnAvailMed(String IDS ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/deleteUnAvailmedicineTask.php?IDS="+IDS+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
      /**
     * NAME: deleteStatus
     * PURPOSE: This method will delete medicine status record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
      public void deleteStatus(String ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/deleteAccomplishedMed.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
}
