/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class LinensInformation {
    public static void main(String[] args)
    {     
         LinensInformation x = new LinensInformation();

    }
    
    /**
     * NAME: getLinens
     * PURPOSE: connects to the servers php to get the records of linens and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getLinens(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/linens.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/linens/linens.json", "linens");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
     /**
     * NAME: addLinen
     * PURPOSE: This method will add new linens record.
     * PARAMETERS: String item code String name  String quantity , String type , String description and String price
     * RETURN VALUE: void
     */
    
    public void addLinen(String itemCode, String name, String qty, String type, String desc, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/addLinen.php?itemCode="+itemCode+"&name="+name+"&qty="+qty+"&type="+type+"&desc="+desc+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    /**
     * NAME: deleteLinen
     * PURPOSE: This method will delete new linens record.
     * PARAMETERS: int linen ID to be deleted
     * RETURN VALUE: void
     */
    
    public void deleteLinen(int LinenID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/deleteLinen.php?LinenID="+LinenID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
     /**
     * NAME: updateLinen
     * PURPOSE: This method will update new Equipment record.
     * PARAMETERS: String item Code, String name, String quantity, String type, String description,String price, int linen ID
     * RETURN VALUE: void
     */
    
    public void updateLinen(String itemCode, String name, String qty, String type, String desc,String price,int LinenID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/updateLinen.php?itemCode="+itemCode+"&name="+name+"&qty="+qty+"&type="+type+"&desc="+desc+"&price="+price+"&LinenID="+LinenID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: dupdateQuantity
     * PURPOSE: This method will delete the record of the linens record.
     * PARAMETERS: int Equipment ID and quantity.
     * RETURN VALUE: void
     */
    
    public void updateQuantity(int ID, int qty){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/updateLinensQty.php?ID="+ID+"&qty="+qty+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
       
    /**
     * NAME: getRequistionLinen
     * PURPOSE: This method will get the record of the Requisition record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getRequistionLinen(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/linens/purchaseRequisitionLinen.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/linens/purchaseRequisitionLinen.json", "purchaseRequisitionLinen");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}
