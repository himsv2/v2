/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author CAMINERO
 */
public class MealPlanInformation {
    public static void main(String[] args)
    {     
         MealPlanInformation x = new MealPlanInformation();

    }
    
    /**
     * NAME: getMealPlan
     * PURPOSE: connects to the servers php to get the records of meal plan and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getMealPlan(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/mealPlan.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/mealPlan/mealPlan.json", "mealPlan");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
   
     /**
     * NAME: getMealPlanId
     * PURPOSE: connects to the server to get the records of mealPlan and download the record
     * from the server to this PC.
     * PARAMETERS: integer Id
     * RETURN VALUE: void
     */
    
    public void getMealPlanId(int ID){      
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/getMealPlanId.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/mealPlan/getMealPlanId.json", "getMealPlanId");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addMealPlan
     * PURPOSE: This method will add new mealPlan record.
     * PARAMETERS: String meal name String price and String meal description 
     * RETURN VALUE: void
     */  
    
    public void addMealPlan(String Name, String desc, String price){
        String date;
        System.out.println(Name+","+desc);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/addMealPlan.php?Name="+Name+"&desc="+desc+"&price="+price+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: getMealPlanRestId
     * PURPOSE: This method will get mealPlan Restriction ID record.
     * PARAMETERS: integer ID 
     * RETURN VALUE: void
     */  
    
    public void getMealPlanRestId(int ID){
        
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/getMealPlanRestId.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/mealPlan/getMealPlanRestId.json", "getMealPlanRestId");
        } catch (IOException ex) {
            System.out.println("error");
        }  
        System.out.println("manakog doooownloadddddd");
    }
    /**
     * NAME: addMealFood
     * PURPOSE: This method will add new mealItem record.
     * PARAMETERS: integer Id , integer foodID
     * RETURN VALUE: void
     */
    
    public void addMealFood(int ID, int foodID){      
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/addFoodMealPlan.php?patID="+ID+"&ingID="+foodID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
    }
    
   
    
    /**
     * NAME: DeleteMealPlan
     * PURPOSE: This method will delete the mealPlan record.
     * PARAMETERS: integer Id
     * RETURN VALUE: void
     */
    
    public void deleteMealPlan( int ID){    
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/deleteMealPlan.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
    
    /**
     * NAME: updateMealPlan
     * PURPOSE: This method will update mealPlan record.
     * PARAMETERS: String meal name, String meal description and integer ID
     * RETURN VALUE: void
     */
    
    public void updateMealPlan(String Name, String desc,String price, int ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/updateMealPlan.php?Name="+Name+"&desc="+desc+"&price="+price+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
   /**
     * NAME: removeFood
     * PURPOSE: This method will remove the mealItem record.
     * PARAMETERS: integer foodId, integer mealId
     * RETURN VALUE: void
     */ 
    
   public void removeFood(int FoodID, int mealID){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/removeFood.php?foodID="+FoodID+"&mealID="+mealID+"";
        new JavaHTTPConnectionReader(myUrl);       
        System.out.println(myUrl);  
    } 
    
   /**
     * NAME: addMealRest
     * PURPOSE: This method will add new meal Restriction record.
     * PARAMETERS: integer Id , integer restID
     * RETURN VALUE: void
     */
    
    public void addMealRest(int ID, int restID){     
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/addFoodMealPlanRestriction.php?patID="+ID+"&restID="+restID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);      
    }
   
    /**
     * NAME: removeRest
     * PURPOSE: This method will remove the mealItem record.
     * PARAMETERS: integer restId, integer Id
     * RETURN VALUE: void
     */ 
    
   public void removeRest(int restID, int ID){
        
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/mealPlan/removeRestriction.php?restID="+restID+"&ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);       
        System.out.println(myUrl);  
    } 
}
