/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB.Download_JSON;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author albert
 */
public class MedicineScheduleUnavailableInformation {
    public static void main(String[] args)
    {     
         MedicineScheduleUnavailableInformation x = new MedicineScheduleUnavailableInformation();

    }
    
    /**
     * NAME: getMedSchedUnavail
     * PURPOSE: connects to the servers php to get the records of Unavailable Medicine and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getMedSchedUnavail(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/unavailmedicineTask.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/unavailMedTask/unavailmedicineTask.json", "unavailmedicineTask");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
     /**
     * NAME: delMedSched
     * PURPOSE: This method will delete Medicine Schedule record
     * PARAMETERS: int ID, String status
     * RETURN VALUE: void
     */
    
    public void delMedSched(int ID, String status ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/deleteAccomplishedUnaccomplished.php?umsId="+ID+"&umsStatus="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
     /**
     * NAME: setAccomplishUnaccomplish
     * PURPOSE: This method will set Accomplish or Un-accomplish Medicine Schedule record
     * PARAMETERS: int ID, String status
     * RETURN VALUE: void
     */
    
    public void setAccomplishUnaccomplish(int ID, String status ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/unavailMedTask/setAccomplishUnaccomplish.php?umsId="+ID+"&umsStatus="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
}
