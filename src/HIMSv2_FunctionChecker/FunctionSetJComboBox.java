/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.BloodInformation;
import HIMSv2_DB.BuildingInformation;
import HIMSv2_DB.DocFeeInformation;
import HIMSv2_DB.EquipmentInformation;
import HIMSv2_DB.FloorInformation;
import HIMSv2_DB.IngredientsInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.NurseStationInformation;
import HIMSv2_DB.PatientInformation;
import HIMSv2_DB.RestrictionInformation;
import HIMSv2_DB.RoomInformation;
import HIMSv2_DB.RoomTypeInformation;
import HIMSv2_DB.StaffInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import javax.swing.JComboBox;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionSetJComboBox {
    
    /**
     * NAME: building
     * PURPOSE: this will get the information of the building from the database and put the building name in the combo box.
     * PARAMETERS: JComboBox building
     * RETURN VALUE: no return values.
     */
    
    public void building(JComboBox building) throws IOException, ParseException{
        BuildingInformation a = new BuildingInformation();
        a.getBuilding();
        building.removeAllItems();
        building.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            building.addItem((String) json.get("buildName"));
        }
            
    }
    
    /**
     * NAME: nurseStation
     * PURPOSE: this will get the information of the nurse station from the database and put the nurse station name in the combo box.
     * PARAMETERS: JcomboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void nurseStation(JComboBox ns) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        a.getRoom();   
        ns.removeAllItems();
        ns.addItem("ALL");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            ns.addItem((String) json.get("nstName"));
        }
            
    }
    
    /**
     * NAME: nurseStation2
     * PURPOSE: this will get the information of the nurse station from the database and put the nurse station name in the combo box.
     * PARAMETERS: JcomboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void nurseStation2(JComboBox ns) throws IOException, ParseException{
        NurseStationInformation a = new NurseStationInformation();
        a.getNurseStation();
        ns.removeAllItems();
        ns.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            ns.addItem((String) json.get("nstName"));
        }
            
    }
    
    /**
     * NAME: room
     * PURPOSE: this will get the information of the room from the database and put the room name in the combo box.
     * PARAMETERS: JcomboBox room
     * RETURN VALUE: no return values.
     */
    
    public void room(JComboBox room) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        a.getRoom();   
        room.removeAllItems();
        room.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            room.addItem((String) json.get("rmName"));
        }
            
    }
    
    /**
     * NAME: equipment
     * PURPOSE: this will get the information of the equipment from the database and put the equipment name in the combo box.
     * PARAMETERS: JcomboBox room
     * RETURN VALUE: no return values.
     */
    
    public void equipment(JComboBox room) throws IOException, ParseException{
        EquipmentInformation a = new EquipmentInformation();
        a.getEquipment();   
        room.removeAllItems();
        room.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            room.addItem((String) json.get("equipName"));
        }
            
    }
    /**
     * NAME: room type
     * PURPOSE: this will get the information of the room type from the database and put the room type name in the combo box.
     * PARAMETERS: JComboBox room type.
     * RETURN VALUE: no return values.
     */
    
    public void roomType(JComboBox roomType) throws IOException, ParseException{
        RoomTypeInformation a = new RoomTypeInformation();
        a.getRoomType();   
        roomType.removeAllItems();
        roomType.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\roomType.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            roomType.addItem((String) json.get("rtName"));
        }
            
    }
    
    /**
     * NAME: floor
     * PURPOSE: this will get the information of the floor from the database and put the floor name in the combo box.
     * PARAMETERS: JComboBox floor.
     * RETURN VALUE: no return values.
     */
    
    public void floor(JComboBox floor) throws IOException, ParseException{
        FloorInformation a = new FloorInformation();
        a.getFloor();   
        floor.removeAllItems();
        floor.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\floor.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            floor.addItem((String) json.get("floorName"));
        }
            
    }
    
    /**
     * NAME: setRoom
     * PURPOSE: this will get the information of the room from the database and put the room name in the combo box.
     * PARAMETERS: JComboBox room and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setRoom(JComboBox room, String name) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String cmp;
        
        a.getRoom();   
        room.removeAllItems();
        room.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("rmName"))){
                room.addItem((String) json.get("rmName"));
            }   
        }
            
    }
    /**
     * NAME: setBuilding
     * PURPOSE: this will get the information of the building from the database and put the building name in the combo box.
     * PARAMETERS: JComboBox building and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setbuilding(JComboBox building, String name) throws IOException, ParseException{
        BuildingInformation a = new BuildingInformation();
        a.getBuilding();
        building.removeAllItems();
        building.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("buildName"))){
                building.addItem((String) json.get("buildName"));
            } 
            
        }
            
    }
    /**
     * NAME: setRoomType
     * PURPOSE: this will get the information of the room type from the database and put the room type name in the combo box.
     * PARAMETERS: JComboBox building and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setRoomType(JComboBox roomType, String name) throws IOException, ParseException{
        RoomTypeInformation a = new RoomTypeInformation();
        a.getRoomType();   
        roomType.removeAllItems();
        roomType.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\roomType.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("rtName"))){
                roomType.addItem((String) json.get("rtName"));
            } 
            
        }
            
    }
    
    /**
     * NAME: linens
     * PURPOSE: this will get the information of the linens from the database and put the linens name in the combo box.
     * PARAMETERS: JComboBox linens
     * RETURN VALUE: no return values.
     */
    
    public void Linens(JComboBox linens) throws IOException, ParseException{
        RoomTypeInformation a = new RoomTypeInformation();
        a.getRoomType();   
        linens.removeAllItems();
        linens.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
             linens.addItem((String) json.get("lineName"));  
        }     
    }
    
    /**
     * NAME: setlinens
     * PURPOSE: this will get the information of the linens from the database and put the linens name in the combo box.
     * PARAMETERS: JComboBox lines and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setLinens(JComboBox linens, String name) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String cmp;
        
        a.getRoom();   
        linens.removeAllItems();
        linens.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("lineName"))){
                linens.addItem((String) json.get("lineName"));
            }   
        }
            
    }
    
    /**
     * NAME: setNurseStation
     * PURPOSE: this will get the information of the nurse Station from the database and put the nurse station name in the combo box.
     * PARAMETERS: JComboBox nurse station and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setNurseStation(JComboBox ns, String name) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String cmp;
        
        a.getRoom();   
        ns.removeAllItems();
        ns.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("nstName"))){
                ns.addItem((String) json.get("nstName"));
            }   
        }       
    }
    
    /**
     * NAME: setEquipment
     * PURPOSE: this will get the information of the equipment from the database and put the equipment name in the combo box.
     * PARAMETERS: JComboBox nurse station and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setEquipment(JComboBox equipment, String name) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String cmp;
        
        a.getRoom();   
        equipment.removeAllItems();
        equipment.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("equipName"))){
                equipment.addItem((String) json.get("equipName"));
            }   
        }       
    }
     /**
     * NAME: nurse
     * PURPOSE: this will get the information of the nurse from the database and put the nurse name in the combo box.
     * PARAMETERS: JcomboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void nurse(JComboBox ns) throws IOException, ParseException{
        StaffInformation a = new StaffInformation();
        a.getStaff();   
        ns.removeAllItems();
        ns.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
           if ((json.get("stPosition").equals("nurseStaff"))) {
                ns.addItem((String) json.get("stId"));
            }
        }
            
    }
    /**
     * NAME: nurse
     * PURPOSE: this will get the information of the nurse from the database and put the nurse name in the combo box.
     * PARAMETERS: JcomboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void nurseIDName(JComboBox ns, JComboBox nsName) throws IOException, ParseException{
        StaffInformation a = new StaffInformation();
        a.getStaff();   
        String name;
        ns.removeAllItems();
        ns.addItem("--.--.--");
        nsName.removeAllItems();
        nsName.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
           if ((json.get("stPosition").equals("nurseStaff"))) {
                ns.addItem((String) json.get("stId"));
                name = (json.get("stLname"))+","+ (json.get("stFname"));
                nsName.addItem(name);
            }
        }
            
    }
    
    /**
     * NAME: setnurse
     * PURPOSE: this will get the information of the nurse from the database and put the nurse name in the combo box.
     * PARAMETERS: JComboBox ns and String id
     * RETURN VALUE: no return values.
     */
    
    public void setnurse(JComboBox ns,String id) throws IOException, ParseException{
        StaffInformation a = new StaffInformation();
        String cmp;       
        a.getStaff();   
        ns.removeAllItems();
        ns.addItem(id);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!id.contentEquals((String) json.get("stId"))){
                ns.addItem((String) json.get("stId"));
            }   
        }          
    }
    /**
     * NAME: setns
     * PURPOSE: this will get the information of the nurse station from the database and put the nurse station name in the combo box.
     * PARAMETERS: JComboBox ns and string nurse station id.
     * RETURN VALUE: no return values.
     */
    
    public void setns(JComboBox ns, String nsID) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String name1;
      
        int ID;
        
        a.getRoom();   
        ns.removeAllItems();
        ID = Integer.parseInt(nsID);
        FunctionNameGetter b = new FunctionNameGetter();
        name1 = b.getNurseStation(ID);
        ns.addItem(name1);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            
            if(!(json.get("nstName").equals(name1))){
                ns.addItem((String) json.get("nstName"));
            }    
        }           
    }
      /**
     * NAME: patient
     * PURPOSE: this will get the information of the patient from the database and put the patient name in the combo box.
     * PARAMETERS: JComboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void patient(JComboBox ns) throws IOException, ParseException{
        PatientInformation a = new PatientInformation();
        a.getPatient();   
        ns.removeAllItems();
        ns.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            ns.addItem((String) json.get("ptId"));
        }
            
    }
     /**
     * NAME: setPatient
     * PURPOSE: this will get the information of the patient from the database and put the patient name in the combo box.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values.
     */
    
    public void setPatient (JComboBox ns,String id) throws IOException, ParseException{
        PatientInformation a = new  PatientInformation();
        String cmp;       
        a.getPatient();   
        ns.removeAllItems();
        ns.addItem(id);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!id.contentEquals((String) json.get("ptId"))){
                ns.addItem((String) json.get("ptId"));
            }   
        }      
    }

    /**
     * NAME: medicine
     * PURPOSE: this will get the information of the medicine from the database and put the medicine name in the combo box.
     * PARAMETERS: JComboBox room
     * RETURN VALUE: no return values.
     */
    
    public void medicine(JComboBox room) throws IOException, ParseException{
        MedicineInformation a = new MedicineInformation();
        a.getMedicine();   
        room.removeAllItems();
        room.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            room.addItem((String) json.get("medInName"));
        }
            
    }



/**
     * NAME: medicine
     * PURPOSE: this will get the information of the medicine from the database and put the medicine name in the combo box.
     * PARAMETERS: JComboBox medicine
     * RETURN VALUE: no return values.
     */
    
    public void Medicine(JComboBox medicine) throws IOException, ParseException{
        RoomTypeInformation a = new RoomTypeInformation();
        a.getRoomType();   
        medicine.removeAllItems();
        medicine.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
             medicine.addItem((String) json.get("medInName"));  
        }     
    }


/**
     * NAME: setMedicine
     * PURPOSE: this will get the information of the medicine from the database and put the medicine name in the combo box.
     * PARAMETERS: JComboBox nurse station and string name.
     * RETURN VALUE: no return values.
     */
    
    public void setMedicine(JComboBox medicine, String name) throws IOException, ParseException{
        RoomInformation a = new RoomInformation();
        String cmp;
        
        a.getRoom();   
        medicine.removeAllItems();
        medicine.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            if(!name.contentEquals((String) json.get("medInName"))){
                medicine.addItem((String) json.get("medInName"));
            }   
        }       
    }
    /**
     * NAME: ingredients
     * PURPOSE: this will get the information of the ingredients from the database and put the ingredients name in the combo box.
     * PARAMETERS: JComboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void ingredients(JComboBox ns) throws IOException, ParseException{
        IngredientsInformation a = new IngredientsInformation();
        a.getIngredients();
        ns.removeAllItems();
        ns.addItem("--,--,--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\ingredients.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            ns.addItem((String) json.get("IngredientsName"));
        }
            
    }
  /**
     * NAME: restrict 
     * PURPOSE: this will get the information of the restriction from the
     * database and put the room name in the combo box. 
     * PARAMETERS: JComboBox rest
     * RETURN VALUE: no return values.
     */

    public void restrict(JComboBox rest) throws IOException, ParseException {
        RestrictionInformation a = new RestrictionInformation();
        a.getRestriction();
        rest.removeAllItems();
        rest.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\restriction.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            rest.addItem((String) json.get("restName"));
        }
    }
   /**
     * NAME: setDoctor 
     * PURPOSE: this will get the information of the doctor from the database and put the doctor name in the combo box. 
     * PARAMETERS: JComboBox ns and String id. 
     * RETURN VALUE: no return values.
     */

    public void setdoctor(JComboBox ns, String id) throws IOException, ParseException {
        StaffInformation a = new StaffInformation();
        String cmp;
        a.getStaff();
        ns.removeAllItems();
        ns.addItem(id);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (!id.contentEquals((String) json.get("stId"))) {
                if ((json.get("stPosition").equals("doctor"))) {
                ns.addItem((String) json.get("stId"));
                }
            }
        }
    }
/**
     * NAME: doctor 
     * PURPOSE: this will get the information of the doctor from the database and put the doctor name in the combo box. 
     * PARAMETERS: JComboBox doctor. 
     * RETURN VALUE: no return values.
     */
    public void doctor(JComboBox doctorId, JComboBox doctorName) throws IOException, ParseException {
        StaffInformation a = new StaffInformation();
        String name;
        a.getStaff();
        int check = 0;
        doctorId.removeAllItems();
        doctorId.addItem("--.--.--");
        doctorName.removeAllItems();
        doctorName.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            check = this.docfee((String) json.get("stId"));
            if ((json.get("stPosition").equals("doctor")) && check == 1  ) {
                name = (json.get("stLname"))+","+ (json.get("stFname"));
                doctorId.addItem((String) json.get("stId"));
                doctorName.addItem(name);
            }
        }
    }
    
    public int docfee(String ID) throws FileNotFoundException, IOException, ParseException{
        int isZero = 0;
        JSONParser parser = new JSONParser();
        DocFeeInformation info = new DocFeeInformation();
        info.getDocFee();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\DocFee.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            
            if ((json.get("doc_Id").equals(ID)) && !(json.get("docFee").equals("0"))  ) {
                isZero = 1;
            }
        }
        return isZero;
    }
    
    /**
     * NAME: docAndHeadnurse 
     * PURPOSE: this will get the information of the doctor and head nurse from the database and put the doctor and head nurse name in the combo box. 
     * PARAMETERS: JComboBox headnurseId . 
     * RETURN VALUE: no return values.
     */
    public void docAndHeadnurse(JComboBox headnurseId) throws IOException, ParseException{
        StaffInformation a = new StaffInformation();
        a.getStaff();   
        headnurseId.removeAllItems();
        headnurseId.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\staff.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
           if ((json.get("stPosition").equals("headnurse")) ||(json.get("stPosition").equals("doctor")) ) {
                headnurseId.addItem((String) json.get("stId"));
            }
        }     
    }
    /**
     * NAME: meal
     * PURPOSE: this will get the information of the meal from the database and put the meal name in the combo box. 
     * PARAMETERS: JComboBox meal. 
     * RETURN VALUE: no return values.
     */
    public void meal(JComboBox meal) throws IOException, ParseException {
        MealPlanInformation a = new MealPlanInformation();
        a.getMealPlan();
        meal.removeAllItems();
        meal.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
                meal.addItem((String) json.get("mealplanName"));
        }
    }
    /**
     * NAME:setmeal 
     * PURPOSE: this will get the information of the meal from the database and put the meal name in the combo box. 
     * PARAMETERS: JComboBox ns and String name
     * RETURN VALUE: no return values.
     */

    public void setmeal(JComboBox ns, String name) throws IOException, ParseException {
        MealPlanInformation a = new MealPlanInformation();

        a.getMealPlan();
        ns.removeAllItems();
        ns.addItem(name);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
        Iterator<String> i = k.iterator();
        for (int x = 0; x < k.size(); x++) {
            json = (JSONObject) k.get(x);
            if (!name.contentEquals((String) json.get("mealplanName"))) {
                ns.addItem((String) json.get("mealplanName"));
            }
        }
    }
    
    /**
     * NAME: bloodType
     * PURPOSE: this will get the information of the blood type from the database and put the blood type name in the combo box.
     * PARAMETERS: JComboBox bloodType
     * RETURN VALUE: no return values.
     */
    
    public void bloodType(JComboBox bloodType) throws IOException, ParseException{
        BloodInformation a = new BloodInformation();
        a.getBloodInventory();   
        bloodType.removeAllItems();
        bloodType.addItem("--.--.--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\getBloodApprove.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            bloodType.addItem((String) json.get("donBloodtype"));
        }
            
    }
    
    
    
    /**
     * NAME: foods
     * PURPOSE: this will get the information of the foods from the database and put the foods name in the combo box.
     * PARAMETERS: JComboBox ns
     * RETURN VALUE: no return values.
     */
    
    public void foods(JComboBox ns) throws IOException, ParseException{
        IngredientsInformation a = new IngredientsInformation();
        a.getIngredients();
        ns.removeAllItems();
        ns.addItem("--,--,--");
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\food.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0;x < k.size();x++){
            json = (JSONObject)k.get(x);
            ns.addItem((String) json.get("foodName"));
        }
            
    }

}
