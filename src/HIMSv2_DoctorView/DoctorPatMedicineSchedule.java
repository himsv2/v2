package HIMSv2_DoctorView;

import HIMSv2_HeadnurseView.*;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.MedicineScheduleInformation;
import HIMSv2_DB.UnavailabMedicineScheduleInformation;
import HIMSv2_FunctionChecker.FunctionNameGetter;
import HIMSv2_FunctionChecker.FunctionSearch;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public final class DoctorPatMedicineSchedule extends javax.swing.JFrame {

    String patId, patNames, ids;
    DoctorPatientViewDetails a;

    public void setCenterScreen() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    /**
     * Creates new form adminAddBedView
     */
    public DoctorPatMedicineSchedule() {
        initComponents();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setCenterScreen();
        this.setResizable(false);
    }

    public void setVb(DoctorPatientViewDetails a) {
        try {
            this.a = a;
            loadMeds();
        } catch (IOException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * NAME: data PURPOSE: gets the name of the items and nurse station from the
     * database and put it in the combo box. PARAMETERS: no parameters RETURN
     * VALUE: void
     */
    public void data(String IDS) throws IOException, FileNotFoundException, ParseException {
        String patients;
        patId = IDS;
        FunctionNameGetter a = new FunctionNameGetter();
        patients = a.getPat(Integer.parseInt(patId));
        patient.setText(patId);
        patName.setText(patients);
        loadMedSched();
        loadMeds();
        loadUnavailMedSched();
    }

    /**
     * NAME: loadMedSched 
     * PURPOSE: it will displays the data that we get from
     * the database.
     * PARAMETERS: no parameters. 
     * RETURN VALUE: no return values
     */
    public void loadMedSched() throws FileNotFoundException, IOException {
        String med, stat;
        int num = 0;
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel) tblTask.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        MedicineScheduleInformation meds = new MedicineScheduleInformation();
        meds.getMedSched();
        try {
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicineTask.json"));

            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
            Iterator<String> i = k.iterator();

            for (int x = 0; x < k.size(); x++) {

                json = (JSONObject) k.get(x);
                if (json.get("msPat_id").equals(patId) && json.get("mdhDel").equals("1")) {

                    FunctionNameGetter a = new FunctionNameGetter();
                    med = (String) json.get("msMedId");
                    med = a.getMed(Integer.parseInt(med));
                    stat = (String) json.get("msStatus");

                    if (stat.equals("0")) {
                        stat = "Canceled";
                    } else if (stat.equals("1")) {
                        stat = "Pending";
                    } else if (stat.equals("2")) {
                        stat = "Done";
                    } else {
                        stat = "Unaccomplished";
                    }

                    num++;
                    model.addRow(new Object[]{num, med, (String) json.get("msDate"), (String) json.get("msTime")+" "+(String) json.get("msDay"), stat});
                }
            }

        } catch (ParseException ex) {
            //  Logger.getLogger(Patient.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error");
        }
    }

    /**
     * NAME: loadMeds 
     * PURPOSE: it will displays the data that we get from the
     * database. 
     * PARAMETERS: no parameters. 
     * RETURN VALUE: no return values
     */
    public void loadMeds() throws FileNotFoundException, IOException {
        String medname;
        int num =0 ;
        MedicineInformation b = new MedicineInformation();
        b.getMedicine();
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel) tblMed.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        try {

            Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));

            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
            Iterator<String> i = k.iterator();

            for (int x = 0; x < k.size(); x++) {

                json = (JSONObject) k.get(x);

                medname = (String) json.get("medInName");
                medname = medname.replace("_", " ");

                num++;
                model.addRow(new Object[]{num, medname, json.get("medInDosage"), json.get("medInUnitMeasure"), json.get("medInQuantiy")});

            }
        } catch (ParseException ex) {

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator3 = new javax.swing.JSeparator();
        myUI = new javax.swing.JPanel();
        btnCancel = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        noAvail = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        patient = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        patName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTask = new javax.swing.JTable();
        unavailable = new javax.swing.JScrollPane();
        tblUnAvail = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMed = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        myUI.setBackground(new java.awt.Color(255, 255, 255));

        btnCancel.setText("CLOSE");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(96, 155, 213));
        jLabel3.setText("ADD MEDICINE SCHEDULE");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        search.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });
        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        jLabel1.setText("SEARCH");

        noAvail.setText("ADD MEDICINE NOT AVAILABLE");
        noAvail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noAvailActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        patient.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
        patient.setEnabled(false);
        patient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientActionPerformed(evt);
            }
        });

        jLabel6.setBackground(new java.awt.Color(96, 155, 213));
        jLabel6.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("PATIENT ID :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel6))
                    .addComponent(patient))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(patient, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(96, 155, 213));

        patName.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 12)); // NOI18N
        patName.setEnabled(false);
        patName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patNameActionPerformed(evt);
            }
        });

        jLabel7.setBackground(new java.awt.Color(96, 155, 213));
        jLabel7.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("PATIENT NAME : ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 10, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(patName, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(patName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(96, 155, 213));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane1KeyPressed(evt);
            }
        });

        tblTask.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "MEDICINE NAME", "DATE", "TIME", "STATUS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblTask.setRowHeight(25);
        tblTask.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTaskMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblTask);

        unavailable.setBackground(new java.awt.Color(255, 255, 255));
        unavailable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                unavailableKeyPressed(evt);
            }
        });

        tblUnAvail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "MEDICINE NAME", "DATE", "TIME", "STATUS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblUnAvail.setRowHeight(25);
        tblUnAvail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblUnAvailMouseClicked(evt);
            }
        });
        unavailable.setViewportView(tblUnAvail);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                    .addComponent(unavailable))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(unavailable, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(96, 155, 213));

        jScrollPane2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jScrollPane2KeyPressed(evt);
            }
        });

        tblMed.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "Drug Name", "DOSAGE", "UNIT", "STOCK"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMed.setRowHeight(25);
        tblMed.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMedMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblMed);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout myUILayout = new javax.swing.GroupLayout(myUI);
        myUI.setLayout(myUILayout);
        myUILayout.setHorizontalGroup(
            myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, myUILayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(375, 375, 375))
            .addGroup(myUILayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(myUILayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(489, 489, 489))
                    .addGroup(myUILayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(myUILayout.createSequentialGroup()
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(myUILayout.createSequentialGroup()
                        .addGap(198, 198, 198)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(276, 276, 276)
                        .addComponent(noAvail, javax.swing.GroupLayout.PREFERRED_SIZE, 468, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(myUILayout.createSequentialGroup()
                        .addGap(308, 308, 308)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        myUILayout.setVerticalGroup(
            myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(myUILayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(myUILayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(myUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(noAvail, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(myUI, javax.swing.GroupLayout.PREFERRED_SIZE, 1066, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(myUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to close this?",
                "Confirm Cancel", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                DoctorPatientViewDetails view = new DoctorPatientViewDetails();
                view.setVisible(true);
                view.data(patId);
                this.dispose();
            } catch (IOException ex) {
                Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void tblTaskMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTaskMouseClicked
        try {
            // TODO add your handling code here:
            int no;
            int rowNum = tblTask.getSelectedRow();
            no = (int) tblTask.getValueAt(rowNum, 0);

            DoctorMedicineSchedUpdate view = new DoctorMedicineSchedUpdate();
            view.setVb(this);
            view.setVisible(true);
            view.data(no, patId);
            this.dispose();
        } catch (IOException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblTaskMouseClicked

    private void jScrollPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane1KeyPressed

    }//GEN-LAST:event_jScrollPane1KeyPressed

    /**
     * NAME: loadUnavailMedSched 
     * PURPOSE: it will displays the data that we get
     * from the database. 
     * PARAMETERS: no parameters. 
     * RETURN VALUE: no return
     * values
     */
    public void loadUnavailMedSched() throws FileNotFoundException, IOException {
        String med, stat;
        int num=0;
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel) tblUnAvail.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        UnavailabMedicineScheduleInformation meds = new UnavailabMedicineScheduleInformation();
        meds.getunMedSched();
        try {
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\unavailmedicineTask.json"));

            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray) json.get("data");
            Iterator<String> i = k.iterator();

            for (int x = 0; x < k.size(); x++) {

                json = (JSONObject) k.get(x);
                if (json.get("umsPat_id").equals(patId) && json.get("umdhDel").equals("1")) {

                    med = (String) json.get("umsMedName");
                    stat = (String) json.get("umsStatus");

                    if (stat.equals("0")) {
                        stat = "Canceled";
                    } else if (stat.equals("1")) {
                        stat = "Pending";
                    } else if (stat.equals("2")) {
                        stat = "Done";
                    } else {
                        stat = "Unaccomplished";
                    }

                    num++;
                    model.addRow(new Object[]{num, med, (String) json.get("umsDate"), (String) json.get("umsTime")+" "+(String) json.get("umsDay"), stat});
                }
            }

        } catch (ParseException ex) {
            //  Logger.getLogger(Patient.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("error");
        }
    }
    private void patNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patNameActionPerformed

    private void tblMedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMedMouseClicked
        // TODO add your handling code here:
        try {
            int no;
            int rowNum = tblMed.getSelectedRow();
            no = (int) tblMed.getValueAt(rowNum, 0);

            DoctorAddMedicineSchedule view = new DoctorAddMedicineSchedule();
            view.setVb(this);
            view.setVisible(true);
            view.data(no, patId);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblMedMouseClicked

    private void jScrollPane2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jScrollPane2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane2KeyPressed

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        // TODO add your handling code here:
        FunctionSearch a = new FunctionSearch();
        a.search(tblMed, search.getText());
        a.search(tblTask, search.getText());
    }//GEN-LAST:event_searchKeyReleased

    private void noAvailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noAvailActionPerformed
        System.out.println("HIMSv2_HeadnurseView.HeadnurseMedicineScheduleAdd.noAvailActionPerformed()" + " " + patId);
        try {
            DoctorAddNotAvailMedicine a = new DoctorAddNotAvailMedicine();
            a.setVb(this);
            a.setVisible(true);
            a.data(patId);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_noAvailActionPerformed

    private void searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchActionPerformed

    private void tblUnAvailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblUnAvailMouseClicked
         try {
            // TODO add your handling code here:
            int no;
            int rowNum = tblUnAvail.getSelectedRow();
            no = (int) tblUnAvail.getValueAt(rowNum, 0);

            DoctorUnAvailMedicineUpdate view = new DoctorUnAvailMedicineUpdate();
            view.setVb(this);
            view.setVisible(true);
            view.data(no, patId);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblUnAvailMouseClicked

    private void unavailableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_unavailableKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_unavailableKeyPressed

    private void patientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DoctorPatMedicineSchedule.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DoctorPatMedicineSchedule().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JPanel myUI;
    private javax.swing.JButton noAvail;
    private javax.swing.JTextField patName;
    private javax.swing.JTextField patient;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblMed;
    private javax.swing.JTable tblTask;
    private javax.swing.JTable tblUnAvail;
    private javax.swing.JScrollPane unavailable;
    // End of variables declaration//GEN-END:variables

}
