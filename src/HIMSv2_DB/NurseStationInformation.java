/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class NurseStationInformation {
    public static void main(String[] args)
    {     
         NurseStationInformation x = new NurseStationInformation();

    }
    
    /**
     * NAME: getNurseStation
     * PURPOSE: connects to the servers php to get the records of Nurse stations and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getNurseStation(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/nurseStation/nurseStation.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/nurseStation/nurseStation.json", "nurseStation");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
     /**
     * NAME: addNurseStation
     * PURPOSE: This method will add new Nurse Station record.
     * PARAMETERS: String nsName, int buildingID
     * RETURN VALUE: void
     */
    
    public void addNurseStation(String nsName, int buildingID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/nurseStation/addNurseStation.php?nsName="+nsName+"&buildingID="+buildingID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteNurseStation
     * PURPOSE: This method will delete nurse Station Record record.
     * PARAMETERS: integer nsID
     * RETURN VALUE: void
     */
    
    public void deleteNurseStation( int nsID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/nurseStation/deleteNurseStation.php?nsID="+nsID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
     /**
     * NAME: updateNurseStation
     * PURPOSE: This method will update nurse station record.
     * PARAMETERS: String nsName, int buildingID, int nsID
     * RETURN VALUE: void
     */
    
    public void updateNurseStation(String nsName, int buildingID, int nsID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/nurseStation/updateNurseStation.php?nsName="+nsName+"&buildingID="+buildingID+"&nsID="+nsID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
}
