/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_NutritionInventoryView;

import HIMSv2_DB.IngredientsInformation;
import HIMSv2_DB.LinensInformation;
import HIMSv2_FunctionChecker.FunctionSearch;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class NutritionistInventoryIngredients extends javax.swing.JPanel {

    /**
     * Creates new form adminManageBuildingView
     */
    public NutritionistInventoryIngredients() throws IOException {
        initComponents();
        this.data();
    }
    
    /**
     * NAME: data
     * PURPOSE: this will get the information of the bed from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values.
     */
    
    public void data() throws IOException{
        IngredientsInformation a = new IngredientsInformation();
        a.getIngredients();
        try {
           this.loadLinen();
        } catch (FileNotFoundException ex) {
            System.out.println("error");
        } catch (JSONException ex) {
            System.out.println("error");
        }
        
    }
    
    public void loadLinen() throws FileNotFoundException, IOException, JSONException{
        String description;
        String name;
        int num= 0;
        
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel)tblIngredients.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        try {
  
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\ingredients.json"));
            JSONObject json = (JSONObject) o;
            
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                num++;
                description =(String) json.get("IngredientsDesc");
                description = description.replace("_", " ");
                name =(String) json.get("IngredientsName");
                name = name.replace("_", " ");
                
                model.addRow(new Object[]{num,name,description});
            }
                    
        } catch (ParseException ex) {
            System.out.println("error");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblIngredients = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        addingredient = new javax.swing.JButton();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        tblIngredients.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "NAME", "DESCRIPTION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblIngredients.setRowHeight(25);
        tblIngredients.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblIngredientsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblIngredients);

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        addingredient.setText("ADD INGREDIENTS");
        addingredient.setActionCommand("ADD INGREDIENT");
        addingredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addingredientActionPerformed(evt);
            }
        });

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("SEARCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
                .addComponent(addingredient, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(addingredient, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(search, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addingredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addingredientActionPerformed
        NutritionInventoryAddIngredients a = new NutritionInventoryAddIngredients();
        a.setVb(this);
        a.setVisible(true);             
    }//GEN-LAST:event_addingredientActionPerformed

    private void tblIngredientsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblIngredientsMouseClicked
        try {
            // TODO add your handling code here:
            String code;
            String IngName;
            String Ingdesc;
            
            int rowNum = tblIngredients.getSelectedRow();
            System.out.println(rowNum);
            IngName = (String) tblIngredients.getValueAt(rowNum, 1);
            Ingdesc = (String) tblIngredients.getValueAt(rowNum, 2);
            NutritionInventoryUpdateDeleteIngredients view = new NutritionInventoryUpdateDeleteIngredients();
            view.setVb(this);
            view.setVisible(true);
            view.setData( IngName, Ingdesc);
        } catch (IOException ex) {
            Logger.getLogger(NutritionistInventoryIngredients.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(NutritionistInventoryIngredients.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblIngredientsMouseClicked

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        // TODO add your handling code here:
        FunctionSearch a = new FunctionSearch();
        a.search(tblIngredients, search.getText());       
    }//GEN-LAST:event_searchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addingredient;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblIngredients;
    // End of variables declaration//GEN-END:variables
}
