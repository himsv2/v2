/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class FloorInformation {
    public static void main(String[] args)
    {     
         FloorInformation x = new FloorInformation();

    }
    
    /**
     * NAME: getFloor
     * PURPOSE: connects to the servers php to get the records of floor and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getFloor(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/floor/floor.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/floor/floor.json", "floor");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addFloor
     * PURPOSE: This method will add new floor record.
     * PARAMETERS: String floor name 
     * RETURN VALUE: void
     */
    
    public void addFloor(String floorName){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/floor/addFloor.php?floorName="+floorName+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteFloor
     * PURPOSE: This method will delete floor record.
     * PARAMETERS: int floor id to be deleted
     * RETURN VALUE: void
     */
    
    public void deleteFloor(int floorID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/floor/deleteFloor.php?floorID="+floorID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    /**
     * NAME: updateFloor
     * PURPOSE: This method will add new floor record.
     * PARAMETERS: String floor name and int floor ID
     * RETURN VALUE: void
     */
    
    public void updateFloor(String floorName, int floorID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/floor/updateFloor.php?floorName="+floorName+"&floorID="+floorID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
}
