/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.BedInformation;
import HIMSv2_DB.BuildingInformation;
import HIMSv2_DB.EquipmentInformation;
import HIMSv2_DB.FloorInformation;
import HIMSv2_DB.FoodInformation;
import HIMSv2_DB.FoodIngredientsInformotion;
import HIMSv2_DB.IngredientsInformation;
import HIMSv2_DB.LinensInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.NurseStationInformation;
import HIMSv2_DB.PatientInformation;
import HIMSv2_DB.RequestInformation;
import HIMSv2_DB.RoomInformation;
import HIMSv2_DB.RoomTypeInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionChecker {
    
    /**
     * NAME: checkBedName
     * PURPOSE: This method will check if the bed name is already in the record or not.
     * PARAMETERS: String bed name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkBedName(String bedName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        BedInformation a = new BedInformation();
        a.getBed();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\bed.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("bedName").equals(bedName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    
    /**
     * NAME: checkBuildingName
     * PURPOSE: This method will check if the building name is already in the record or not.
     * PARAMETERS: String building name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkBuildingName(String buildingName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        BuildingInformation a = new BuildingInformation();
        a.getBuilding();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\building.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("buildName").equals(buildingName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    
    /**
     * NAME: checkFloorName
     * PURPOSE: This method will check if the floor name is already in the record or not.
     * PARAMETERS: String floor name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    public int checkFloorName(String floorName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        FloorInformation a = new FloorInformation();
        a.getFloor();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\floor.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("floorName").equals(floorName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    /**
     * NAME: checkNurseStationName
     * PURPOSE: This method will check if the nurse station name is already in the record or not.
     * PARAMETERS: String nurse station name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkNurseStationName(String nurseStationName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        NurseStationInformation a = new NurseStationInformation();
        a.getNurseStation();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\nurseStation.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("nstName").equals(nurseStationName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    
    /**
     * NAME: checkRoomName
     * PURPOSE: This method will check if the room name is already in the record or not.
     * PARAMETERS: String room name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkRoomName(String roomName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        RoomInformation a = new RoomInformation();
        a.getRoom();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("rmName").equals(roomName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    /**
     * NAME: checkRoomTypeName
     * PURPOSE: This method will check if the room type name is already in the record or not.
     * PARAMETERS: String room type name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkRoomTypeName(String roomTypeName) throws FileNotFoundException, IOException, ParseException{
        int bedExist = 0;
        RoomTypeInformation a = new RoomTypeInformation();
        a.getRoomType();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\roomType.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("rtName").equals(roomTypeName))) {
                bedExist = 1;
            } 
        }
        
        return bedExist;
    }
    
    /**
     * NAME: checkEquipment
     * PURPOSE: This method will check if the equipment name is already in the record or not.
     * PARAMETERS: String code and String name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkEquipment(String code, String name) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        EquipmentInformation a = new EquipmentInformation();
        a.getEquipment();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("equipName").equals(name)) || (json.get("equipCode").equals(code))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    
    /**
     * NAME: checkQuantity
     * PURPOSE: This method will check if quantity is not less than 100 and the it is not out of stock.
     * PARAMETERS: int qty and String item
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkQuantity(int qty, String item) throws FileNotFoundException, IOException, ParseException{
        int quantity = 0;
        int result = 0;
        int quanty = 0;
        int ID = 0;
                
        FunctionQuantityGetter b = new FunctionQuantityGetter();
        quantity = b.getEquipment(item);
        
        FunctionIDGetter c = new FunctionIDGetter();
        ID = c.getEquipt(item);

        quanty = quantity - qty;
        if(quanty < 100){
            
            if(quanty > 1){
                result = 1;
               EquipmentInformation a = new EquipmentInformation();
                a.updateQuantity(ID, quanty);
            } else{
                result = 2;
                JOptionPane.showMessageDialog(null, "Sorry, you are not allowed to approved a request its out of stock");
                
            } 
            
        }else{
            EquipmentInformation a = new EquipmentInformation();
            a.updateQuantity(ID, quanty);
           
        }
        return result;
    }
    
    /**
     * NAME: checkLinen
     * PURPOSE: This method will check if the linen name is already in the record or not.
     * PARAMETERS: String code and String name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */    
     public int checkLinen(String code, String name) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        EquipmentInformation a = new EquipmentInformation();
        a.getEquipment();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("lineName").equals(name)) || (json.get("lineItemCode").equals(code))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    /**
     * NAME: checkQuantityLinen
     * PURPOSE: This method will check if the quantity name is already in the record or not.
     * PARAMETERS: int qty and String item
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkQuantityLinen(int qty, String item) throws FileNotFoundException, IOException, ParseException{
        int quantity = 0;
        int result = 0;
        int quanty = 0;
        int ID = 0;
                
        FunctionQuantityGetter b = new FunctionQuantityGetter();
        quantity = b.getLinen(item);
        quanty = quantity - qty;
        FunctionIDGetter c = new FunctionIDGetter();
        ID = c.getLinen(item);
        
        if(quanty < 100){
            if(quanty > 1){
               LinensInformation a = new LinensInformation();
                a.updateQuantity(ID, qty);
                result = 1;
            } else{
                result = 2;
                JOptionPane.showMessageDialog(null, "Sorry, you are not allowed to approved a request its out of stock");  
            } 
            
        }else{
            LinensInformation a = new LinensInformation();
            a.updateQuantity(ID, qty);
        }
        return result;
    }
    
    /**
     * NAME: checkPatient
     * PURPOSE: This method will check if the patient exist in the record or not.
     * PARAMETERS: String patient number
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */    
     public int checkPatient(String patID) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        PatientInformation a = new PatientInformation();
        a.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patient.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("ptId").equals(patID))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
     
     /**
     * NAME: checkRoomBuilding
     * PURPOSE: This method will check if the building exist in the room record or not.
     * PARAMETERS: String building number
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */    
     public String checkRoomBuilding(int builID) throws FileNotFoundException, IOException, ParseException{
        String Exist = "0";
        String roomBuilding;
        int ID;
        PatientInformation a = new PatientInformation();
        a.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            roomBuilding = (String) json.get("rmBuilding_id");
             ID = Integer.parseInt(roomBuilding);
             if(ID == builID) {
                Exist = (String) json.get("rmName");
            } 
        }
        
        return Exist;
    }
     /**
     * NAME: checkRoomRT
     * PURPOSE: This method will check if the room type exist in the room record or not.
     * PARAMETERS: String room number
     * RETURN VALUE: Sting name of the room if it already exist and 0 if not.
     */    
     public String checkRoomRT(int rtID) throws FileNotFoundException, IOException, ParseException{
        String Exist = "0";
        String roomBuilding;
        int ID;
        PatientInformation a = new PatientInformation();
        a.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            roomBuilding = (String) json.get("rmRommType_id");
             ID = Integer.parseInt(roomBuilding);
             if(ID == rtID) {
                Exist = (String) json.get("rmName");
            } 
        }
        
        return Exist;
    }
     
     /**
     * NAME: checkRoomFloor
     * PURPOSE: This method will check if the Floor exist in the room record or not.
     * PARAMETERS: integer room floor number
     * RETURN VALUE: Sting name of the floor if it already exist and 0 if not.
     */    
     public String checkRoomFloor(int fID) throws FileNotFoundException, IOException, ParseException{
        String Exist = "0";
        String roomBuilding;
        int ID;
        PatientInformation a = new PatientInformation();
        a.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\room.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            roomBuilding = (String) json.get("rmFloor");
             ID = Integer.parseInt(roomBuilding);
             if(ID == fID) {
                Exist = (String) json.get("rmName");
            } 
        }
        
        return Exist;
    }
     /**
     * NAME: checkBedRoom
     * PURPOSE: This method will check if the bedroom exist in the room record or not.
     * PARAMETERS: integer room number
     * RETURN VALUE: Sting name of the bedroom if it already exist and 0 if not.
     */    
     public String checkBedRoom(int rID) throws FileNotFoundException, IOException, ParseException{
        String Exist = "0";
        String roomBuilding;
        int ID;
        PatientInformation a = new PatientInformation();
        a.getPatient();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\bed.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            roomBuilding = (String) json.get("bedRoom_id");
             ID = Integer.parseInt(roomBuilding);
             if(ID == rID) {
                Exist = (String) json.get("bedName");
            } 
        }
        
        return Exist;
    }

    /**
     * NAME: checkMedicine
     * PURPOSE: This method will check if the medicine name is already in the record or not.
     * PARAMETERS: String item name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkMedicine(String itmName) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        MedicineInformation a = new MedicineInformation();
        a.getMedicine();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("medInName").equals(itmName))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }


/**
     * NAME: checkMedicineQuantity
     * PURPOSE: This method will check if the medicine quantity name is already in the record or not.
     * PARAMETERS: integer qty and String item
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkMedicineQuantity(int qty, String item) throws FileNotFoundException, IOException, ParseException{
        int quantity = 0;
        int result = 0;
        int quanty = 0;
        int ID = 0;
                
        FunctionQuantityGetter b = new FunctionQuantityGetter();
        quantity = b.getMedicine(item);
        
        FunctionIDGetter c = new FunctionIDGetter();
        ID = c.getMed(item);

        quanty = quantity - qty;
        if(quanty > 100){
            
            if(quantity > 1){
                result = 1;
               MedicineInformation a = new MedicineInformation();
               a.updateQuantity(ID, quanty); 
            } else{
                result = 2;
                JOptionPane.showMessageDialog(null, "Sorry, you are not allowed to approved a request its out of stock");
                
            } 
            
        }else{
            MedicineInformation a = new MedicineInformation();
            a.updateQuantity(ID, quanty);
           
        }
        return result;
    }
    
    /**
     * NAME: checkIngredientsName
     * PURPOSE: This method will check if the ingredients name is already in the record or not.
     * PARAMETERS: String  name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkIngredietsName(String name) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        IngredientsInformation a = new IngredientsInformation();
        a.getIngredients();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\ingredients.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("IngredientsName").equals(name))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    /**
     * NAME: checkFoodName
     * PURPOSE: This method will check if the food name is already in the record or not.
     * PARAMETERS: String  name
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkFoodName(String name) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        FoodInformation a = new FoodInformation();
        a.getFood();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\food.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("foodName").equals(name))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    /**
     * NAME: checkIng
     * PURPOSE: This method will check if the food name is already in the record or not.
     * PARAMETERS: integer ID and integer ing
     * RETURN VALUE: int 1 if it already exist and 0 if not.
     */
    
    public int checkIng(int ID, int ing) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        String ingId;
        int idIng;
        FoodIngredientsInformotion a = new FoodIngredientsInformotion();
        a.getFoodIngredients(ID);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\foodItem.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            ingId = (String) json.get("fdltIngrt_id");
            idIng = Integer.parseInt(ingId);
             if(idIng == ing) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    
    
    
    /**
     * NAME: checkFoodNameId
     * PURPOSE: This method will check if the food name is already in the record or not.
     * PARAMETERS: integer Id, integer ingredient
     * RETURN VALUE: integer 1 if it already exist and 0 if not.
     */
    
    public int checkFoodNameId(int ID, int ing) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        String foodId;
        int idfood;
        MealPlanInformation a = new MealPlanInformation();
        a.getMealPlanId(ID);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\getMealPlanId.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            foodId = (String) json.get("mealitemFood_id");
            idfood = Integer.parseInt(foodId);
             if(idfood == ing) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    
    /**
     * NAME: checkMealPlanName
     * PURPOSE: This method will check if the mealPlan name is already in the record or not.
     * PARAMETERS: String mealPlan name
     * RETURN VALUE: integer 1 if it already exist and 0 if not.
     */
    
    public int checkMealPlanName(String name) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        FoodInformation a = new FoodInformation();
        a.getFood();
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
             if((json.get("mealplanName").equals(name))) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
    
    /**
     * NAME: checkFoodRestrictionNameId
     * PURPOSE: This method will check if the Food Restriction name is already in the record or not.
     * PARAMETERS: integer ID and integer Rest
     * RETURN VALUE: integer 1 if it already exist and 0 if not.
     */
    
    public int checkFoodRestrictionNameId(int ID, int rest) throws FileNotFoundException, IOException, ParseException{
        int Exist = 0;
        String restId;
        int idrest;
        MealPlanInformation a = new MealPlanInformation();
        a.getMealPlanRestId(ID);
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\getMealPlanRestId.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
        for(int x = 0; x < k.size();x++){
            json = (JSONObject)k.get(x);
            restId = (String) json.get("mealRest_id");
            idrest = Integer.parseInt(restId);
             if(idrest == rest) {
                Exist = 1;
            } 
        }
        
        return Exist;
    }
}
