/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author CAMINERO
 */
public class BloodInformation {
    public static void main(String[] args) {
        BloodInformation x = new BloodInformation();
    }
    
    /**
     * NAME: getBlood
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood.json", "blood");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    } 
    
    /**
     * NAME: getBlood
     * PURPOSE: This method will get Blood Inventory record
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodInventory(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getBloodApprove.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/request/getBloodApprove.json", "getBloodApprove");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodID
     * PURPOSE: This method will get Blood Inventory record
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getBloodID(String[] ID){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood.json", "blood");
        } catch (IOException ex) {
            System.out.println("error");
        }  
        System.out.println("manakog doooownloadddddd");
    }
    
    /**
     * NAME: updateRequest
     * PURPOSE: This method will update new Blood Request record.
     * PARAMETERS: integer reqID
     * RETURN VALUE: void
     */
    public void updateRequest(int reqID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/setBloodRequestApprove.php?";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Your request is approved!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: getBloodAvail
     * PURPOSE: This method will get the Available Blood record.
     * PARAMETERS: String bloodType
     * RETURN VALUE: void
     */
    
    public void getBloodAvail(String[] bloodType){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodstatus.php?bloodType="+bloodType+"&date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);

        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodstatus.json", "bloodstatus");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getDonorId
     * PURPOSE: This method will get the Donor record.
     * PARAMETERS: integer ID
     * RETURN VALUE: void
     */
    
    public void getDonorId(int ID){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/donor.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/donor/donor.json", "donor");
        } catch (IOException ex) {
            System.out.println("error");
        } 
    }
    
    /**
     * NAME: getBloodO
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodO(){  
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodO.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodO.json", "bloodO");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_O
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_O(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_O.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_O.json", "blood_O");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodA
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodA(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodA.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodA.json", "bloodA");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_A
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_A(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_A.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_A.json", "blood_A");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodB
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodB(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodB.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodB.json", "bloodB");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_B
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_B(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_B.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_B.json", "blood_B");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodAB
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodAB(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodAB.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodAB.json", "bloodAB");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_AB
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_AB(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_AB.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_AB.json", "blood_AB");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodOExpired
     * PURPOSE: connects to the servers php to get the records of Expired Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodOExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodOExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodOExpired.json", "bloodOExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_0Expired
     * PURPOSE: connects to the servers php to get the records of Expired Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_OExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_OExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_OExpired.json", "blood_OExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodAExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodAExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodAExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodAExpired.json", "bloodAExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_AExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_AExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_AExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_AExpired.json", "blood_AExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodBExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodBExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodBExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodBExpired.json", "bloodBExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_BExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_BExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_BExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_BExpired.json", "blood_BExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodABExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodABExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/bloodABExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/bloodABExpired.json", "bloodABExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBlood_ABExpired
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBlood_ABExpired(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        String date1 = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/blood/blood_ABExpired.php?date="+date+"&date1="+date1+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/blood/blood_ABExpired.json", "blood_ABExpired");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}
