/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author CAMINERO
 */
public class MedicineInformation {
    public static void main(String[] args)
    {     
         MedicineInformation x = new MedicineInformation();

    }
    
    /**
     * NAME: getMedicine
     * PURPOSE: connects to the servers php to get the records of Medicine and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getMedicine(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicine/medicine.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/medicine/medicine.json", "medicine");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addMedicine
     * PURPOSE: This method will add new Medicine record
     * PARAMETERS: String name, String dosage, String um, String qty, String desc, String price
     * RETURN VALUE: void
     */
    
    public void addMedicine(String name, String dosage, String um, String qty, String desc, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicine/addMedicine.php?name="+name+"&dosage="+dosage+"&um="+um+"&qty="+qty+"&desc="+desc+"&price="+price+"";
        JavaHTTPConnectionReader javaHTTPConnectionReader = new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateMedicine
     * PURPOSE: This method will update new Medicine record.
     * PARAMETERS: int medID, String name, String dosage, String um, String qty, String desc, String price
     * RETURN VALUE: void
     */
    
    public void updateMedicine(int medID, String name, String dosage, String um, String qty, String desc, String price){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicine/updateMedicine.php?name="+name+"&dosage="+dosage+"&um="+um+"&qty="+qty+"&desc="+desc+"&medID="+medID+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteMedicine
     * PURPOSE: This method will delete Medicine record.
     * PARAMETERS: integer medicine ID
     * RETURN VALUE: void
     */
    public void deleteMedicine(int medID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicine/deleteMedicine.php?medID="+medID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateQuantity
     * PURPOSE: This method will update the quantity of the Medicine record.
     * PARAMETERS: integer Equipment ID and quantity.
     * RETURN VALUE: void
     */
    public void updateQuantity(int ID, int qty){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicine/updateMedicineQuantity.php?ID="+ID+"&qty="+qty+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
}
