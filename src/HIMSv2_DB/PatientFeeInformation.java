/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class PatientFeeInformation {
    
    /**
     * NAME: getPatientFee
     * PURPOSE: connects to the servers php to get the records of Patient fee and download the record
     * from the server to this PC.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getPatientFee(String ID){
        
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patientFee/patientFee.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/patientFee/patientFee.json", "patientFee");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}
