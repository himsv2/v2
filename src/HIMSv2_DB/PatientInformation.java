/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class PatientInformation {
    public static void main(String[] args)
    {     
         PatientInformation x = new PatientInformation();

    }
    
    /**
     * NAME: getPatient
     * PURPOSE: connects to the servers php to get the records of patient and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getPatient(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patient/patient.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/patient/patient.json", "patient");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    /**
     * NAME: addPatient
     * PURPOSE: This method will add new Patient record.
     * PARAMETERS: String fname, String mname, String lname,String sex,String age,String email, String contact,String suffix, String religion, String nationality, String maritalS,String occupation,String address,String height,String weight,String famName,String relation,String famContact,String blood,String admitted,String d,String nurse,String bloodpre ,String temp,String doc
     * RETURN VALUE: void
     */
    public void addPatient(String fname, String mname, String lname,String sex,String age,String email, String contact,String suffix, String religion, String nationality, String maritalS,String occupation,String address,String height,String weight,String famName,String relation,String famContact,String blood,String admitted,String d,String nurse,String bloodpre ,String temp,String doc) {     
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patient/addPatient.php?fname="+fname+"&mname="+mname+"&lname="+lname+"&sex="+sex+"&age="+age+"&email="+email+"&contact="+contact+"&suffix="+suffix+"&religion="+religion+"&nationality="+nationality+"&maritalS="+maritalS+"&occupation="+occupation+"&address="+address+"&height="+height+"&weight="+weight+"&famName="+famName+"&relation="+relation+"&famContact="+famContact+"&blood="+blood+"&admitted="+admitted+"&dob="+d+"&nurse="+nurse+"&bloodpre="+bloodpre+"&temp="+temp+"&doc="+doc+" ";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    /**
     * NAME: updatePatient
     * PURPOSE: This method will update new Patient record.
     * PARAMETERS: String fname, String mname,String lname,String age,String email ,String contact,String religion,String nationality ,String maritalS,String occupation,String address,String height, String weight,String suffix,String famName,String relation,String famContact,String sex,String admitted,String temp,String bloodpre,String blood,String dob,int PatientID,String nurse,String doc
     * RETURN VALUE: void
     */
    public void updatePatient(String fname, String mname,String lname,String age,String email ,String contact,String religion,String nationality ,String maritalS,String occupation,String address,String height, String weight,String suffix,String famName,String relation,String famContact,String sex,String admitted,String temp,String bloodpre,String blood,String dob,int PatientID,String nurse,String doc){      
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patient/updatePatient.php?fname="+fname+"&mname="+mname+"&lname="+lname+"&age="+age+"&email="+email+"&contact="+contact+"&religion="+religion+"&nationality="+nationality+"&maritalS="+maritalS+"&occupation="+occupation+"&address="+address+"&height="+height+"&weight="+weight+"&suffix="+suffix+"&famName="+famName+"&relation="+relation+"&famContact="+famContact+"&sex="+sex+"&admitted="+admitted+"&temp="+temp+"&bloodpre="+bloodpre+"&blood="+blood+"&dob="+dob+"&PatientID="+PatientID+"&nurse="+nurse+"&doc="+doc+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updatePatient
     * PURPOSE: This method will update new Patient record.
     * PARAMETERS: String admitted,int PatientID
     * RETURN VALUE: void
     */
    public void outPatient(String admitted,int PatientID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patient/outPatient.php?admitted="+admitted+"&PatientID="+PatientID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
     /**
     * NAME: updatePatientDoc
     * PURPOSE: This method will update Patient doctor record.
     * PARAMETERS: String fname, String mname,String lname,String age,String email ,String contact,String religion,String nationality ,String maritalS,String occupation,String address,String height, String weight,String suffix,String famName,String relation,String famContact,String sex,String admitted,String temp,String bloodpre,String blood,String dob,int PatientID,String nurse,String doc
     * RETURN VALUE: void
     */
    public void updatePatientDoc(String PatientID,String doc){      
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/patient/updatePatientDoc.php?PatientID="+PatientID+"&doc="+doc+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
}
