/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author CAMINERO
 */
public class DonorInformation {

    public static void main(String[] args) {
        DonorInformation x = new DonorInformation();
    }

    /**
     * NAME: getDonor 
     * PURPOSE: connects to the servers php to get the records of Donors and download the record from the server to this PC. 
     * PARAMETERS: no parameters 
     * RETURN VALUE: void
     */
    public void getDonor() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/donor.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/donor/donor.json", "donor");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }
    
    /**
     * NAME: getDonor
     * PURPOSE: connects to the servers php to get the records of Donors and download the record from the server to this PC. 
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getDonor(String ID) {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/donor.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/donor/donor.json", "donor");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }
    
    
    
    /**
     * NAME: getDonateId
     * PURPOSE: connects to the servers php to get the records of Donors and download the record
     * from the server to this PC.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getDonateId(String ID){    
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/getDonateList.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/donor/getDonateList.json", "getDonateList");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getBloodStock 
     * PURPOSE: connects to the servers php to get the records of Blood and download the record from the server to this PC. 
     * PARAMETERS: String blood
     * RETURN VALUE: void
     */
    public void getBloodStock(String blood) {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/donor.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/donor/donor.json", "donor");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }

    /**
     * NAME: addDonor 
     * PURPOSE: This method will add new Donor record.
     * PARAMETERS: String firstName, String middleName, String lastName, String gend, String contact, String addr, String bloodtype, String dob
     * RETURN VALUE: void
     */
    public void addDonor(String firstName, String middleName, String lastName, String gend, String contact, String addr, String bloodtype, String dob) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/addDonor.php?firstName=" + firstName + "&middleName=" + middleName + "&lastName=" + lastName + "&gend=" + gend + "&contact=" + contact + "&addr=" + addr + "&bloodtype=" + bloodtype + "&dob=" + dob +"&date="+date+"";
        JavaHTTPConnectionReader javaHTTPConnectionReader = new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
        /**
     * NAME: addDonate 
     * PURPOSE: This method will add new Donate record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    public void addDonate(String ID) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        String date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/addDonorDonate.php?ID="+ID+"&date="+date+"";
        JavaHTTPConnectionReader javaHTTPConnectionReader = new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }

    /**
     * NAME: updateDonor 
     * PURPOSE: This method will update new Donor record. 
     * PARAMETERS: String donorId, String fname, String mname, String lname, String contact, String address, String dob, String blood
     * RETURN VALUE: void
     */
    public void updateDonor(String donorId, String fname, String mname, String lname, String contact, String address, String dob, String blood) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/donor/updateDonor.php?donorId="+donorId+"&fname=" + fname + "&mname=" + mname + "&lname=" + lname + "&address=" + address + "&dob=" + dob + "&contact=" + contact + "&blood=" + blood + "";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: getDonorId
     * PURPOSE: This method will get Donor record.
     * PARAMETERS: String donorID , JLabel donate
     * RETURN VALUE: void
     */
    public void getDonorId(String donorID, JLabel donate){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/donorLast.php?donate="+donate+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
     /**
     * NAME: delDonor
     * PURPOSE: This method will delete Donor record
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void delDonor(String ID){
        
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/delDonor.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Changed!");
        System.out.println(myUrl);
    }
    
}
