/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB.Download_JSON;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author albert
 */
public class ReportInformation {
    public static void main(String[] args)
    {     
         ReportInformation x = new ReportInformation();

    }
    
    /**
     * NAME: getReport
     * PURPOSE: connects to the servers php to get the records of Report and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getReport(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/report.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/report/report.json", "report");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: delHeadnurseReport
     * PURPOSE: This method will delete Head nurse Report record.
     * PARAMETERS: int reportID
     * RETURN VALUE: void
     */
    
    public void delHeadnurseReport(int reportID){   
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/delreport.php?ID="+reportID+"";
        new JavaHTTPConnectionReader(myUrl);
         
    }
    
    /**
     * NAME: getNurseReport
     * PURPOSE: This method will get nurse Report record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void getNurseReport(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/report.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/report/report.json", "report");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: delNurseReport
     * PURPOSE: This method will delete nurse Report record.
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void delNurseReport(int ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/delreportnurse.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
    /**
     * NAME: updateNurseReport
     * PURPOSE: This method will update nurse Report record.
     * PARAMETERS: String srReport ,int srId
     * RETURN VALUE: void
     */
    
    public void updateNurseReport(String srReport ,int srId){     
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/updatereportnurse.php?srReport="+srReport+"&srId="+srId+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: addNurseReport
     * PURPOSE: This method will add nurse Report record.
     * PARAMETERS: String srReport,String patID, String ns, String docAndHn
     * RETURN VALUE: void
     */
    
    public void addReport(String srReport,String patID, String ns, String docAndHn){
        String date;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/report/addreportnurse.php?srReport="+srReport+"&patID="+patID+"&ns="+ns+"&date="+date+"&docAndHn="+docAndHn+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);

    }
}
