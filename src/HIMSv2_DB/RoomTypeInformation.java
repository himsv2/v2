/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class RoomTypeInformation {
    public static void main(String[] args)
    {     
         RoomTypeInformation x = new RoomTypeInformation();

    }
    
    /**
     * NAME: getRoomType
     * PURPOSE: connects to the servers php to get the records of Room type and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getRoomType(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/roomType.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/room/roomType.json", "roomType");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addRoomType
     * PURPOSE: This method will add new roomType record.
     * PARAMETERS: String roomTypeName, String desc, String price
     * RETURN VALUE: void
     */
    
    public void addRoomType(String roomTypeName, String desc, String price){
        float p = 0;
        p = Float.parseFloat(price);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/addRoomType.php?roomTypeName="+roomTypeName+"&desc="+desc+"&price="+price+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteRoomType
     * PURPOSE: This method will delete room type record.
     * PARAMETERS: Sint id of the room type to delete
     * RETURN VALUE: void
     */
    
    public void deleteRoomType(int rtID){
        float p = 0;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/deleteRoomType.php?rtID="+rtID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null, "Successfully Delete!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: updateRoomType
     * PURPOSE: This method will update room type record.
     * PARAMETERS: String building name and String description, price and id of the room type
     * RETURN VALUE: void
     */
    public void updateRoomType(String roomTypeName, String desc, String price, int rtID){
        float p = 0;
        p = Float.parseFloat(price);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/room/updateRoomType.php?roomTypeName="+roomTypeName+"&desc="+desc+"&price="+price+"&rtID="+rtID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null, "Successfully Updated!");
        System.out.println(myUrl);
    }
}
