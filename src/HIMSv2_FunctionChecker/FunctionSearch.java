/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import java.util.regex.PatternSyntaxException;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author cathy
 */


public class FunctionSearch {
    
    static DefaultTableModel model;
    
    /**
     * NAME: search
     * PURPOSE: This method will search for the given query
     * PARAMETERS: Jtable tab and String query
     * RETURN VALUE: none
     */

    public void search(JTable tab, String query) {
         model = (DefaultTableModel) tab.getModel();
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel> (model);
        tab.setRowSorter(tr);
        try {
            tr.setRowFilter(RowFilter.regexFilter("(?i)"+query));
        } catch (PatternSyntaxException e) {
            
        } }

   
}
