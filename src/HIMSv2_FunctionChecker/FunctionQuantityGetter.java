/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_FunctionChecker;

import HIMSv2_DB.EquipmentInformation;
import HIMSv2_DB.LinensInformation;
import HIMSv2_DB.MedicineInformation;
import HIMSv2_DB.PatientRoomInformation;
import HIMSv2_DB.RoomInformation;
import HIMSv2_DB.StaffInformation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class FunctionQuantityGetter {
    
    /**
     * NAME: getEquipment
     * PURPOSE: This method will get the name of the given equipment id.
     * PARAMETERS: String item
     * RETURN VALUE: string  quantity
     */
    
    public int getEquipment(String item) throws FileNotFoundException, IOException, ParseException {
        int quantity = 0;
        String q;
        
        EquipmentInformation c = new EquipmentInformation();
        c.getEquipment();
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\equipment.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            
            if(json.get("equipName").equals(item)){
                q =  (String) json.get("equipQuanity");
                quantity = Integer.parseInt(q);
            }
            
        } 
        return quantity;
    }
   
    /**
     * NAME: getLinen
     * PURPOSE: This method will get the name of the given linen id.
     * PARAMETERS: String item
     * RETURN VALUE: string linen quantity
     */
    
    public int getLinen(String item) throws FileNotFoundException, IOException, ParseException {
        int quantity = 0;
        String q;
        
        LinensInformation c = new LinensInformation();
        c.getLinens();
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\linens.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            
            if(json.get("lineName").equals(item)){
                q =  (String) json.get("lineQuantity");
                quantity = Integer.parseInt(q);
            }
            
        } 
        return quantity;
    }

      /**
     * NAME: getMedicine
     * PURPOSE: This method will get the name of the given medicine id.
     * PARAMETERS: String item
     * RETURN VALUE: string medicine quantity
     */
    
    public int getMedicine(String item) throws FileNotFoundException, IOException, ParseException {
        int quantity = 0;
        String q;
        
        MedicineInformation c = new MedicineInformation();
        c.getMedicine();
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\medicine.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            
            if(json.get("medInName").equals(item)){
                q =  (String) json.get("medInQuantiy");
                quantity = Integer.parseInt(q);
            }
            
        } 
        return quantity;
    }

    /**
     * NAME: getRmPat
     * PURPOSE: This method will get the name of the given room patient id.
     * PARAMETERS: integer roomPatId
     * RETURN VALUE: string room quantity
     */

    public int getRmPat(int roomPatId) throws FileNotFoundException, IOException, ParseException {
        int quantity = 0;
        String q;
        String id;
        int date;
        String d;
        PatientRoomInformation c = new PatientRoomInformation();
        c.getPatRoom();
        
        JSONParser parser = new JSONParser();
        Object o = parser.parse(new FileReader("C:\\HIMSv2\\patRoom.json"));
        JSONObject json = (JSONObject) o;
        org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
        Iterator<String> i = k.iterator();
       
        for(int x = 0; x < k.size();x++){
            
            json = (JSONObject)k.get(x);
            if(String.valueOf(roomPatId).equals(json.get("prId"))){
                d =  (String) json.get("prDate_In");
            }
            
        } 
        return quantity;
    }
}
