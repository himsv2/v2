/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author albert
 */
public class FeeInformation {
    public static void main(String[] args)
    {     
         FeeInformation x = new FeeInformation();

    }
    
    
    /**
     * NAME: addFee
     * PURPOSE: This method will add new Fee record.
     * PARAMETERS: String feeDesc, String feeQty, String feePrice,String ID
     * RETURN VALUE: void
     */
    
    public void addFee(String feeDesc, String feeQty, String feePrice,String ID){
        String date;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/feeAdd.php?feeDesc="+feeDesc+"&feeQty="+feeQty+"&feePrice="+feePrice+"&date="+date+"&pat_ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);

    }
    
    /**
     * NAME: getBalance
     * PURPOSE: This method will get Balance record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getBalance(String ID){
        String date;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/balance.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/fee/balance.json", "balance");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: feeDell
     * PURPOSE: This method will delete Balance record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void feeDell(String ID){   
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/feeDel.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: feeAddHistory
     * PURPOSE: This method will add History record.
     * PARAMETERS: String amount, String balance, String patID, String nurseID, String date
     * RETURN VALUE: void
     */
    
    public void feeAddHistory(String amount, String balance, String patID, String nurseID, String date){      
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/feeAddHistory.php?amount="+amount+"&balance="+balance+"&nurseID="+nurseID+"&date="+date+"&ID="+patID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: updateFeeHistory
     * PURPOSE: This method will update History record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void updateFeeHistory(String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/updateFeeHistory.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: paymentHistory
     * PURPOSE: This method will get Payment History record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void paymentHistory(String ID){  
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/paymentHistory.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/fee/paymentHistory.json", "paymentHistory");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: History
     * PURPOSE: This method will get History record.
     * PARAMETERS: none
     * RETURN VALUE: void
     */
    
    public void History(){    
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/History.php?";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/fee/History.json", "History");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}
