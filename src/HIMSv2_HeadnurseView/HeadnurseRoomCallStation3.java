/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_HeadnurseView;

import HIMSv2_DB.RoomInformation;
import HIMSv2_FunctionChecker.FunctionNurseStationGetter;
import HIMSv2_View.HeadNurseHome;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.json.simple.parser.ParseException;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 *
 * @author cathy
 */
public class HeadnurseRoomCallStation3 extends javax.swing.JPanel {

    int flag = 0;
    String rm1 = "", rm2 = "", rm3 = "", rm4 = "", rm6 = "", rm7 = "", rm8 = "", rm9 = "", rm10 = "", rm11 = "";
    String rm12 = "", rm13 = "", rm14 = "", rm15 = "", rm16 = "", rm17 = "", rm18 = "", rm19 = "", rm20 = "", rm21 = "";
    String roomname = "";
    String ns;
    String room1 = "", room2 = "",room3 = "",room4 = "", room6="", room7="", room8="", room9="", room10="", room11="";
    String room12 = "", room13 = "",room14 = "",room15 = "", room16="", room17="", room18="", room19="", room20="", room21="";
    ArrayList<String> users;
    ServerSocket serverSock;
    ArrayList clientOutputStreams;
    private volatile boolean soundstoprm1 = false;
    private volatile boolean soundstoprm2 = false;
    private volatile boolean soundstoprm3 = false;
    private volatile boolean soundstoprm4 = false;
    private volatile boolean soundstoprm6 = false;
    private volatile boolean soundstoprm7 = false;
    private volatile boolean soundstoprm8 = false;
    private volatile boolean soundstoprm9 = false;
    private volatile boolean soundstoprm10 = false;
    private volatile boolean soundstoprm11 = false;
    private volatile boolean soundstoprm12 = false;
    private volatile boolean soundstoprm13 = false;
    private volatile boolean soundstoprm14 = false;
    private volatile boolean soundstoprm15 = false;
    private volatile boolean soundstoprm16 = false;
    private volatile boolean soundstoprm17 = false;
    private volatile boolean soundstoprm18 = false;
    private volatile boolean soundstoprm19 = false;
    private volatile boolean soundstoprm20 = false;
    private volatile boolean soundstoprm21 = false;
    private boolean checksoundrm1 = false;
    private boolean checksoundrm2 = false;
    private boolean checksoundrm3 = false;
    private boolean checksoundrm4 = false;
    private boolean checksoundrm6 = false;
    private boolean checksoundrm7 = false;
    private boolean checksoundrm8 = false;
    private boolean checksoundrm9 = false;
    private boolean checksoundrm10 = false;
    private boolean checksoundrm11 = false;
    private boolean checksoundrm12 = false;
    private boolean checksoundrm13 = false;
    private boolean checksoundrm14 = false;
    private boolean checksoundrm15 = false;
    private boolean checksoundrm16 = false;
    private boolean checksoundrm17 = false;
    private boolean checksoundrm18 = false;
    private boolean checksoundrm19 = false;
    private boolean checksoundrm20 = false;
    private boolean checksoundrm21 = false;
    private volatile boolean exitrm1 = false;
    private volatile boolean exitrm2 = false;
    private volatile boolean exitrm3 = false;
    private volatile boolean exitrm4 = false;
    private volatile boolean exitrm6 = false;
    private volatile boolean exitrm7 = false;
    private volatile boolean exitrm8 = false;
    private volatile boolean exitrm9 = false;
    private volatile boolean exitrm10 = false;
    private volatile boolean exitrm11 = false;
    private volatile boolean exitrm12 = false;
    private volatile boolean exitrm13 = false;
    private volatile boolean exitrm14 = false;
    private volatile boolean exitrm15 = false;
    private volatile boolean exitrm16 = false;
    private volatile boolean exitrm17 = false;
    private volatile boolean exitrm18 = false;
    private volatile boolean exitrm19 = false;
    private volatile boolean exitrm20 = false;
    private volatile boolean exitrm21 = false;
    private boolean checkstoprm1 = false;
    private boolean checkstoprm2 = false;
    private boolean checkstoprm3 = false;
    private boolean checkstoprm4 = false;
    private boolean checkstoprm6 = false;
    private boolean checkstoprm7 = false;
    private boolean checkstoprm8 = false;
    private boolean checkstoprm9 = false;
    private boolean checkstoprm10 = false;
    private boolean checkstoprm11 = false;
    private boolean checkstoprm12 = false;
    private boolean checkstoprm13 = false;
    private boolean checkstoprm14 = false;
    private boolean checkstoprm15 = false;
    private boolean checkstoprm16 = false;
    private boolean checkstoprm17 = false;
    private boolean checkstoprm18 = false;
    private boolean checkstoprm19 = false;
    private boolean checkstoprm20 = false;
    private boolean checkstoprm21 = false;
    HeadnurseRoomCallStation3.AlarmSoundrm1 as = new HeadnurseRoomCallStation3.AlarmSoundrm1();
    Thread alarmrm1 = new Thread(as);
    HeadnurseRoomCallStation3.AlarmSoundrm2 as2 = new HeadnurseRoomCallStation3.AlarmSoundrm2();
    Thread alarmrm2 = new Thread(as2);
    HeadnurseRoomCallStation3.AlarmSoundrm3 as3 = new HeadnurseRoomCallStation3.AlarmSoundrm3();
    Thread alarmrm3 = new Thread(as3);
    HeadnurseRoomCallStation3.AlarmSoundrm3 as4 = new HeadnurseRoomCallStation3.AlarmSoundrm3();
    Thread alarmrm4 = new Thread(as4);
    HeadnurseRoomCallStation3.AlarmSoundrm6 as6 = new HeadnurseRoomCallStation3.AlarmSoundrm6();
    Thread alarmrm6 = new Thread(as6);
    HeadnurseRoomCallStation3.AlarmSoundrm7 as7 = new HeadnurseRoomCallStation3.AlarmSoundrm7();
    Thread alarmrm7 = new Thread(as7);
    HeadnurseRoomCallStation3.AlarmSoundrm8 as8 = new HeadnurseRoomCallStation3.AlarmSoundrm8();
    Thread alarmrm8 = new Thread(as8);
    HeadnurseRoomCallStation3.AlarmSoundrm9 as9 = new HeadnurseRoomCallStation3.AlarmSoundrm9();
    Thread alarmrm9 = new Thread(as9);
    HeadnurseRoomCallStation3.AlarmSoundrm10 as10 = new HeadnurseRoomCallStation3.AlarmSoundrm10();
    Thread alarmrm10 = new Thread(as10);
    HeadnurseRoomCallStation3.AlarmSoundrm11 as11 = new HeadnurseRoomCallStation3.AlarmSoundrm11();
    Thread alarmrm11 = new Thread(as11);
    HeadnurseRoomCallStation3.AlarmSoundrm12 as12 = new HeadnurseRoomCallStation3.AlarmSoundrm12();
    Thread alarmrm12 = new Thread(as12);
    HeadnurseRoomCallStation3.AlarmSoundrm13 as13 = new HeadnurseRoomCallStation3.AlarmSoundrm13();
    Thread alarmrm13 = new Thread(as13);
    HeadnurseRoomCallStation3.AlarmSoundrm14 as14 = new HeadnurseRoomCallStation3.AlarmSoundrm14();
    Thread alarmrm14 = new Thread(as14);
    HeadnurseRoomCallStation3.AlarmSoundrm15 as15 = new HeadnurseRoomCallStation3.AlarmSoundrm15();
    Thread alarmrm15 = new Thread(as15);
    HeadnurseRoomCallStation3.AlarmSoundrm16 as16 = new HeadnurseRoomCallStation3.AlarmSoundrm16();
    Thread alarmrm16 = new Thread(as16);
    HeadnurseRoomCallStation3.AlarmSoundrm17 as17 = new HeadnurseRoomCallStation3.AlarmSoundrm17();
    Thread alarmrm17 = new Thread(as17);
    HeadnurseRoomCallStation3.AlarmSoundrm18 as18 = new HeadnurseRoomCallStation3.AlarmSoundrm18();
    Thread alarmrm18 = new Thread(as18);
    HeadnurseRoomCallStation3.AlarmSoundrm19 as19 = new HeadnurseRoomCallStation3.AlarmSoundrm19();
    Thread alarmrm19 = new Thread(as19);
    HeadnurseRoomCallStation3.AlarmSoundrm20 as20 = new HeadnurseRoomCallStation3.AlarmSoundrm20();
    Thread alarmrm20 = new Thread(as20);
    HeadnurseRoomCallStation3.AlarmSoundrm21 as21 = new HeadnurseRoomCallStation3.AlarmSoundrm21();
    Thread alarmrm21 = new Thread(as21);
    
    HeadnurseRoomCallStation3.FlashPanelStartrm1 fps = new HeadnurseRoomCallStation3.FlashPanelStartrm1();
    Thread flash = new Thread(fps);
    HeadnurseRoomCallStation3.FlashPanelStartrm2 fps2 = new HeadnurseRoomCallStation3.FlashPanelStartrm2();
    Thread flash2 = new Thread(fps2);
    HeadnurseRoomCallStation3.FlashPanelStartrm3 fps3 = new HeadnurseRoomCallStation3.FlashPanelStartrm3();
    Thread flash3 = new Thread(fps3);
    HeadnurseRoomCallStation3.FlashPanelStartrm4 fps4 = new HeadnurseRoomCallStation3.FlashPanelStartrm4();
    Thread flash4 = new Thread(fps4);
    HeadnurseRoomCallStation3.FlashPanelStartrm6 fps6 = new HeadnurseRoomCallStation3.FlashPanelStartrm6();
    Thread flash6 = new Thread(fps6);
    HeadnurseRoomCallStation3.FlashPanelStartrm7 fps7 = new HeadnurseRoomCallStation3.FlashPanelStartrm7();
    Thread flash7 = new Thread(fps7);
    HeadnurseRoomCallStation3.FlashPanelStartrm8 fps8 = new HeadnurseRoomCallStation3.FlashPanelStartrm8();
    Thread flash8 = new Thread(fps8);
    HeadnurseRoomCallStation3.FlashPanelStartrm9 fps9 = new HeadnurseRoomCallStation3.FlashPanelStartrm9();
    Thread flash9 = new Thread(fps9);
    HeadnurseRoomCallStation3.FlashPanelStartrm10 fps10 = new HeadnurseRoomCallStation3.FlashPanelStartrm10();
    Thread flash10 = new Thread(fps10);
    HeadnurseRoomCallStation3.FlashPanelStartrm11 fps11 = new HeadnurseRoomCallStation3.FlashPanelStartrm11();
    Thread flash11 = new Thread(fps11);
    HeadnurseRoomCallStation3.FlashPanelStartrm12 fps12 = new HeadnurseRoomCallStation3.FlashPanelStartrm12();
    Thread flash12 = new Thread(fps12);
    HeadnurseRoomCallStation3.FlashPanelStartrm13 fps13 = new HeadnurseRoomCallStation3.FlashPanelStartrm13();
    Thread flash13 = new Thread(fps13);
    HeadnurseRoomCallStation3.FlashPanelStartrm14 fps14 = new HeadnurseRoomCallStation3.FlashPanelStartrm14();
    Thread flash14 = new Thread(fps14);
    HeadnurseRoomCallStation3.FlashPanelStartrm15 fps15 = new HeadnurseRoomCallStation3.FlashPanelStartrm15();
    Thread flash15 = new Thread(fps15);
    HeadnurseRoomCallStation3.FlashPanelStartrm16 fps16 = new HeadnurseRoomCallStation3.FlashPanelStartrm16();
    Thread flash16 = new Thread(fps16);
    HeadnurseRoomCallStation3.FlashPanelStartrm17 fps17 = new HeadnurseRoomCallStation3.FlashPanelStartrm17();
    Thread flash17 = new Thread(fps17);
    HeadnurseRoomCallStation3.FlashPanelStartrm18 fps18 = new HeadnurseRoomCallStation3.FlashPanelStartrm18();
    Thread flash18 = new Thread(fps18);
    HeadnurseRoomCallStation3.FlashPanelStartrm19 fps19 = new HeadnurseRoomCallStation3.FlashPanelStartrm19();
    Thread flash19 = new Thread(fps19);
    HeadnurseRoomCallStation3.FlashPanelStartrm20 fps20 = new HeadnurseRoomCallStation3.FlashPanelStartrm20();
    Thread flash20 = new Thread(fps20);
    HeadnurseRoomCallStation3.FlashPanelStartrm21 fps21 = new HeadnurseRoomCallStation3.FlashPanelStartrm21();
    Thread flash21 = new Thread(fps21);
    private String[] nurseStation = new String[10];
    /**
     * Creates new form adminManageBuildingView
     */
    public HeadnurseRoomCallStation3() throws IOException {
        initComponents();
        Panelrm1.setVisible(false);
        Panelrm2.setVisible(false);
        Panelrm3.setVisible(false);
        Panelrm4.setVisible(false);
        Panelrm6.setVisible(false);
        Panelrm7.setVisible(false);
        Panelrm8.setVisible(false);
        Panelrm9.setVisible(false);
        Panelrm10.setVisible(false);
        Panelrm11.setVisible(false);
        Panelrm12.setVisible(false);
        Panelrm13.setVisible(false);
        Panelrm14.setVisible(false);
        Panelrm15.setVisible(false);
        Panelrm16.setVisible(false);
        Panelrm17.setVisible(false);
        Panelrm18.setVisible(false);
        Panelrm19.setVisible(false);
        Panelrm20.setVisible(false);
        Panelrm21.setVisible(false);
       
        
    }
    public void data(String nsStation) throws IOException{
        ns = nsStation;
        int numOfNS = 1;
        RoomInformation info = new RoomInformation();
        info.getRoom();
         FunctionNurseStationGetter a = new FunctionNurseStationGetter();
        nurseStation = a.getNS(nsStation);
        for(int x =0; nurseStation[x] != null;x++){
            switch (numOfNS) {
                case 1:
                    Panelrm1.setVisible(true);
                    r1.setText(nurseStation[x]);
                    room1 = nurseStation[x];
                    break;
                case 2:
                    Panelrm2.setVisible(true);
                    r2.setText(nurseStation[x]);
                    room2 = nurseStation[x];
                    break;
                case 3:
                    Panelrm3.setVisible(true);
                    r3.setText(nurseStation[x]);
                    room3 = nurseStation[x];
                    break;
                case 4:
                    Panelrm4.setVisible(true);
                    r4.setText(nurseStation[x]);
                    room4 = nurseStation[x];
                    break;
                case 5:
                    Panelrm6.setVisible(true);
                    r6.setText(nurseStation[x]);
                    room6 = nurseStation[x];
                    break;
                case 6:
                    Panelrm7.setVisible(true);
                    r7.setText(nurseStation[x]);
                    room7 = nurseStation[x];
                    break;
                case 7:
                    Panelrm8.setVisible(true);
                    r8.setText(nurseStation[x]);
                    room8 = nurseStation[x];
                    break;
                case 8:
                    Panelrm9.setVisible(true);
                    r9.setText(nurseStation[x]);
                    room9 = nurseStation[x];
                    break;
                case 9:
                    Panelrm10.setVisible(true);
                    r10.setText(nurseStation[x]);
                    room10 = nurseStation[x];
                    break;
                case 10:
                    Panelrm11.setVisible(true);
                    r11.setText(nurseStation[x]);
                    room11 = nurseStation[x];
                    break;
                case 11:
                    Panelrm12.setVisible(true);
                    r12.setText(nurseStation[x]);
                    room12 = nurseStation[x];
                    break;
                case 12:
                    Panelrm13.setVisible(true);
                    r13.setText(nurseStation[x]);
                    room13 = nurseStation[x];
                    break;
                case 13:
                    Panelrm14.setVisible(true);
                    r14.setText(nurseStation[x]);
                    room14 = nurseStation[x];
                    break;
                case 14:
                    Panelrm15.setVisible(true);
                    r15.setText(nurseStation[x]);
                    room15 = nurseStation[x];
                    break;
                case 15:
                    Panelrm16.setVisible(true);
                    r16.setText(nurseStation[x]);
                    room16 = nurseStation[x];
                    break;
                case 16:
                    Panelrm17.setVisible(true);
                    r17.setText(nurseStation[x]);
                    room17 = nurseStation[x];
                    break;
                case 17:
                    Panelrm18.setVisible(true);
                    r18.setText(nurseStation[x]);
                    room18 = nurseStation[x];
                    break;
                case 18:
                    Panelrm19.setVisible(true);
                    r19.setText(nurseStation[x]);
                    room19 = nurseStation[x];
                    break;
                case 19:
                    Panelrm20.setVisible(true);
                    r20.setText(nurseStation[x]);
                    room20 = nurseStation[x];
                    break;
                case 20:
                    Panelrm21.setVisible(true);
                    r21.setText(nurseStation[x]);
                    room21 = nurseStation[x];
                    break;
                
                default:
                    break;
            }
            numOfNS++;
        }
    }
    /**
     * NAME: ServerStart
     * PURPOSE: This class will Start the server and waits for a client.
     */
    public class ServerStart implements Runnable 
    {
        @Override
        public void run() 
        {
            clientOutputStreams = new ArrayList();
            users = new ArrayList();  
            
            try{
                serverSock = new ServerSocket(1111);
                
                while (true){
                    Socket clientSock = serverSock.accept();
                    PrintWriter writer = new PrintWriter(clientSock.getOutputStream());
                    clientOutputStreams.add(writer);

                    Thread listener = new Thread(new ClientHandler(clientSock, writer));
                    listener.start();
                }
            } catch (IOException ex){
                if (serverSock != null && !serverSock.isClosed()) {
                    try {
                        serverSock.close();
                        endThreadColorrm1();
                        endThreadColorrm2();
                        endThreadColorrm3();
                        endThreadColorrm4();
                        endThreadColorrm6();
                        endThreadColorrm7();
                        endThreadColorrm8();
                        endThreadColorrm9();
                        endThreadColorrm10();
                        endThreadColorrm11();
                        endThreadColorrm12();
                        endThreadColorrm13();
                        endThreadColorrm14();
                        endThreadColorrm15();
                        endThreadColorrm16();
                        endThreadColorrm17();
                        endThreadColorrm18();
                        endThreadColorrm19();
                        endThreadColorrm20();
                        endThreadColorrm21();
                        endAlarmrm1();
                        endAlarmrm2();
                        endAlarmrm3();
                        endAlarmrm4();
                        endAlarmrm6();
                        endAlarmrm7();
                        endAlarmrm8();
                        endAlarmrm9();
                        endAlarmrm10();
                        endAlarmrm11();
                        endAlarmrm12();
                        endAlarmrm13();
                        endAlarmrm14();
                        endAlarmrm15();
                        endAlarmrm16();
                        endAlarmrm17();
                        endAlarmrm18();
                        endAlarmrm19();
                        endAlarmrm20();
                        endAlarmrm21();
                    } catch (IOException e) {
                        e.printStackTrace(System.err);
                    }
                }
            }
        }
    }
    /**
     * NAME: ClientHandler
     * PURPOSE: This class handles client requests.
     */
    public class ClientHandler implements Runnable	
   {
       BufferedReader reader;
       Socket sock;
       PrintWriter client;

       public ClientHandler(Socket clientSocket, PrintWriter user) 
       {
            client = user;
            
            try{
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader);
            } catch (Exception ex){
                JOptionPane.showMessageDialog(null, ex);
            }

       }
       @Override
       public void run() 
       {
            String message, connect = "Connect", disconnect = "Disconnect";
            String[] data;

            try{
                while ((message = reader.readLine()) != null){
                    data = message.split(":");

                    if (data[2].equals(connect)){
                        roomname = data[0];
                        if(rm1.isEmpty() && roomname.equals(room1)){
                            rm1 = data[0];
                        }else if(rm2.isEmpty() && roomname.equals(room2)){
                            rm2 = data[0];
                        }else if(rm3.isEmpty() && roomname.equals(room3)){
                            rm3 = data[0];
                        }else if(rm4.isEmpty() && roomname.equals(room4)){
                            rm4 = data[0];
                        }else if(rm6.isEmpty() && roomname.equals(room6)){
                            rm6 = data[0];
                        }else if(rm7.isEmpty() && roomname.equals(room7)){
                            rm7 = data[0];
                        }else if(rm8.isEmpty() && roomname.equals(room8)){
                            rm8 = data[0];
                        }else if(rm9.isEmpty() && roomname.equals(room9)){
                            rm9 = data[0];
                        }else if(rm10.isEmpty() && roomname.equals(room10)){
                            rm10 = data[0];
                        }else if(rm11.isEmpty() && roomname.equals(room11)){
                            rm11 = data[0];
                        }else if(rm12.isEmpty() && roomname.equals(room12)){
                            rm12 = data[0];
                        }else if(rm13.isEmpty() && roomname.equals(room13)){
                            rm13 = data[0];
                        }else if(rm14.isEmpty() && roomname.equals(room14)){
                            rm14 = data[0];
                        }else if(rm15.isEmpty() && roomname.equals(room15)){
                            rm15 = data[0];
                        }else if(rm16.isEmpty() && roomname.equals(room16)){
                            rm16 = data[0];
                        }else if(rm17.isEmpty() && roomname.equals(room17)){
                            rm17 = data[0];
                        }else if(rm18.isEmpty() && roomname.equals(room18)){
                            rm18 = data[0];
                        }else if(rm19.isEmpty() && roomname.equals(room19)){
                            rm19 = data[0];
                        }else if(rm20.isEmpty() && roomname.equals(room20)){
                            rm20 = data[0];
                        }else if(rm21.isEmpty() && roomname.equals(room21)){
                            rm21 = data[0];
                        }
                        userAdd(data[0]);
                    } 
                    else if (data[2].equals(disconnect)){
                        if(data[0].equals(rm1)){
                            rm1 = "";
                            endThreadColorrm1();
                            endAlarmrm1();
                        
                        }else if(data[0].equals(rm2)){
                            rm2 = "";
                            endThreadColorrm2();
                            endAlarmrm2();
                        
                        }else if(data[0].equals(rm3)){
                            rm3 = "";
                            endThreadColorrm3();
                            endAlarmrm3();
                        
                        }else if(data[0].equals(rm4)){
                            rm4 = "";
                            endThreadColorrm4();
                            endAlarmrm4();
                        
                        }else if(data[0].equals(rm6)){
                            rm6 = "";
                            endThreadColorrm6();
                            endAlarmrm6();
                        
                        }else if(data[0].equals(rm7)){
                            rm7 = "";
                            endThreadColorrm7();
                            endAlarmrm7();
                        
                        }else if(data[0].equals(rm8)){
                            rm8 = "";
                            endThreadColorrm8();
                            endAlarmrm8();
                        
                        }else if(data[0].equals(rm9)){
                            rm9 = "";
                            endThreadColorrm9();
                            endAlarmrm9();
                        
                        }else if(data[0].equals(rm10)){
                            rm10 = "";
                            endThreadColorrm10();
                            endAlarmrm10();
                        
                        }else if(data[0].equals(rm11)){
                            rm11 = "";
                            endThreadColorrm11();
                            endAlarmrm11();
                        
                        }else if(data[0].equals(rm12)){
                            rm12 = "";
                            endThreadColorrm12();
                            endAlarmrm12();
                        
                        }else if(data[0].equals(rm13)){
                            rm13 = "";
                            endThreadColorrm13();
                            endAlarmrm13();
                        
                        }else if(data[0].equals(rm14)){
                            rm14 = "";
                            endThreadColorrm14();
                            endAlarmrm14();
                        
                        }else if(data[0].equals(rm15)){
                            rm15 = "";
                            endThreadColorrm15();
                            endAlarmrm15();
                        
                        }else if(data[0].equals(rm16)){
                            rm16 = "";
                            endThreadColorrm16();
                            endAlarmrm16();
                        
                        }else if(data[0].equals(rm17)){
                            rm17 = "";
                            endThreadColorrm17();
                            endAlarmrm17();
                        
                        }else if(data[0].equals(rm18)){
                            rm18 = "";
                            endThreadColorrm18();
                            endAlarmrm18();
                        
                        }else if(data[0].equals(rm19)){
                            rm19 = "";
                            endThreadColorrm19();
                            endAlarmrm19();
                        
                        }else if(data[0].equals(rm20)){
                            rm20 = "";
                            endThreadColorrm20();
                            endAlarmrm20();
                        
                        }else if(data[0].equals(rm21)){
                            rm21 = "";
                            endThreadColorrm21();
                            endAlarmrm21();
                        
                        }
                    }
                } 
            } catch (Exception ex){
               ex.printStackTrace();
               clientOutputStreams.remove(client);
            } 
	} 
    }
    /***********************************************************************************************************
     * NAME: newThreadStart
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     ***********************************************************************************************************/
    public void newThreadStart()
    {
        if(flash.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm1 == true){
            checkstoprm1 = false;
            exitrm1 = false;
            flash = new Thread(fps);
            flash.start();
        }else{
            flash.start();
        }
    }
    /**
     * NAME: newThreadStart
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart2()
    {
        if(flash2.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm2 == true){
            checkstoprm2 = false;
            exitrm2 = false;
            flash2 = new Thread(fps2);
            flash2.start();
        }else{
            flash2.start();
        }
    }
    /**
     * NAME: newThreadStart
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart898()
    {
        if(flash3.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm3 == true){
            checkstoprm3 = false;
            exitrm3 = false;
            flash3 = new Thread(fps3);
            flash3.start();
        }else{
            flash3.start();
        }
    }
    /**
     * NAME: newThreadStart4
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart4()
    {
        if(flash4.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm4 == true){
            checkstoprm4 = false;
            exitrm4 = false;
            flash4 = new Thread(fps4);
            flash4.start();
        }else{
            flash4.start();
        }
    }
    /**
     * NAME: newThreadStart6
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart6()
    {
        if(flash6.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm6 == true){
            checkstoprm6 = false;
            exitrm6 = false;
            flash6 = new Thread(fps6);
            flash6.start();
        }else{
            flash6.start();
        }
    }
    /**
     * NAME: newThreadStart7
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart7()
    {
        if(flash7.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm7 == true){
            checkstoprm7 = false;
            exitrm7 = false;
            flash7 = new Thread(fps7);
            flash7.start();
        }else{
            flash7.start();
        }
    }
    /**
     * NAME: newThreadStart8
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart8()
    {
        if(flash8.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm8 == true){
            checkstoprm8 = false;
            exitrm8 = false;
            flash8 = new Thread(fps8);
            flash8.start();
        }else{
            flash8.start();
        }
    }
    /**
     * NAME: newThreadStart9
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart9()
    {
        if(flash9.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm9 == true){
            checkstoprm9 = false;
            exitrm9 = false;
            flash9 = new Thread(fps9);
            flash9.start();
        }else{
            flash9.start();
        }
    }
    /**
     * NAME: newThreadStart10
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart10()
    {
        if(flash10.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm10 == true){
            checkstoprm10 = false;
            exitrm10 = false;
            flash10 = new Thread(fps10);
            flash10.start();
        }else{
            flash10.start();
        }
    }
    /**
     * NAME: newThreadStart11
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart11()
    {
        if(flash11.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm11 == true){
            checkstoprm11 = false;
            exitrm11 = false;
            flash11 = new Thread(fps11);
            flash11.start();
        }else{
            flash11.start();
        }
    }
    /***********************************************************************************************************
     * NAME: newThreadStart
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     ***********************************************************************************************************/
    public void newThreadStart12()
    {
        if(flash12.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm12 == true){
            checkstoprm12 = false;
            exitrm12 = false;
            flash12 = new Thread(fps12);
            flash12.start();
        }else{
            flash12.start();
        }
    }
    /**
     * NAME: newThreadStart13
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart13()
    {
        if(flash13.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm13 == true){
            checkstoprm13 = false;
            exitrm13 = false;
            flash13 = new Thread(fps13);
            flash13.start();
        }else{
            flash13.start();
        }
    }
    /**
     * NAME: newThreadStart
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart14()
    {
        if(flash14.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm14 == true){
            checkstoprm14 = false;
            exitrm14 = false;
            flash14 = new Thread(fps14);
            flash14.start();
        }else{
            flash14.start();
        }
    }
    /**
     * NAME: newThreadStart15
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart15()
    {
        if(flash15.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm15 == true){
            checkstoprm15 = false;
            exitrm15 = false;
            flash15 = new Thread(fps15);
            flash15.start();
        }else{
            flash15.start();
        }
    }
    /**
     * NAME: newThreadStart6
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart16()
    {
        if(flash16.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm16 == true){
            checkstoprm16 = false;
            exitrm16 = false;
            flash16 = new Thread(fps16);
            flash16.start();
        }else{
            flash16.start();
        }
    }
    /**
     * NAME: newThreadStart7
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart17()
    {
        if(flash17.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm17 == true){
            checkstoprm17 = false;
            exitrm17 = false;
            flash17 = new Thread(fps17);
            flash17.start();
        }else{
            flash17.start();
        }
    }
    /**
     * NAME: newThreadStart8
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart18()
    {
        if(flash18.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm18 == true){
            checkstoprm18 = false;
            exitrm18 = false;
            flash18 = new Thread(fps18);
            flash18.start();
        }else{
            flash18.start();
        }
    }
    /**
     * NAME: newThreadStart9
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart19()
    {
        if(flash19.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm19 == true){
            checkstoprm19 = false;
            exitrm19 = false;
            flash19 = new Thread(fps19);
            flash19.start();
        }else{
            flash19.start();
        }
    }
    /**
     * NAME: newThreadStart20
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart20()
    {
        if(flash20.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm20 == true){
            checkstoprm20 = false;
            exitrm20 = false;
            flash20 = new Thread(fps20);
            flash20.start();
        }else{
            flash20.start();
        }
    }
    /**
     * NAME: newThreadStart21
     * PURPOSE: this method will start a new thread.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newThreadStart21()
    {
        if(flash21.isAlive()){
            System.out.println("Thread is already running");
        }else if(checkstoprm21 == true){
            checkstoprm21 = false;
            exitrm21 = false;
            flash21 = new Thread(fps21);
            flash21.start();
        }else{
            flash21.start();
        }
    }
    
    
    /**********************************************************************************************************
     * NAME: endThreadColorrm1
     * PURPOSE: this method will end the thread color for the panel rm1.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     *********************************************************************************************************/
    public void endThreadColorrm1()
    {
        fps.stopThread();
        checkstoprm1 = true;
        Thread flashend = new Thread(new Flashend());
        flashend.start();
    }
    /*
    * NAME: endThreadColorrm2
     * PURPOSE: this method will end the thread color for the panel LB567.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm2()
    {
        fps2.stopThread();
        checkstoprm2 = true;
        Thread flashend2 = new Thread(new Flashend2());
        flashend2.start();
    }
    /*
    * NAME: endThreadColorrm3
     * PURPOSE: this method will end the thread color for the panel rm3.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm3()
    {
        fps3.stopThread();
        checkstoprm3 = true;
        Thread flashend3 = new Thread(new Flashend3());
        flashend3.start();
    }
    /*
    * NAME: endThreadColorrm4
     * PURPOSE: this method will end the thread color for the panel rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm4()
    {
        fps4.stopThread();
        checkstoprm4 = true;
        Thread flashend4 = new Thread(new Flashend4());
        flashend4.start();
    }
    /*
    * NAME: endThreadColorrm6
     * PURPOSE: this method will end the thread color for the panel rm6.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm6()
    {
        fps6.stopThread();
        checkstoprm6 = true;
        Thread flashend6 = new Thread(new Flashend6());
        flashend6.start();
    }/*
    * NAME: endThreadColorrm7
     * PURPOSE: this method will end the thread color for the panel rm7.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm7()
    {
        fps7.stopThread();
        checkstoprm7 = true;
        Thread flashend7 = new Thread(new Flashend7());
        flashend7.start();
    }
    /*
    * NAME: endThreadColorrm8
     * PURPOSE: this method will end the thread color for the panel rm8.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm8()
    {
        fps8.stopThread();
        checkstoprm8 = true;
        Thread flashend8 = new Thread(new Flashend8());
        flashend8.start();
    }
    /*
    * NAME: endThreadColorrm9
     * PURPOSE: this method will end the thread color for the panel rm9.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm9()
    {
        fps9.stopThread();
        checkstoprm9 = true;
        Thread flashend9 = new Thread(new Flashend9());
        flashend9.start();
    }
    /*
    * NAME: endThreadColorrm10
     * PURPOSE: this method will end the thread color for the panel rm10.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm10()
    {
        fps10.stopThread();
        checkstoprm10 = true;
        Thread flashend10 = new Thread(new Flashend10());
        flashend10.start();
    }
    /*
    * NAME: endThreadColorrm11
     * PURPOSE: this method will end the thread color for the panel rm11.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm11()
    {
        fps11.stopThread();
        checkstoprm11 = true;
        Thread flashend11 = new Thread(new Flashend11());
        flashend11.start();
    }
    public void endThreadColorrm12()
    {
        fps12.stopThread();
        checkstoprm12 = true;
        Thread flashend12 = new Thread(new Flashend12());
        flashend12.start();
    }
    /*
    * NAME: endThreadColorrm13
     * PURPOSE: this method will end the thread color for the panel LB567.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm13()
    {
        fps13.stopThread();
        checkstoprm13 = true;
        Thread flashend13 = new Thread(new Flashend13());
        flashend13.start();
    }
    /*
    * NAME: endThreadColorrm14
     * PURPOSE: this method will end the thread color for the panel rm3.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm14()
    {
        fps14.stopThread();
        checkstoprm14 = true;
        Thread flashend14 = new Thread(new Flashend14());
        flashend14.start();
    }
    /*
    * NAME: endThreadColorrm15
     * PURPOSE: this method will end the thread color for the panel rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm15()
    {
        fps15.stopThread();
        checkstoprm15 = true;
        Thread flashend15 = new Thread(new Flashend15());
        flashend15.start();
    }
    /*
    * NAME: endThreadColorrm16
     * PURPOSE: this method will end the thread color for the panel rm16.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm16()
    {
        fps16.stopThread();
        checkstoprm16 = true;
        Thread flashend16 = new Thread(new Flashend16());
        flashend16.start();
    }/*
    * NAME: endThreadColorrm17
     * PURPOSE: this method will end the thread color for the panel rm17.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm17()
    {
        fps17.stopThread();
        checkstoprm17 = true;
        Thread flashend17 = new Thread(new Flashend17());
        flashend17.start();
    }
    /*
    * NAME: endThreadColorrm8
     * PURPOSE: this method will end the thread color for the panel rm8.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm18()
    {
        fps18.stopThread();
        checkstoprm18 = true;
        Thread flashend18 = new Thread(new Flashend18());
        flashend18.start();
    }
    /*
    * NAME: endThreadColorrm19
     * PURPOSE: this method will end the thread color for the panel rm19.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm19()
    {
        fps19.stopThread();
        checkstoprm19 = true;
        Thread flashend19 = new Thread(new Flashend19());
        flashend19.start();
    }
    /*
    * NAME: endThreadColorrm20
     * PURPOSE: this method will end the thread color for the panel rm20.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm20()
    {
        fps20.stopThread();
        checkstoprm20 = true;
        Thread flashend20 = new Thread(new Flashend20());
        flashend20.start();
    }
    /*
    * NAME: endThreadColorrm11
     * PURPOSE: this method will end the thread color for the panel rm11.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
    */
    
    public void endThreadColorrm21()
    {
        fps21.stopThread();
        checkstoprm21 = true;
        Thread flashend21 = new Thread(new Flashend21());
        flashend21.start();
    }
   /***********************************************************************************************************
     * NAME: Flashend
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     **********************************************************************************************************/
    public class Flashend implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm1.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend2 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm2.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend3 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm3.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
   /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend4 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm4.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend6
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend6 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm6.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend7 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm7.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend8
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend8 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm8.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend9 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm9.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend10 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm10.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend11
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend11 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm11.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    public class Flashend12 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm12.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend13 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm13.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend14 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm14.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
   /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend15 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm15.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend16
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend16 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm16.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend17
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend17 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm17.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend8
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend18 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm18.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend19 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm19.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend4
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend20 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm20.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*
     * NAME: Flashend11
     * PURPOSE: this class will turn the panel to its original color once the client disconnected.
     */
    public class Flashend21 implements Runnable
    {
        @Override
        public void run(){
             try {
                 Thread.sleep(500);
                 Panelrm21.setBackground(new Color(240, 240, 240));
             } catch (InterruptedException ex) {
                 Thread.currentThread().interrupt();
             }
        }
    }
    /*************************************************************************************************************
     * NAME: FlashPanelStartLB446
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     ***********************************************************************************************************/
    public class FlashPanelStartrm1 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm1) {
                    i++;
                    if (i % 2 == 0) {
                        Panelrm1.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm1.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm1 = true;
            System.out.println("Process STOPPED");
            if(exitrm1 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    
    /**
     * NAME: FlashPanelStartrm2
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm2 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm2) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm2.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm2.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm2 = true;
            System.out.println("Process STOPPED");
            if(exitrm2 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
     /**
     * NAME: FlashPanelStartrm3
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm3 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm3) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm3.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm3.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm3 = true;
            System.out.println("Process STOPPED");
            if(exitrm3 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
     /**
     * NAME: FlashPanelStartrm4
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm4 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm4) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm4.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm4.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm4 = true;
            System.out.println("Process STOPPED");
            if(exitrm4 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm6
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm6 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm6) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm6.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm6.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm6 = true;
            System.out.println("Process STOPPED");
            if(exitrm6 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm7
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm7 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm7) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm7.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm7.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm7 = true;
            System.out.println("Process STOPPED");
            if(exitrm7 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm8
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm8 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm8) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm8.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm8.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm8 = true;
            System.out.println("Process STOPPED");
            if(exitrm8 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm9
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm9 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm9) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm9.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm9.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm9 = true;
            System.out.println("Process STOPPED");
            if(exitrm9 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm10
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm10 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm10) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm10.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm10.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm10 = true;
            System.out.println("Process STOPPED");
            if(exitrm10 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm11
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm11 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm11) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm11.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm11.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm11 = true;
            System.out.println("Process STOPPED");
            if(exitrm11 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    public class FlashPanelStartrm12 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm12) {
                    i++;
                    if (i % 2 == 0) {
                        Panelrm12.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm12.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm12 = true;
            System.out.println("Process STOPPED");
            if(exitrm12 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    
    /**
     * NAME: FlashPanelStartrm2
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm13 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm13) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm13.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm13.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm13 = true;
            System.out.println("Process STOPPED");
            if(exitrm13 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
     /**
     * NAME: FlashPanelStartrm3
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm14 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm14) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm14.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm14.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm14 = true;
            System.out.println("Process STOPPED");
            if(exitrm14 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
     /**
     * NAME: FlashPanelStartrm15
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm15 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm15) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm15.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm15.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm15 = true;
            System.out.println("Process STOPPED");
            if(exitrm15 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm6
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm16 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm16) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm16.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm16.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm16 = true;
            System.out.println("Process STOPPED");
            if(exitrm16 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm17
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm17 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm17) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm17.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm17.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm17 = true;
            System.out.println("Process STOPPED");
            if(exitrm17 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm8
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm18 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm18) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm18.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm18.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm18 = true;
            System.out.println("Process STOPPED");
            if(exitrm18 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm19
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm19 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm19) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm19.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm19.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm19 = true;
            System.out.println("Process STOPPED");
            if(exitrm19 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm10
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm20 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm20) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm20.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm20.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm20 = true;
            System.out.println("Process STOPPED");
            if(exitrm20 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: FlashPanelStartrm21
     * PURPOSE: this class will change the color of the panel to red and yellow as long as the client is connected.
     */
    public class FlashPanelStartrm21 implements Runnable
    {
        @Override
        public void run()
        {
            int i = 0;

                while(!exitrm21) {
                    i++;
                    if (i % 2 == 0) {
                        System.out.println("z");
                        Panelrm21.setBackground(Color.yellow);
                        System.out.println("Yellow\n");
                    }else{
                        Panelrm21.setBackground(Color.red);
                        System.out.println("Red\n");
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            exitrm21 = true;
            System.out.println("Process STOPPED");
            if(exitrm21 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /********************************************************************************************************
     * NAME: endAlarmrm1
     * PURPOSE: this method will end the alarm or the sound effect for rm1.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     *********************************************************************************************************/
    public void endAlarmrm1()
    {
        as.stopThread();
        checksoundrm1 = true;
    }
    /**
     * NAME: endAlarmrm2
     * PURPOSE: this method will end the alarm or the sound effect for rm2.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm2()
    {
        as2.stopThread();
        checksoundrm2 = true;
    }
    /**
     * NAME: endAlarmrm3
     * PURPOSE: this method will end the alarm or the sound effect for rm3.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm3()
    {
        as3.stopThread();
        checksoundrm3 = true;
    }
    /**
     * NAME: endAlarmrm4
     * PURPOSE: this method will end the alarm or the sound effect for rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm4()
    {
        as4.stopThread();
        checksoundrm4 = true;
    }
    /**
     * NAME: endAlarmrm6
     * PURPOSE: this method will end the alarm or the sound effect for rm6.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm6()
    {
        as6.stopThread();
        checksoundrm6 = true;
    }
    /**
     * NAME: endAlarmrm7
     * PURPOSE: this method will end the alarm or the sound effect for rm7.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm7()
    {
        as7.stopThread();
        checksoundrm7 = true;
    }
    /**
     * NAME: endAlarmrm8
     * PURPOSE: this method will end the alarm or the sound effect for rm8.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm8()
    {
        as8.stopThread();
        checksoundrm8 = true;
    }
    /**
     * NAME: endAlarmrm9
     * PURPOSE: this method will end the alarm or the sound effect for rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm9()
    {
        as9.stopThread();
        checksoundrm9 = true;
    }
    /**
     * NAME: endAlarmrm10
     * PURPOSE: this method will end the alarm or the sound effect for rm10.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm10()
    {
        as10.stopThread();
        checksoundrm10 = true;
    }
    /**
     * NAME: endAlarmrm11
     * PURPOSE: this method will end the alarm or the sound effect for rm11.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm11()
    {
        as11.stopThread();
        checksoundrm11 = true;
    }
    public void endAlarmrm12()
    {
        as12.stopThread();
        checksoundrm12 = true;
    }
    /**
     * NAME: endAlarmrm2
     * PURPOSE: this method will end the alarm or the sound effect for rm2.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm13()
    {
        as13.stopThread();
        checksoundrm13 = true;
    }
    /**
     * NAME: endAlarmrm14
     * PURPOSE: this method will end the alarm or the sound effect for rm3.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm14()
    {
        as14.stopThread();
        checksoundrm14 = true;
    }
    /**
     * NAME: endAlarmrm15
     * PURPOSE: this method will end the alarm or the sound effect for rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm15()
    {
        as15.stopThread();
        checksoundrm15 = true;
    }
    /**
     * NAME: endAlarmrm16
     * PURPOSE: this method will end the alarm or the sound effect for rm6.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm16()
    {
        as16.stopThread();
        checksoundrm16 = true;
    }
    /**
     * NAME: endAlarmrm17
     * PURPOSE: this method will end the alarm or the sound effect for rm7.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm17()
    {
        as17.stopThread();
        checksoundrm17 = true;
    }
    /**
     * NAME: endAlarmrm18
     * PURPOSE: this method will end the alarm or the sound effect for rm8.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm18()
    {
        as18.stopThread();
        checksoundrm18 = true;
    }
    /**
     * NAME: endAlarmrm19
     * PURPOSE: this method will end the alarm or the sound effect for rm4.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm19()
    {
        as19.stopThread();
        checksoundrm19 = true;
    }
    /**
     * NAME: endAlarmrm20
     * PURPOSE: this method will end the alarm or the sound effect for rm10.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm20()
    {
        as20.stopThread();
        checksoundrm20 = true;
    }
    /**
     * NAME: endAlarmrm21
     * PURPOSE: this method will end the alarm or the sound effect for rm11.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void endAlarmrm21()
    {
        as21.stopThread();
        checksoundrm21 = true;
    }
    /********************************************************************************************************
     * NAME: newAlarmStart
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     ********************************************************************************************************/
    public void newAlarmStart()
    {
        if(alarmrm1.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm1 == true){
            checksoundrm1 = false;
            soundstoprm1 = false;
            alarmrm1 = new Thread(as);
            alarmrm1.start();
        }else{
            alarmrm1.start();
        }
    }
    /**
     * NAME: newAlarmStart2
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart2()
    {
        if(alarmrm2.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm2 == true){
            checksoundrm2 = false;
            soundstoprm2 = false;
            alarmrm2 = new Thread(as2);
            alarmrm2.start();
        }else{
            alarmrm2.start();
        }
    }
    /**
     * NAME: newAlarmStart3
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart3()
    {
        if(alarmrm3.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm3 == true){
            checksoundrm3 = false;
            soundstoprm3 = false;
            alarmrm3 = new Thread(as3);
            alarmrm3.start();
        }else{
            alarmrm3.start();
        }
    }
    /**
     * NAME: newAlarmStart4
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart4()
    {
        if(alarmrm4.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm4 == true){
            checksoundrm4 = false;
            soundstoprm4 = false;
            alarmrm4 = new Thread(as4);
            alarmrm4.start();
        }else{
            alarmrm4.start();
        }
    }
    /**
     * NAME: newAlarmStart6
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart6()
    {
        if(alarmrm6.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm6 == true){
            checksoundrm6 = false;
            soundstoprm6 = false;
            alarmrm6 = new Thread(as6);
            alarmrm6.start();
        }else{
            alarmrm6.start();
        }
    }
    /**
     * NAME: newAlarmStart7
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart7()
    {
        if(alarmrm7.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm7 == true){
            checksoundrm7 = false;
            soundstoprm7 = false;
            alarmrm7 = new Thread(as7);
            alarmrm7.start();
        }else{
            alarmrm7.start();
        }
    }
    /**
     * NAME: newAlarmStart8
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart8()
    {
        if(alarmrm8.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm8 == true){
            checksoundrm8 = false;
            soundstoprm8 = false;
            alarmrm8 = new Thread(as8);
            alarmrm8.start();
        }else{
            alarmrm8.start();
        }
    }
    /**
     * NAME: newAlarmStart9
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart9()
    {
        if(alarmrm9.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm9 == true){
            checksoundrm9 = false;
            soundstoprm9 = false;
            alarmrm9 = new Thread(as9);
            alarmrm9.start();
        }else{
            alarmrm9.start();
        }
    }
    /**
     * NAME: newAlarmStart10
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart10()
    {
        if(alarmrm10.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm10 == true){
            checksoundrm10 = false;
            soundstoprm10 = false;
            alarmrm10 = new Thread(as10);
            alarmrm10.start();
        }else{
            alarmrm10.start();
        }
    }
    /**
     * NAME: newAlarmStart11
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart11()
    {
        if(alarmrm11.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm11 == true){
            checksoundrm11 = false;
            soundstoprm11 = false;
            alarmrm11 = new Thread(as11);
            alarmrm11.start();
        }else{
            alarmrm11.start();
        }
    }
    public void newAlarmStart12()
    {
        if(alarmrm12.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm12 == true){
            checksoundrm12 = false;
            soundstoprm12 = false;
            alarmrm12 = new Thread(as12);
            alarmrm12.start();
        }else{
            alarmrm12.start();
        }
    }
    /**
     * NAME: newAlarmStart3
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart13()
    {
        if(alarmrm13.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm13 == true){
            checksoundrm13 = false;
            soundstoprm13 = false;
            alarmrm13 = new Thread(as13);
            alarmrm13.start();
        }else{
            alarmrm13.start();
        }
    }
    /**
     * NAME: newAlarmStart14
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart14()
    {
        if(alarmrm14.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm14 == true){
            checksoundrm14 = false;
            soundstoprm14 = false;
            alarmrm14 = new Thread(as14);
            alarmrm14.start();
        }else{
            alarmrm14.start();
        }
    }
    /**
     * NAME: newAlarmStart15
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart15()
    {
        if(alarmrm15.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm15 == true){
            checksoundrm15 = false;
            soundstoprm15 = false;
            alarmrm15 = new Thread(as15);
            alarmrm15.start();
        }else{
            alarmrm15.start();
        }
    }
    /**
     * NAME: newAlarmStart16
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart16()
    {
        if(alarmrm16.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm16 == true){
            checksoundrm16 = false;
            soundstoprm16 = false;
            alarmrm16 = new Thread(as16);
            alarmrm16.start();
        }else{
            alarmrm16.start();
        }
    }
    /**
     * NAME: newAlarmStart17
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart17()
    {
        if(alarmrm17.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm17 == true){
            checksoundrm17 = false;
            soundstoprm17 = false;
            alarmrm17 = new Thread(as17);
            alarmrm17.start();
        }else{
            alarmrm17.start();
        }
    }
    /**
     * NAME: newAlarmStart18
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart18()
    {
        if(alarmrm18.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm18 == true){
            checksoundrm18 = false;
            soundstoprm18 = false;
            alarmrm18 = new Thread(as18);
            alarmrm18.start();
        }else{
            alarmrm18.start();
        }
    }
    /**
     * NAME: newAlarmStart19
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart19()
    {
        if(alarmrm19.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm19 == true){
            checksoundrm19 = false;
            soundstoprm19 = false;
            alarmrm19 = new Thread(as19);
            alarmrm19.start();
        }else{
            alarmrm19.start();
        }
    }
    /**
     * NAME: newAlarmStart20
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart20()
    {
        if(alarmrm20.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm20 == true){
            checksoundrm20 = false;
            soundstoprm20 = false;
            alarmrm20 = new Thread(as10);
            alarmrm20.start();
        }else{
            alarmrm20.start();
        }
    }
    /**
     * NAME: newAlarmStart21
     * PURPOSE: this method will start new alarm or the sound effect.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    public void newAlarmStart21()
    {
        if(alarmrm21.isAlive()){
            System.out.println("Thread is already running");
        }else if(checksoundrm21 == true){
            checksoundrm21 = false;
            soundstoprm21 = false;
            alarmrm21 = new Thread(as21);
            alarmrm21.start();
        }else{
            alarmrm21.start();
        }
    }
    
    /*********************************************************************************************************
     * NAME: AlarmSoundrm1
     * PURPOSE: this class will set the alarm or the sound effect for LB446.
     **********************************************************************************************************/
    public class AlarmSoundrm1 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm1) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm1 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm1 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm2
     * PURPOSE: this class will set the alarm or the sound effect for rm2.
     */
    public class AlarmSoundrm2 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm2) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm2 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm2 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm2
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm3 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm3) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm3 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm3 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm4
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm4 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm4) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm4 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm4 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm6
     * PURPOSE: this class will set the alarm or the sound effect for rm6.
     */
    public class AlarmSoundrm6 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm6) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm6 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm6 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm7
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm7 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm7) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm7 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm7 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm8
     * PURPOSE: this class will set the alarm or the sound effect for rm8.
     */
    public class AlarmSoundrm8 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm8) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm8 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm8 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm9
     * PURPOSE: this class will set the alarm or the sound effect for rm9.
     */
    public class AlarmSoundrm9 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm9) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm9 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm9 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm10
     * PURPOSE: this class will set the alarm or the sound effect for rm10.
     */
    public class AlarmSoundrm10 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm10) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm10 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm10 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm11
     * PURPOSE: this class will set the alarm or the sound effect for rm11.
     */
    public class AlarmSoundrm11 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm11) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm11 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm11 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    public class AlarmSoundrm12 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm12) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm12 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm12 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm13
     * PURPOSE: this class will set the alarm or the sound effect for rm2.
     */
    public class AlarmSoundrm13 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm13) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm13 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm13 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm14
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm14 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm14) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm14 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm14 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm15
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm15 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm15) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm15 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm15 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm6
     * PURPOSE: this class will set the alarm or the sound effect for rm6.
     */
    public class AlarmSoundrm16 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm16) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm16 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm16 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm7
     * PURPOSE: this class will set the alarm or the sound effect for rm3.
     */
    public class AlarmSoundrm17 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm17) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm17 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm17 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm18
     * PURPOSE: this class will set the alarm or the sound effect for rm8.
     */
    public class AlarmSoundrm18 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm18) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm18 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm18 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm19
     * PURPOSE: this class will set the alarm or the sound effect for rm9.
     */
    public class AlarmSoundrm19 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm19) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm19 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm19 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm20
     * PURPOSE: this class will set the alarm or the sound effect for rm10.
     */
    public class AlarmSoundrm20 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm20) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm20 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm20 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
    /**
     * NAME: AlarmSoundrm11
     * PURPOSE: this class will set the alarm or the sound effect for rm11.
     */
    public class AlarmSoundrm21 implements Runnable
    {
        @Override
        public void run()
        {
                while(!soundstoprm21) {
                    try {
                        try {
                            InputStream music = new FileInputStream(new File("src/HIMSv2_Audio/StoreDoorChime.wav"));
                            AudioStream sound = new AudioStream(music);
                            AudioPlayer.player.start(sound);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(HeadNurseHome.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
        }

        public void stopThread()
        {
            soundstoprm21 = true;
            System.out.println("Process STOPPED");
            if(soundstoprm21 == true){
                System.out.println("EXIT is now true");
            }
        }
    }
     /**
     * NAME: userAdd
     * PURPOSE: this method will add a new client connection.
     * PARAMETERS: String data or the room name inputed in the room call device.
     * RETURN VALUE: void
     */
    public void userAdd (String data) 
    {
        String name = data;
        
        if(data.equals(rm1)){
            newThreadStart();
            newAlarmStart();
        }else if(data.equals(rm2)){
            newThreadStart2();
            newAlarmStart2();
        }else if(data.equals(rm3)){
            newThreadStart898();
            newAlarmStart3();
        }else if(data.equals(rm4)){
            newThreadStart4();
            newAlarmStart4();
        }else if(data.equals(rm6)){
            newThreadStart6();
            newAlarmStart6();
        }else if(data.equals(rm7)){
            newThreadStart7();
            newAlarmStart7();
        }else if(data.equals(rm8)){
            newThreadStart8();
            newAlarmStart8();
        }else if(data.equals(rm9)){
            newThreadStart9();
            newAlarmStart9();
        }else if(data.equals(rm10)){
            newThreadStart10();
            newAlarmStart10();
        }else if(data.equals(rm11)){
            newThreadStart11();
            newAlarmStart11();
        }else if(data.equals(rm12)){
            newThreadStart12();
            newAlarmStart12();
        }else if(data.equals(rm13)){
            newThreadStart13();
            newAlarmStart13();
        }else if(data.equals(rm14)){
            newThreadStart14();
            newAlarmStart14();
        }else if(data.equals(rm15)){
            newThreadStart15();
            newAlarmStart15();
        }else if(data.equals(rm16)){
            newThreadStart16();
            newAlarmStart16();
        }else if(data.equals(rm17)){
            newThreadStart17();
            newAlarmStart17();
        }else if(data.equals(rm18)){
            newThreadStart18();
            newAlarmStart18();
        }else if(data.equals(rm19)){
            newThreadStart19();
            newAlarmStart19();
        }else if(data.equals(rm20)){
            newThreadStart20();
            newAlarmStart20();
        }else if(data.equals(rm21)){
            newThreadStart21();
            newAlarmStart21();
        }
       
        users.add(name);
        String[] tempList = new String[(users.size())];
        users.toArray(tempList);

    }
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        switchOnOff = new javax.swing.JButton();
        LB446Panel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        switchOffOn = new javax.swing.JButton();
        Panelrm1 = new javax.swing.JPanel();
        r1 = new javax.swing.JLabel();
        Panelrm2 = new javax.swing.JPanel();
        r2 = new javax.swing.JLabel();
        Panelrm3 = new javax.swing.JPanel();
        r3 = new javax.swing.JLabel();
        Panelrm4 = new javax.swing.JPanel();
        r4 = new javax.swing.JLabel();
        Panelrm6 = new javax.swing.JPanel();
        r6 = new javax.swing.JLabel();
        Panelrm7 = new javax.swing.JPanel();
        r7 = new javax.swing.JLabel();
        Panelrm8 = new javax.swing.JPanel();
        r8 = new javax.swing.JLabel();
        Panelrm9 = new javax.swing.JPanel();
        r9 = new javax.swing.JLabel();
        Panelrm10 = new javax.swing.JPanel();
        r10 = new javax.swing.JLabel();
        Panelrm11 = new javax.swing.JPanel();
        r11 = new javax.swing.JLabel();
        Panelrm12 = new javax.swing.JPanel();
        r12 = new javax.swing.JLabel();
        Panelrm13 = new javax.swing.JPanel();
        r13 = new javax.swing.JLabel();
        Panelrm14 = new javax.swing.JPanel();
        r14 = new javax.swing.JLabel();
        Panelrm15 = new javax.swing.JPanel();
        r15 = new javax.swing.JLabel();
        Panelrm16 = new javax.swing.JPanel();
        r16 = new javax.swing.JLabel();
        Panelrm17 = new javax.swing.JPanel();
        r17 = new javax.swing.JLabel();
        Panelrm18 = new javax.swing.JPanel();
        r18 = new javax.swing.JLabel();
        Panelrm19 = new javax.swing.JPanel();
        r19 = new javax.swing.JLabel();
        Panelrm20 = new javax.swing.JPanel();
        r20 = new javax.swing.JLabel();
        Panelrm21 = new javax.swing.JPanel();
        r21 = new javax.swing.JLabel();

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SWITCH", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft YaHei UI", 1, 10), new java.awt.Color(96, 155, 213))); // NOI18N

        switchOnOff.setIcon(new javax.swing.ImageIcon(getClass().getResource("/HIMSv2_Images/redcircle-10x10.png"))); // NOI18N
        switchOnOff.setText("OFF");
        switchOnOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                switchOnOffActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(switchOnOff, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(switchOnOff, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                .addContainerGap())
        );

        LB446Panel.setBackground(new java.awt.Color(255, 255, 255));
        LB446Panel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(96, 155, 213));
        jLabel4.setText("LB446");

        javax.swing.GroupLayout LB446PanelLayout = new javax.swing.GroupLayout(LB446Panel);
        LB446Panel.setLayout(LB446PanelLayout);
        LB446PanelLayout.setHorizontalGroup(
            LB446PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LB446PanelLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel4)
                .addContainerGap(37, Short.MAX_VALUE))
        );
        LB446PanelLayout.setVerticalGroup(
            LB446PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LB446PanelLayout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addContainerGap())
        );

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SWITCH", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft YaHei UI", 1, 10), new java.awt.Color(96, 155, 213))); // NOI18N

        switchOffOn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/HIMSv2_Images/redcircle-10x10.png"))); // NOI18N
        switchOffOn.setText("OFF");
        switchOffOn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                switchOffOnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(switchOffOn, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(switchOffOn, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                .addContainerGap())
        );

        Panelrm1.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r1.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r1.setForeground(new java.awt.Color(96, 155, 213));
        r1.setText("         ");
        r1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm1Layout = new javax.swing.GroupLayout(Panelrm1);
        Panelrm1.setLayout(Panelrm1Layout);
        Panelrm1Layout.setHorizontalGroup(
            Panelrm1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r1)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm1Layout.setVerticalGroup(
            Panelrm1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm2.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r2.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r2.setForeground(new java.awt.Color(96, 155, 213));
        r2.setText("         ");
        r2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm2Layout = new javax.swing.GroupLayout(Panelrm2);
        Panelrm2.setLayout(Panelrm2Layout);
        Panelrm2Layout.setHorizontalGroup(
            Panelrm2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(r2)
                .addContainerGap(29, Short.MAX_VALUE))
        );
        Panelrm2Layout.setVerticalGroup(
            Panelrm2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Panelrm3.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r3.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r3.setForeground(new java.awt.Color(96, 155, 213));
        r3.setText("         ");
        r3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm3Layout = new javax.swing.GroupLayout(Panelrm3);
        Panelrm3.setLayout(Panelrm3Layout);
        Panelrm3Layout.setHorizontalGroup(
            Panelrm3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(r3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Panelrm3Layout.setVerticalGroup(
            Panelrm3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
        );

        Panelrm4.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r4.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r4.setForeground(new java.awt.Color(96, 155, 213));
        r4.setText("         ");
        r4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm4Layout = new javax.swing.GroupLayout(Panelrm4);
        Panelrm4.setLayout(Panelrm4Layout);
        Panelrm4Layout.setHorizontalGroup(
            Panelrm4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm4Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(r4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Panelrm4Layout.setVerticalGroup(
            Panelrm4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
        );

        Panelrm6.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r6.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r6.setForeground(new java.awt.Color(96, 155, 213));
        r6.setText("         ");
        r6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r6MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm6Layout = new javax.swing.GroupLayout(Panelrm6);
        Panelrm6.setLayout(Panelrm6Layout);
        Panelrm6Layout.setHorizontalGroup(
            Panelrm6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r6)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm6Layout.setVerticalGroup(
            Panelrm6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r6, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm7.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r7.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r7.setForeground(new java.awt.Color(96, 155, 213));
        r7.setText("         ");
        r7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r7MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm7Layout = new javax.swing.GroupLayout(Panelrm7);
        Panelrm7.setLayout(Panelrm7Layout);
        Panelrm7Layout.setHorizontalGroup(
            Panelrm7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm7Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r7)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm7Layout.setVerticalGroup(
            Panelrm7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r7, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm8.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r8.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r8.setForeground(new java.awt.Color(96, 155, 213));
        r8.setText("         ");
        r8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r8MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm8Layout = new javax.swing.GroupLayout(Panelrm8);
        Panelrm8.setLayout(Panelrm8Layout);
        Panelrm8Layout.setHorizontalGroup(
            Panelrm8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm8Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r8)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm8Layout.setVerticalGroup(
            Panelrm8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r8, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm9.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r9.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r9.setForeground(new java.awt.Color(96, 155, 213));
        r9.setText("         ");
        r9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r9MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm9Layout = new javax.swing.GroupLayout(Panelrm9);
        Panelrm9.setLayout(Panelrm9Layout);
        Panelrm9Layout.setHorizontalGroup(
            Panelrm9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm9Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r9)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm9Layout.setVerticalGroup(
            Panelrm9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r9, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm10.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r10.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r10.setForeground(new java.awt.Color(96, 155, 213));
        r10.setText("         ");
        r10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r10MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm10Layout = new javax.swing.GroupLayout(Panelrm10);
        Panelrm10.setLayout(Panelrm10Layout);
        Panelrm10Layout.setHorizontalGroup(
            Panelrm10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm10Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r10)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm10Layout.setVerticalGroup(
            Panelrm10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r10, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm11.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r11.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r11.setForeground(new java.awt.Color(96, 155, 213));
        r11.setText("         ");
        r11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r11MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm11Layout = new javax.swing.GroupLayout(Panelrm11);
        Panelrm11.setLayout(Panelrm11Layout);
        Panelrm11Layout.setHorizontalGroup(
            Panelrm11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm11Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r11)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm11Layout.setVerticalGroup(
            Panelrm11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r11, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm12.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r12.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r12.setForeground(new java.awt.Color(96, 155, 213));
        r12.setText("         ");
        r12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r12MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm12Layout = new javax.swing.GroupLayout(Panelrm12);
        Panelrm12.setLayout(Panelrm12Layout);
        Panelrm12Layout.setHorizontalGroup(
            Panelrm12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm12Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r12)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm12Layout.setVerticalGroup(
            Panelrm12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r12, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm13.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm13.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r13.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r13.setForeground(new java.awt.Color(96, 155, 213));
        r13.setText("         ");
        r13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r13MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm13Layout = new javax.swing.GroupLayout(Panelrm13);
        Panelrm13.setLayout(Panelrm13Layout);
        Panelrm13Layout.setHorizontalGroup(
            Panelrm13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm13Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r13)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm13Layout.setVerticalGroup(
            Panelrm13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r13, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm14.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm14.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r14.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r14.setForeground(new java.awt.Color(96, 155, 213));
        r14.setText("         ");
        r14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r14MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm14Layout = new javax.swing.GroupLayout(Panelrm14);
        Panelrm14.setLayout(Panelrm14Layout);
        Panelrm14Layout.setHorizontalGroup(
            Panelrm14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm14Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r14)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm14Layout.setVerticalGroup(
            Panelrm14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r14, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm15.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm15.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r15.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r15.setForeground(new java.awt.Color(96, 155, 213));
        r15.setText("         ");
        r15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r15MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm15Layout = new javax.swing.GroupLayout(Panelrm15);
        Panelrm15.setLayout(Panelrm15Layout);
        Panelrm15Layout.setHorizontalGroup(
            Panelrm15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm15Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r15)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm15Layout.setVerticalGroup(
            Panelrm15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r15, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm16.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm16.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r16.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r16.setForeground(new java.awt.Color(96, 155, 213));
        r16.setText("         ");
        r16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r16MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm16Layout = new javax.swing.GroupLayout(Panelrm16);
        Panelrm16.setLayout(Panelrm16Layout);
        Panelrm16Layout.setHorizontalGroup(
            Panelrm16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm16Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r16)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm16Layout.setVerticalGroup(
            Panelrm16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r16, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm17.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm17.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r17.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r17.setForeground(new java.awt.Color(96, 155, 213));
        r17.setText("         ");
        r17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r17MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm17Layout = new javax.swing.GroupLayout(Panelrm17);
        Panelrm17.setLayout(Panelrm17Layout);
        Panelrm17Layout.setHorizontalGroup(
            Panelrm17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm17Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r17)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm17Layout.setVerticalGroup(
            Panelrm17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r17, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm18.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm18.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r18.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r18.setForeground(new java.awt.Color(96, 155, 213));
        r18.setText("         ");
        r18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r18MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm18Layout = new javax.swing.GroupLayout(Panelrm18);
        Panelrm18.setLayout(Panelrm18Layout);
        Panelrm18Layout.setHorizontalGroup(
            Panelrm18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm18Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r18)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm18Layout.setVerticalGroup(
            Panelrm18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r18, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm19.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm19.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r19.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r19.setForeground(new java.awt.Color(96, 155, 213));
        r19.setText("         ");
        r19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r19MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm19Layout = new javax.swing.GroupLayout(Panelrm19);
        Panelrm19.setLayout(Panelrm19Layout);
        Panelrm19Layout.setHorizontalGroup(
            Panelrm19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm19Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r19)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm19Layout.setVerticalGroup(
            Panelrm19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r19, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm20.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm20.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r20.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r20.setForeground(new java.awt.Color(96, 155, 213));
        r20.setText("         ");
        r20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r20MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm20Layout = new javax.swing.GroupLayout(Panelrm20);
        Panelrm20.setLayout(Panelrm20Layout);
        Panelrm20Layout.setHorizontalGroup(
            Panelrm20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm20Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r20)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm20Layout.setVerticalGroup(
            Panelrm20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r20, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        Panelrm21.setBackground(new java.awt.Color(255, 255, 255));
        Panelrm21.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        r21.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 14)); // NOI18N
        r21.setForeground(new java.awt.Color(96, 155, 213));
        r21.setText("         ");
        r21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                r21MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout Panelrm21Layout = new javax.swing.GroupLayout(Panelrm21);
        Panelrm21.setLayout(Panelrm21Layout);
        Panelrm21Layout.setHorizontalGroup(
            Panelrm21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Panelrm21Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(r21)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        Panelrm21Layout.setVerticalGroup(
            Panelrm21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(r21, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Panelrm20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Panelrm21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(Panelrm18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(Panelrm19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Panelrm16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Panelrm17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Panelrm14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Panelrm15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Panelrm12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Panelrm13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Panelrm8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Panelrm6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(Panelrm3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(Panelrm1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(Panelrm10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Panelrm9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(Panelrm7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(Panelrm4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(Panelrm2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(Panelrm11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(22, 22, 22))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Panelrm2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panelrm20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panelrm21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void switchOnOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_switchOnOffActionPerformed
        // TODO add your handling code here:

        if(flag == 0){
            ImageIcon MI = new ImageIcon(getClass().getResource("/HIMSv2_Images/greencircle-10x10.png"));
            flag = 1;
            try {
                Thread.sleep(500);
                switchOnOff.setIcon(MI);
                switchOnOff.setText("ON");
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }else if(flag == 1){
            ImageIcon MI = new ImageIcon(getClass().getResource("/HIMSv2_Images/redcircle-10x10.png"));
            flag = 0;
            try {
                Thread.sleep(500);
                switchOnOff.setIcon(MI);
                switchOnOff.setText("OFF");
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        Thread starter = new Thread(new ServerStart());
        starter.start();

    }//GEN-LAST:event_switchOnOffActionPerformed

    private void switchOffOnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_switchOffOnActionPerformed
        // TODO add your handling code here:

        if(flag == 0){
            
            ImageIcon MI = new ImageIcon(getClass().getResource("/HIMSv2_Images/greencircle-10x10.png"));
            flag = 1;
            try {
                Thread.sleep(500);
                switchOffOn.setIcon(MI);
                switchOffOn.setText("ON");
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }else if(flag == 1){
            ImageIcon MI = new ImageIcon(getClass().getResource("/HIMSv2_Images/redcircle-10x10.png"));
            flag = 0;
            try {
                Thread.sleep(500);
                switchOffOn.setIcon(MI);
                switchOffOn.setText("OFF");
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        Thread starter = new Thread(new ServerStart());
        starter.start();

    }//GEN-LAST:event_switchOffOnActionPerformed

    private void r1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r1MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r1.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_r1MouseClicked

    private void r2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r2MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r2.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r2MouseClicked

    private void r3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r3MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r3.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r3MouseClicked

    private void r4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r4MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r4.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r4MouseClicked

    private void r6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r6MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r6.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r6MouseClicked

    private void r7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r7MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r7.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r7MouseClicked

    private void r8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r8MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r8.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r8MouseClicked

    private void r9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r9MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r9.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r9MouseClicked

    private void r10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r10MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r10.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r10MouseClicked

    private void r11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r11MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r11.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r11MouseClicked

    private void r12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r12MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r12.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r12MouseClicked

    private void r13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r13MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r13.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r13MouseClicked

    private void r14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r14MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r14.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r14MouseClicked

    private void r15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r15MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r15.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r15MouseClicked

    private void r16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r16MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r16.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r16MouseClicked

    private void r17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r17MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r17.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r17MouseClicked

    private void r18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r18MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r18.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r18MouseClicked

    private void r19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r19MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r19.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r19MouseClicked

    private void r20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r20MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r20.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r20MouseClicked

    private void r21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_r21MouseClicked
        try {
            // TODO add your handling code here:
            HeadnurseRoomInfo view = new HeadnurseRoomInfo();
            view.setVisible(true);
            view.setData(r21.getText());
        } catch (IOException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(HeadnurseRoomCallStation3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_r21MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel LB446Panel;
    private javax.swing.JPanel Panelrm1;
    private javax.swing.JPanel Panelrm10;
    private javax.swing.JPanel Panelrm11;
    private javax.swing.JPanel Panelrm12;
    private javax.swing.JPanel Panelrm13;
    private javax.swing.JPanel Panelrm14;
    private javax.swing.JPanel Panelrm15;
    private javax.swing.JPanel Panelrm16;
    private javax.swing.JPanel Panelrm17;
    private javax.swing.JPanel Panelrm18;
    private javax.swing.JPanel Panelrm19;
    private javax.swing.JPanel Panelrm2;
    private javax.swing.JPanel Panelrm20;
    private javax.swing.JPanel Panelrm21;
    private javax.swing.JPanel Panelrm3;
    private javax.swing.JPanel Panelrm4;
    private javax.swing.JPanel Panelrm6;
    private javax.swing.JPanel Panelrm7;
    private javax.swing.JPanel Panelrm8;
    private javax.swing.JPanel Panelrm9;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel r1;
    private javax.swing.JLabel r10;
    private javax.swing.JLabel r11;
    private javax.swing.JLabel r12;
    private javax.swing.JLabel r13;
    private javax.swing.JLabel r14;
    private javax.swing.JLabel r15;
    private javax.swing.JLabel r16;
    private javax.swing.JLabel r17;
    private javax.swing.JLabel r18;
    private javax.swing.JLabel r19;
    private javax.swing.JLabel r2;
    private javax.swing.JLabel r20;
    private javax.swing.JLabel r21;
    private javax.swing.JLabel r3;
    private javax.swing.JLabel r4;
    private javax.swing.JLabel r6;
    private javax.swing.JLabel r7;
    private javax.swing.JLabel r8;
    private javax.swing.JLabel r9;
    private javax.swing.JButton switchOffOn;
    private javax.swing.JButton switchOnOff;
    // End of variables declaration//GEN-END:variables
}
