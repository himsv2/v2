/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class RequestInformation {
    public static void main(String[] args)
    {     
         RequestInformation x = new RequestInformation();

    }
    
    /**
     * NAME: getRequest
     * PURPOSE: connects to the servers php to get the records of Equipment Request and download the record
     * from the server to this PC.
     * PARAMETERS: int ID, String reqType
     * RETURN VALUE: void
     */
    
    public void getRequest(int ID, String reqType) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getEquipmentRequest.php?ID="+ID+"&reqType="+reqType+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        
        if(reqType.equals("linens")){
            download.downloadData("http://"+IP+"/HIMSv2/request/getLinensRequest.json", "getLinensRequest");
         
        }else{
            download.downloadData("http://"+IP+"/HIMSv2/request/getEquipmentRequest.json", "getEquipmentRequest");
         
        }
         
    }
    
    /**
     * NAME: addRequest
     * PURPOSE: This method will add new request record.
     * PARAMETERS: int ID, String item, int ns, int urgent, String remarks, String type, int qty, String patID
     * RETURN VALUE: void
     */
    public void addRequest(int ID, String item, int ns, int urgent, String remarks, String type, int qty, String patID ){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/addRequest.php?ID="+ID+"&item="+item+"&ns="+ns+"&urgent="+urgent+"&remarks="+remarks+"&type="+type+"&qty="+qty+"&patID="+patID+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
    
   
    
    /**
     * NAME: getCancel
     * PURPOSE: This method will cancel the specific  request record.
     * PARAMETERS: int ID,int nsID, String reqType 
     * RETURN VALUE: void
     */
    public void getCancel(int ID,int nsID, String reqType ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/cancelRequest.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Request is Succesfully canceled!");
        this.getRequest(nsID, reqType);
    }
    
    /**
     * NAME: setDel
     * PURPOSE: This method will delete the specific request record.
     * PARAMETERS: int ID,int nsID, String reqType
     * RETURN VALUE: void
     */
    
    public void setDel(int ID,int nsID, String reqType ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/deleteRequest.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
        this.getRequest(nsID, reqType);
    }
    
    /**
     * NAME: UpdateRequest
     * PURPOSE: This method will add new request record.
     * PARAMETERS: int ID, String item, int ns, int urgent, String type, int qty, String patID
     * RETURN VALUE: void
     */
    public void updateRequest(int ID, String item, int ns, int urgent, String type, int qty, String patID ){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/updateRequest.php?ID="+ID+"&item="+item+"&ns="+ns+"&urgent="+urgent+"&qty="+qty+"&patID="+patID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
     /**
     * NAME: getRequestInvetory
     * PURPOSE: This method will add new request record
     * PARAMETERS: String type
     * RETURN VALUE: void
     */
    
    public void getRequestInvetory(String type) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getEquipmentRequestInventory.php?reqType="+type+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        if(type.equals("linens")){
            download.downloadData("http://"+IP+"/HIMSv2/request/getLinensRequestInventory.json", "getLinensRequestInventory");
         
        }else{
            download.downloadData("http://"+IP+"/HIMSv2/request/getEquipmentRequestInventory.json", "getEquipmentRequestInventory");
         
        }
         
    }
    
    /**
     * NAME: setDelInventory
     * PURPOSE: This method will delete the specific request record in the inventory.
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    public void setDelInventory(int ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/deleteRequestInventory.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: setApproveDisapprovedInventory
     * PURPOSE: This method will delete the specific  request record in the inventory.
     * PARAMETERS: int ID, int status
     * RETURN VALUE: void
     */
    
    public void setApproveDisapprovedInventory(int ID, int status ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/setApproveDisapprovedInventory.php?ID="+ID+"&status="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }

/**
     * NAME: getMedicineRequest
     * PURPOSE: connects to the servers php to get the records of Medicine and download the record
     * from the server to this PC.
     * PARAMETERS: int ID, String reqType
     * RETURN VALUE: void
     */
    
    public void getMedicineRequest(int ID, String reqType) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getMedicineRequest.php?ID="+ID+"&reqType="+reqType+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
              
        if(reqType.equals("medicine")){
            download.downloadData("http://"+IP+"/HIMSv2/request/getMedicineRequest.json", "getMedicineRequest");         
        }       
    }

/**
     * NAME: getMedicineRequestInvetory
     * PURPOSE: connects to the servers php to get the records of Medicine and download the record
     * from the server to this PC.
     * PARAMETERS: String type
     * RETURN VALUE: void
     */
    
    public void getMedicineRequestInvetory(String type) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getMedicineRequestInventory.php?reqType="+type+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        if(type.equals("medicine")){
            download.downloadData("http://"+IP+"/HIMSv2/request/getMedicineRequestInventory.json", "getMedicineRequestInventory");
         
        }
         
    }
    
    /**
     * NAME: addBloodRequest
     * PURPOSE: This method will add new Blood request record.
     * PARAMETERS: String patID, String bt, int qty, int urgent, String remarks
     * RETURN VALUE: void
     */
    public void addBloodRequest(String patID, String bt, int qty, int urgent, String remarks){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/addBloodRequest.php?patID="+patID+"&bt="+bt+"&urgent="+urgent+"&remarks="+remarks+"&qty="+qty+"&patID="+patID+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }

    /**
     * NAME: getTradeRequest
     * PURPOSE: connects to the servers php to get the records of Trade Request and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getTradeRequest(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getTradeRequest.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/request/getTradeRequest.json", "getTradeRequest");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }

    /**
     * NAME: addTrade
     * PURPOSE: This method will add new request record.
     * PARAMETERS: String dId, String patID, int urgent
     * RETURN VALUE: void
     */
    
    public void addTrade(String dId, String patID, int urgent){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/addTradeRequest.php?dId="+dId+"&patID="+patID+"&urgent="+urgent+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
    
    /**
     * NAME: getBloodRequest
     * PURPOSE: connects to the servers php to get the records of Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBloodRequest(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getBloodRequest.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/request/getBloodRequest.json", "getBloodRequest");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getDonatedList
     * PURPOSE: connects to the servers php to get the records of Donated Blood and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getDonatedList(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/getDonatedList.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/donor/getDonatedList.json", "getDonatedList");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addDonate
     * PURPOSE: This method will add new Donate record.
     * PARAMETERS: String dId, String bloodtype, String expire
     * RETURN VALUE: void
     */
    public void addDonate(String dId, String bloodtype, String expire){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        String date;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/donor/addDonorDonate.php?dId="+dId+"&bloodtype="+bloodtype+"&expire="+expire+"&date="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
    }
    
    /**
     * NAME: setDelRequisition
     * PURPOSE: This method will delete the specific  request record 
     * PARAMETERS: integer ID
     * RETURN VALUE: void
     */
    public void setDelRequisition(int ID ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/deleteRequisition.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: setRequisitionApproveDisapprovedInventory
     * PURPOSE: This method will set the specific requisition record to Approved or disapproved
     * PARAMETERS: int ID, int status
     * RETURN VALUE: void
     */
    public void setRequisitionApproveDisapprovedInventory(int ID, int status) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/setRequisitionApproveDisapprovedInventory.php?ID="+ID+"&status="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: setOrderApproveDisapprovedInventory
     * PURPOSE: This method will set the specific inventory record to Approved or disapproved
     * PARAMETERS: int ID, int status, int qty, String name
     * RETURN VALUE: void
     */
    public void setOrderApproveDisapprovedInventory(int ID, int status, int qty, String name) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/accounting/setOrderApproveDisapprovedInventory.php?ID="+ID+"&status="+status+"&qty="+qty+"&name="+name+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
   /**
     * NAME: getMedicineRequest
     * PURPOSE: connects to the servers php to get the records of Medicine and download the record
     * from the server to this PC.
     * PARAMETERS: String type
     * RETURN VALUE: void
     */
    
    public void getMedicineRequestI(String type) throws IOException{
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/request/getMedicineRequestI.php?reqType="+type+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        if(type.equals("medicine")){
            download.downloadData("http://"+IP+"/HIMSv2/request/getMedicineRequestI.json", "getMedicineRequestI");
         
        }
         
    }
     
}
