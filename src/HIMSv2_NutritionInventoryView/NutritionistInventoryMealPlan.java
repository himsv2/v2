/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_NutritionInventoryView;

import HIMSv2_DB.FoodInformation;
import HIMSv2_DB.FoodIngredientsInformotion;
import HIMSv2_DB.IngredientsInformation;
import HIMSv2_LinenInventoryView.*;
import HIMSv2_DB.LinensInformation;
import HIMSv2_DB.MealPlanInformation;
import HIMSv2_FunctionChecker.FunctionSearch;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author cathy
 */
public class NutritionistInventoryMealPlan extends javax.swing.JPanel {

    /**
     * Creates new form adminManageBuildingView
     */
    public NutritionistInventoryMealPlan() throws IOException {
        initComponents();
        this.data();
    }
    
    /**
     * NAME: data
     * PURPOSE: this will get the information of the bed from the database.
     * PARAMETERS: no parameters.
     * RETURN VALUE: no return values.
     */
    
    public void data() throws IOException{
        MealPlanInformation a = new MealPlanInformation();
        a.getMealPlan();
        try {
           this.loadMealPlan();
        } catch (FileNotFoundException ex) {
            System.out.println("error");
        } catch (JSONException ex) {
            System.out.println("error");
        }
        
    }
    
    public void loadMealPlan() throws FileNotFoundException, IOException, JSONException{
        String description;
        String name;
        int num= 0;
        String foodID;
        String food = " ";
        String foodList;
        String price;
        
        JSONParser parser = new JSONParser();
        DefaultTableModel model = (DefaultTableModel)tblMealPlan.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        try {
  
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\mealPlan.json"));
            JSONObject json = (JSONObject) o;
            
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                
                if(json.get("mealplanDelete").equals("1") ){
                num++;
                foodID = (String) json.get("mealplanId");
                System.out.println("hnjnjnnmn" + foodID);
                int kani = Integer.parseInt(foodID);
                foodList = this.getMealPlanItem(kani);
                
                price = (String) json.get("mealplanPrice");
                description =(String) json.get("mealplanDesc");
                description = description.replace("_", " ");
                name =(String) json.get("mealplanName");
                name = name.replace("_", " ");
                model.addRow(new Object[]{num,name,description,price,foodList});
            }
          }
                    
        } catch (ParseException ex) {
            System.out.println("error");
        }
    }
    
    public String getMealPlanItem(int ID) throws IOException, ParseException{
        int ndx = 0;
        String food = " ";
        String[] ingID = new String[100];
        try {
            MealPlanInformation a = new MealPlanInformation();
            a.getMealPlanId(ID);
            
            JSONParser parser = new JSONParser();
            
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\getMealPlanId.json"));
            JSONObject json = (JSONObject) o;
            
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
     
            for(int x = 0;x < k.size();x++){
                json = (JSONObject)k.get(x);
                ingID[ndx] = (String) json.get("mealitemFood_id");
                ndx++;
                
            }
            food = this.loadFoods(ingID);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(NutritionistInventoryMealPlanAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
       return food;
    }
    public String loadFoods(String[] ingID) throws FileNotFoundException, IOException{
        
        String name ;
        String food= " ";
        int num= 0;
        
        JSONParser parser = new JSONParser();
        Object[] row = new Object[4];
        try {
  
            Object o = parser.parse(new FileReader("C:\\HIMSv2\\food.json"));
            JSONObject json = (JSONObject) o;
            org.json.simple.JSONArray k = (org.json.simple.JSONArray)json.get("data");
            Iterator<String> i = k.iterator();
            
            for(int y = 0;ingID[y] != null ;y++){
                for(int x = 0;x < k.size();x++){
                    json = (JSONObject)k.get(x);
                    if(ingID[y].equals(json.get("foodId"))){
                        num++;
                        name =(String) json.get("foodName");
                        name = name.replace("_", " ");
                        food = food +" "+name;

                    }
                    
                 }
            }
            
                    
        } catch (ParseException ex) {
            System.out.println("error");
        }
        return food;
    }
    
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMealPlan = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        addingredient = new javax.swing.JButton();
        search = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        tblMealPlan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "NAME", "DESCRIPTION", "PRICE", "FOOD"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMealPlan.setRowHeight(25);
        tblMealPlan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMealPlanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblMealPlan);

        jPanel1.setBackground(new java.awt.Color(96, 155, 213));

        addingredient.setText("ADD FOOD");
        addingredient.setActionCommand("ADD INGREDIENT");
        addingredient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addingredientActionPerformed(evt);
            }
        });

        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchKeyReleased(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("SEARCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
                .addComponent(addingredient, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(addingredient, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(search, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addingredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addingredientActionPerformed
        try {
            NutritionistInventoryMealPlanAdd a = new NutritionistInventoryMealPlanAdd();
            a.setVb(this);             
            a.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(NutritionistInventoryMealPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(NutritionistInventoryMealPlan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_addingredientActionPerformed

    private void tblMealPlanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMealPlanMouseClicked
        try {
            // TODO add your handling code here:
            String code;
            String mealPlanName;
            String mealPlandesc;
            String mealPlanprice;
            
            int rowNum = tblMealPlan.getSelectedRow();
            System.out.println(rowNum);
            mealPlanName = (String) tblMealPlan.getValueAt(rowNum, 1);
            mealPlandesc = (String) tblMealPlan.getValueAt(rowNum, 2);
            mealPlanprice = (String) tblMealPlan.getValueAt(rowNum, 3);
            NutritionistInventoryMealPlanUpdateDelete view = new NutritionistInventoryMealPlanUpdateDelete();
            view.setVb(this);
            view.setVisible(true);
            view.setData( mealPlanName, mealPlandesc,mealPlanprice);
        } catch (IOException ex) {
            Logger.getLogger(NutritionistInventoryMealPlan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(NutritionistInventoryMealPlan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_tblMealPlanMouseClicked

    private void searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchKeyReleased
        // TODO add your handling code here:
        FunctionSearch a = new FunctionSearch();
        a.search(tblMealPlan, search.getText());       
    }//GEN-LAST:event_searchKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addingredient;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblMealPlan;
    // End of variables declaration//GEN-END:variables
}
