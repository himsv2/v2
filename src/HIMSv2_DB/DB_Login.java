/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_DB_IPAddress.ServerIPAddress;
import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author cathy
 */
public class DB_Login {
    
    public String username = null;
    public String password = null;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * NAME: logindata
     * PURPOSE: Checks if the username and password exist or if it is correct.
     * PARAMETERS: no parameters.
     * RETURN VALUE: int(1 if it is correct and 0 if the user is not exist.)
     */
    public int logindata(){
        String[] ret = new String[5];
         JSONArray j = null;
        JSONParser p = new JSONParser();
        try{
            Object o = p.parse(new FileReader("C:\\HIMSv2\\phplog_login.json"));
            JSONObject json = (JSONObject) o;
            j = (JSONArray)json.get("data");
            return 1;
           
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, " Invalid Username/Password", "Login Error", JOptionPane.ERROR_MESSAGE);
            return 0;
        }
        
       
    }
    
    /**
     * NAME: phpLogin
     * PURPOSE: connects to the servers php to get the records of the given username and password and download the record
     * from the server to this PC.
     * PARAMETERS: String password and String username
     * RETURN VALUE: void
     */
    
    public void phpLogin(String password, String username){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        
        String myUrl = "http://"+IP+"/HIMSv2/login/login.php?username="+username+"&password="+password;
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        try {
            download.downloadData("http://"+IP+"/HIMSv2/login/login.json", "phplog_login");
        } catch (IOException ex) {
            System.out.println("error");
        }     
    }
}
