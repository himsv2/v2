/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class FoodIngredientsInformotion {
    /**
     * NAME: getFoodIngredients
     * PURPOSE: connects to the servers php to get the records of ingredients of a food and download the record
     * from the server to this PC.
     * PARAMETERS: int ID of the food
     * RETURN VALUE: void
     */
    
    public void getFoodIngredients(int ID){   
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodItem/foodItem.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/foodItem/foodItem.json", "foodItem");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    /**
     * NAME: addFoodIngredients
     * PURPOSE: connects to the servers php to add new records of the ingredients of the given food.
     * PARAMETERS: int pat id and int ingredients ID.
     * RETURN VALUE: void
     */
    
    public void addFoodIngredients(int patID, int ingID){        
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodItem/addfoodItem.php?patID="+patID+"&ingID="+ingID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: removeFoodIngredients
     * PURPOSE: remove the ingredients of the given food.
     * PARAMETERS: int food ID, int food ingredient ID to be deleted.
     * RETURN VALUE: void
     */
    
    public void removeFoodIngredients(int foodID, int ingID){      
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodItem/removefoodItem.php?foodID="+foodID+"&ingID="+ingID+"";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        
        
    }
}
