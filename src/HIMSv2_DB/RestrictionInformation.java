/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author albert
 */
public class RestrictionInformation {
    public static void main(String[] args)
    {     
         RestrictionInformation x = new RestrictionInformation();

    }
    
    /**
     * NAME: getRestriction
     * PURPOSE: connects to the servers php to get the records of Restriction and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getRestriction(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodrestriction/restriction.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/foodrestriction/restriction.json", "restriction");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: getFoodRestriction
     * PURPOSE: connects to the servers php to get the records of Food Restriction and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getFoodRestriction(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodrestriction/foodRestriction.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/foodrestriction/foodRestriction.json", "foodRestriction");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
     /**
     * NAME: addFoodRestriction
     * PURPOSE: This method will add new FoodRestriction record.
     * PARAMETERS: int fss, String pt
     * RETURN VALUE: void
     */
    
    public void addFoodRestriction(int fss, String pt){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodrestriction/addfoodRestriction.php?fss="+fss+"&pt="+pt+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteFoodRestriction
     * PURPOSE: This method will add new FoodRestriction record.
     * PARAMETERS: String frId
     * RETURN VALUE: void
     */
    
    public void deleteFoodRestriction(String frId){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/foodrestriction/deletefoodRestriction.php?frId="+frId+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
    /**
     * NAME: addRestriction
     * PURPOSE: This method will add new Restriction record.
     * PARAMETERS: String restName
     * RETURN VALUE: void
     */
    
    public void addRestriction(String restName){
        String date;
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        date = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss").format(new Date());
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/restriction/addrestriction.php?restName="+restName+"&restOn="+date+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);

    }
    
    /**
     * NAME: delRestriction
     * PURPOSE: This method will delete Restriction record.
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void delRestriction(int ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/restriction/delrestriction.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
    
    /**
     * NAME: updateRestriction
     * PURPOSE: This method will update Restriction record.
     * PARAMETERS: int restId, String restOn, String restName
     * RETURN VALUE: void
     */
    
    public void updateRestriction(int restId, String restOn, String restName){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
                
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/restriction/updaterestriction.php?restName="+restName+"&restId="+restId+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    
}
