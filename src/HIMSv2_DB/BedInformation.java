/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author cathy
 */
public class BedInformation {
    public static void main(String[] args)
    {     
         BedInformation x = new BedInformation();

    }
    
    /**
     * NAME: getBed
     * PURPOSE: connects to the servers php to get the records of bed and download the record
     * from the server to this PC.
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getBed(){       
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/bed/bed.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/bed/bed.json", "bed");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    
    /**
     * NAME: addBed
     * PURPOSE: This method will add new Bed record.
     * PARAMETERS: String bed name and integer room ID
     * RETURN VALUE: void
     */
    public void addBed(String bedName, int roomID){
        System.out.println(bedName+","+roomID);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/bed/addBed.php?bedName="+bedName+"&roomID="+roomID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Added!");
        System.out.println(myUrl);
    }
    
    /**
     * NAME: deleteBed
     * PURPOSE: This method delete Bed record.
     * PARAMETERS: int id of the bed 
     * RETURN VALUE: void
     */
    public void deleteBed(int bedID){
        System.out.println(bedID);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/bed/deleteBed.php?bedID="+bedID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Deleted!");
        System.out.println(myUrl);
    }
    
     /**
     * NAME: updateBed
     * PURPOSE: This method will update Bed record
     * PARAMETERS: String bed name and integer bed ID
     * RETURN VALUE: void
     */
    public void updateBed(String bedName, int roomID,int bedID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/bed/updateBed.php?bedName="+bedName+"&roomID="+roomID+"&bedID="+bedID+"";
        new JavaHTTPConnectionReader(myUrl);
        JOptionPane.showMessageDialog(null,"Successfully Updated!");
        System.out.println(myUrl);
    }
    /**
     * NAME: filterBed
     * PURPOSE: This method will get the Occupied or not occupied  Bed record.
     * PARAMETERS: integer 0 if not occupied and 1 for occupied.
     * RETURN VALUE: void
     */
    public void filterBed(int bedOccupied){
        Download_JSON download = new Download_JSON();
        System.out.println(bedOccupied);
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/bed/filterBed.php?bedOccupied="+bedOccupied+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/bed/bed.json", "bed");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
}
