/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author albert
 */
public class MedicineScheduleInformation {
    public static void main(String[] args)
    {     
         MedicineScheduleInformation x = new MedicineScheduleInformation();

    }
    /**
     * NAME: getMedSched
     * PURPOSE: connects to the servers php to get the records of Medicine Schedule and download the record
     * from the server to this PC
     * PARAMETERS: no parameters
     * RETURN VALUE: void
     */
    
    public void getMedSched(){
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/medicineTask.php?";
        new JavaHTTPConnectionReader(myUrl);
        
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/medicineTask/medicineTask.json", "medicineTask");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
    /**
     * NAME: deleteMed
     * PURPOSE: This method will delete Medicine Schedule record
     * PARAMETERS: String IDS
     * RETURN VALUE: void
     */
      public void deleteMed(String IDS ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/deletemedicineTask.php?IDS="+IDS+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
      
       /**
     * NAME: deleteStatus
     * PURPOSE: This method will delete Medicine Schedule record.
     * PARAMETERS:String ID
     * RETURN VALUE: void
     */
      
      public void deleteStatus(String ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/deleteAccomplishedMed.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);  
    }
      
       /**
     * NAME: updateMedSched
     * PURPOSE: This method will update new Medicine Schedule record.
     * PARAMETERS: String notes,String dos,String unit,String qty,String d,String MedSchedID,String time,String amPm
     * RETURN VALUE: void
     */
      
    public void updateMedSched(String notes,String dos,String unit,String qty,String d,String MedSchedID,String time,String amPm){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/updateMedicineTask.php?notes="+notes+"&dos="+dos+"&unit="+unit+"&qty="+qty+"&d="+d+"&MedSchedID="+MedSchedID+"&time="+time+"&amPm="+amPm+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: addMedSched
     * PURPOSE: This method will add new Medicine Schedule record
     * PARAMETERS: String d,String Med,String dosage,String un,String qty,String notes, String IDS,String doctorId,String nurseId,String time,String amPm
     * RETURN VALUE: void
     */
    
    public void addMedSched(String d,String Med,String dosage,String un,String qty,String notes, String IDS,String doctorId,String nurseId,String time,String amPm) {
       ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/addMedTask.php?d="+d+"&Med="+Med+"&dosage="+dosage+"&un="+un+"&qty="+qty+"&notes="+notes+"&IDS="+IDS+"&doctorId="+doctorId+"&nurseId="+nurseId+"&time="+time+"&amPm="+amPm+" ";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }
    
    /**
     * NAME: setAccomplishUnaccomplish
     * PURPOSE: This method will set Accomplish or Un-accomplish Medicine Schedule record.
     * PARAMETERS: int ID, String status
     * RETURN VALUE: void
     */
    
    public void setAccomplishUnaccomplish(int ID, String status ) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/setAccomplishUnaccomplish.php?msId="+ID+"&msStatus="+status+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
    
    /**
     * NAME: delMedSched
     * PURPOSE: This method will delete Medicine Schedule record
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void delMedSched(int ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/delMedSched.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
  
      /**
     * NAME: ReqMedSched
     * PURPOSE: This method will request new Medicine Schedule record
     * PARAMETERS: int ID
     * RETURN VALUE: void
     */
    
    public void ReqMedSched(int ID) throws IOException{
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/medicineTask/ReqMedSched.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        
    }
}
