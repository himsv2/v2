/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HIMSv2_DB;

import HIMSv2_ConnectionReader.JavaHTTPConnectionReader;
import HIMSv2_DB_IPAddress.ServerIPAddress;
import java.io.IOException;

/**
 *
 * @author cathy
 */
public class DocFeeInformation {
    /**
     * NAME: getDocFee
     * PURPOSE: This method will get Doctors fee record.
     * PARAMETERS: String ID
     * RETURN VALUE: void
     */
    
    public void getDocFeeID(String ID){
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;
        Download_JSON download = new Download_JSON();
        
        IP = getIP.IPAddress();
        String myUrl = "http://"+IP+"/HIMSv2/fee/docFeeID.php?ID="+ID+"";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
        try {
            download.downloadData("http://"+IP+"/HIMSv2/fee/docFeeID.json", "docFeeID");
        } catch (IOException ex) {
            System.out.println("error");
        }  
    }
     /**
     * NAME: getLinens PURPOSE: connects to the servers php to get the records
     * of linens and download the record from the server to this PC. PARAMETERS:
     * no parameters RETURN VALUE: void
     */
    public void getDocFee() {
        Download_JSON download = new Download_JSON();
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/fee/DocFee.php?";
        new JavaHTTPConnectionReader(myUrl);

        System.out.println(myUrl);
        try {
            download.downloadData("http://" + IP + "/HIMSv2/fee/DocFee.json", "DocFee");
        } catch (IOException ex) {
            System.out.println("error");
        }
    }

    /**
     * NAME: addLinen PURPOSE: This method will add new linens record.
     * PARAMETERS: String item code String name String quantity , String type ,
     * String description and String price RETURN VALUE: void
     */

    public void addDocFee(String DocId, String fee) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
        String myUrl = "http://" + IP + "/HIMSv2/fee/addDocFee.php?DocId=" + DocId + "&fee=" + fee + " ";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }

    /**
     * NAME: updateDocFee PURPOSE: This method will update new Equipment record.
     * PARAMETERS: String item Code, String name, String quantity, String type,
     * String description,String price, int linen ID RETURN VALUE: void
     */
    public void updateDocFee(String IdDocFee, String fee) {
        ServerIPAddress getIP = new ServerIPAddress();
        String IP;

        IP = getIP.IPAddress();
         String myUrl = "http://" + IP + "/HIMSv2/fee/updateDocFee.php?IdDocFee="+IdDocFee+"&fee=" + fee + " ";
        new JavaHTTPConnectionReader(myUrl);
        System.out.println(myUrl);
    }

}
